import styled from '@emotion/styled';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import { useSafeState } from 'ahooks';
import { Divider, Form, Input, InputNumber, theme, Typography } from 'antd';
import { useRouter } from 'next/router';
import { useEffect, useRef } from 'react';
import { BsXLg } from 'react-icons/bs';
import { LiaSearchLocationSolid } from 'react-icons/lia';
import { MdClose, MdOutlineArrowUpward } from 'react-icons/md';
import { RiEdit2Fill } from 'react-icons/ri';
import { TbChevronRight, TbMap, TbZoomReset } from 'react-icons/tb';
import {
  GeolocateControl,
  Layer,
  Marker,
  NavigationControl,
  Popup,
  ScaleControl,
  Source,
} from 'react-map-gl/maplibre';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import ButtonNoti from 'src/components/button/ButtonNoti';
import AddressListItem, { AddressItemWrapper } from 'src/components/card/AddressListItem';
import LocaleSelect from 'src/components/dropdown/LocaleSelect';
import FilterSearchCompact from 'src/components/field/FilterSearchCompact';
import LatLngInput from 'src/components/field/LatLngInput';
import UpdateProvinceForm from 'src/components/form/UpdateProvinceForm';
import MapGL, { TMapRef } from 'src/components/map/MapGL';
import { lineStyle } from 'src/components/map/mapStyle';
import Container from 'src/components/shared/Container';
import useApp from 'src/hooks/useApp';
import useChangeLocale from 'src/hooks/useChangeLocale';
import withAuth from 'src/hooks/withAuth';
import {
  TAddressFilter,
  useGetAddressDetailQuery,
  useGetDistrictListQuery,
  useGetProvinceListQuery,
  useUpdateAddressMutation,
} from 'src/redux/query/address.query';
import { useReverseGeocodingMutation } from 'src/redux/query/nominatim.query';
import { useAppSelector } from 'src/redux/store';
import { ErrorCode } from 'src/types/response.types';
import { DEFAULT_POSITION } from 'src/utils/constant';

function Page() {
  const { pathname, query, replace } = useRouter();
  const selectedProvinceId = !!query?.p ? (query.p as string) : undefined;
  const { i18n } = useChangeLocale();
  const { message } = useApp();
  const {
    token: { colorBorder },
  } = theme.useToken();
  const [formUpdateDistrict] = Form.useForm();
  const mapRef = useRef<TMapRef>(null);
  const { data: userState } = useAppSelector((s) => s.user);
  const [showSearch, setShowSearch] = useSafeState(false);
  const [addressFilterValue, setAddressFilterValue] = useSafeState<TAddressFilter>({});
  const handleLocalSearch = (formData: Pick<TAddressFilter, 'keyword'>) => {
    const { keyword } = formData;
    setAddressFilterValue({ ...addressFilterValue, keyword });
  };
  const { currentData: getProvinceListRes } = useGetProvinceListQuery(addressFilterValue, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });
  const provinceList = getProvinceListRes?.data?.address_list || [];
  const [editProvinceId, setEditProvinceId] = useSafeState<string>();
  const { data: getProvinceDetailRes } = useGetAddressDetailQuery(selectedProvinceId || skipToken, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });
  const provinceDetail = getProvinceDetailRes?.data;

  const handleSelectedProvince = (pId: string) => {
    replace({ pathname, query: { p: pId } }, { pathname, query: { p: pId } }, { shallow: true });
  };

  const [selectedDistrictId, setSelectedDistrictId] = useSafeState<string>();
  const [editDistrictId, setEditDistrictId] = useSafeState<string>();
  const { currentData: getDistrictListRes } = useGetDistrictListQuery(
    !!selectedProvinceId ? { p: selectedProvinceId } : skipToken,
    {
      refetchOnFocus: true,
      refetchOnMountOrArgChange: true,
    },
  );
  const districtList = getDistrictListRes?.data?.address_list || [];
  const { data: getDistrictDetailRes } = useGetAddressDetailQuery(selectedDistrictId || skipToken, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });
  const districtDetail = getDistrictDetailRes?.data;
  const centerTextWatch = Form.useWatch('centerText', formUpdateDistrict);
  const districtCenterLatLng = centerTextWatch?.split(',');
  const [reverseGeocodingMutate, { data: reverseGeocodingRes }] = useReverseGeocodingMutation();
  const addressName = reverseGeocodingRes?.display_name;

  const handleFitBounds = (bounds?: number[]) => {
    if (!!bounds) {
      const [minLng, minLat, maxLng, maxLat] = bounds;
      mapRef.current?.fitBounds(
        [
          [minLng, minLat],
          [maxLng, maxLat],
        ],
        { padding: 12, duration: 100 },
      );
    }
  };

  const [updateAddressMutate, { isLoading: isUpdateAddressLoading }] = useUpdateAddressMutation();
  const handleUpdateAddress = ({ centerText, ...formData }: any) => {
    const newFormData = { ...formData };
    if (centerText) {
      const [lat, long] = centerText.split(',');
      newFormData.center_lat = +lat;
      newFormData.center_long = +long;
    }
    updateAddressMutate(newFormData)
      .unwrap()
      .then(({ message: resMess }) => {
        message.success(resMess);
      })
      .catch((err) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          message.error(err.error[0].message);
      })
      .finally(() => {
        setEditProvinceId(undefined);
        setEditDistrictId(undefined);
      });
  };

  useEffect(() => {
    if (!!selectedProvinceId) {
      const provinceDetail = getProvinceDetailRes?.data;
      handleFitBounds(provinceDetail?.bbox);
    } else {
      setEditProvinceId(undefined);
      setEditDistrictId(undefined);
      setSelectedDistrictId(undefined);
      mapRef.current?.setCenter(DEFAULT_POSITION);
      mapRef.current?.setZoom(16);
    }
  }, [selectedProvinceId, getProvinceDetailRes]);
  useEffect(() => {
    const districtDetail = getDistrictDetailRes?.data;
    if (!!districtDetail) {
      reverseGeocodingMutate({
        lat: districtDetail.address.center_lat,
        lng: districtDetail.address.center_long,
      });
      if (editDistrictId || selectedDistrictId) {
        formUpdateDistrict.resetFields();
      }
    }
    handleFitBounds(districtDetail?.bbox);
  }, [selectedDistrictId, editDistrictId, getDistrictDetailRes]);

  return (
    <PageWrapper>
      <div className='left-wrapper'>
        <div className='breadcrumb-wrapper'>
          <Breadcrumb
            hideHomePage
            items={{
              '/address': i18n['Tỉnh thành'],
            }}
            current={selectedProvinceId ? provinceDetail?.address.name : ''}
          />
          <FilterSearchCompact
            size='large'
            placeholder={i18n['Tìm kiếm theo Tên']}
            onChange={(v) => handleLocalSearch({ keyword: v })}
          />
        </div>
        <Container>
          {!selectedProvinceId ? (
            <div className='list-wrapper'>
              {!!provinceList.length ? (
                provinceList.map((p, pIndex) => (
                  <AddressListItem
                    key={p.id + 'province' + pIndex}
                    selected={selectedProvinceId === p.id}
                    avatarProps={{ children: pIndex + 1 }}
                    actions={
                      <>
                        <Button
                          type='text'
                          icon={<RiEdit2Fill size={18} />}
                          onClick={() => {
                            setEditProvinceId(p.id);
                          }}
                        />
                        <Button
                          type='text'
                          icon={<TbChevronRight size={18} />}
                          onClick={() => {
                            handleSelectedProvince(p.id);
                          }}
                        />
                      </>
                    }
                    onClickName={() => {
                      handleSelectedProvince(p.id);
                    }}
                    style={
                      !!editProvinceId && editProvinceId !== p.id
                        ? { display: 'none' }
                        : editProvinceId === p.id
                        ? { borderBottom: 'none' }
                        : {}
                    }
                    {...p}
                  />
                ))
              ) : (
                <></>
              )}
              {!!editProvinceId && (
                <UpdateProvinceForm
                  provinceId={editProvinceId}
                  onClose={() => {
                    setEditProvinceId(undefined);
                  }}
                  style={{ margin: 0 }}
                  onFinish={handleUpdateAddress}
                />
              )}
            </div>
          ) : !!districtList ? (
            <AddressItemWrapper key={selectedProvinceId + 'wrapper'}>
              {!!provinceDetail?.address && (
                <AddressListItem
                  selected
                  avatarProps={{ icon: <TbMap size={20} /> }}
                  nameIcon={<TbZoomReset className='view-icon' size={18} />}
                  onClickName={() => {
                    handleFitBounds(provinceDetail?.bbox);
                    setSelectedDistrictId(undefined);
                    setEditDistrictId(undefined);
                  }}
                  actions={
                    editProvinceId === provinceDetail.address.id ? (
                      <></>
                    ) : (
                      <>
                        <Button
                          type='text'
                          icon={<RiEdit2Fill size={18} />}
                          onClick={() => {
                            setEditProvinceId(provinceDetail.address.id);
                            handleFitBounds(provinceDetail?.bbox);
                          }}
                        />
                        <Button
                          type='text'
                          icon={<MdClose size={23} />}
                          onClick={() => {
                            replace({ pathname, query: {} }, '/admin/address', { shallow: true });
                          }}
                        />
                      </>
                    )
                  }
                  {...provinceDetail.address}
                />
              )}
              {!!editProvinceId ? (
                <UpdateProvinceForm
                  provinceId={editProvinceId}
                  onClose={() => {
                    setEditProvinceId(undefined);
                    handleFitBounds(districtDetail?.bbox);
                  }}
                  onFinish={handleUpdateAddress}
                />
              ) : (
                <>
                  <div className='district-wrapper'>
                    {districtList.map((d, dIndex) => (
                      <AddressListItem
                        selected={selectedDistrictId === d.id}
                        key={selectedProvinceId + d.id + 'district' + dIndex}
                        avatarProps={{ children: dIndex + 1 }}
                        nameIcon={
                          selectedDistrictId === d.id ? (
                            <TbZoomReset className='view-icon' size={18} />
                          ) : undefined
                        }
                        actions={
                          <>
                            {selectedDistrictId === d.id && (
                              <Button
                                type='text'
                                icon={
                                  <LiaSearchLocationSolid
                                    style={{ transform: 'scaleX(-1)' }}
                                    size={24}
                                  />
                                }
                                onClick={() => {
                                  if (!!d?.center_lat && !!d?.center_long) {
                                    mapRef.current?.setCenter({
                                      lat: d.center_lat,
                                      lng: d.center_long,
                                    });
                                    mapRef.current?.setZoom(18);
                                  }
                                }}
                              />
                            )}
                            {editDistrictId !== d.id && (
                              <Button
                                type='text'
                                icon={<RiEdit2Fill size={18} />}
                                onClick={() => {
                                  setSelectedDistrictId(d.id);
                                  setEditDistrictId(d.id);
                                }}
                              />
                            )}
                          </>
                        }
                        onClickName={() => {
                          setSelectedDistrictId(d.id);
                          handleFitBounds(districtDetail?.bbox);
                        }}
                        style={
                          !!editDistrictId && editDistrictId !== d.id ? { display: 'none' } : {}
                        }
                        {...d}
                      />
                    ))}
                  </div>

                  <div
                    className={
                      !editDistrictId || editDistrictId !== districtDetail?.address.id
                        ? 'form-wrapper hidden'
                        : 'form-wrapper'
                    }
                  >
                    <Typography.Paragraph strong>{i18n['Cập nhật']}</Typography.Paragraph>
                    <Button
                      type='text'
                      className='close-btn'
                      icon={<BsXLg size={18} />}
                      onClick={() => {
                        setEditDistrictId(undefined);
                        setSelectedDistrictId(undefined);
                        handleFitBounds(provinceDetail?.bbox);
                      }}
                    />
                    {!!districtDetail && (
                      <Form
                        disabled={!editDistrictId || editDistrictId !== districtDetail?.address.id}
                        hidden={!editDistrictId || editDistrictId !== districtDetail?.address.id}
                        key={districtDetail.address.id + 'form'}
                        form={formUpdateDistrict}
                        layout='vertical'
                        size='large'
                        initialValues={{
                          id: districtDetail.address.id,
                          name: districtDetail.address.name,
                          centerText: [
                            districtDetail.address.center_lat,
                            districtDetail.address.center_long,
                          ].join(','),
                          priority: districtDetail.address.priority,
                        }}
                        onFinish={handleUpdateAddress}
                      >
                        <Form.Item name={'id'} hidden>
                          <Input />
                        </Form.Item>
                        <Form.Item name={'name'} label={i18n['Tên']} rules={[{ required: true }]}>
                          <Input />
                        </Form.Item>
                        <Form.Item
                          name={'centerText'}
                          label={i18n['Điểm đón']}
                          rules={[{ required: true }]}
                        >
                          <LatLngInput placeholder={i18n['Nhập vĩ độ,kinh độ địa chỉ hiện tại']} />
                        </Form.Item>
                        <Form.Item
                          name={'priority'}
                          label={i18n['Độ ưu tiên trong danh sách']}
                          rules={[{ required: true }]}
                        >
                          <InputNumber step={1} min={1} style={{ width: '100%' }} />
                        </Form.Item>
                        <div className='actions-wrapper'>
                          <Button
                            type='text'
                            htmlType='button'
                            bgColor={colorBorder}
                            onClick={() => {
                              formUpdateDistrict.resetFields();
                            }}
                          >
                            {i18n['Hủy']}
                          </Button>
                          <Button type='primary' htmlType='submit' icon={<MdOutlineArrowUpward />}>
                            {i18n['Cập nhật']}
                          </Button>
                        </div>
                      </Form>
                    )}
                  </div>
                </>
              )}
            </AddressItemWrapper>
          ) : (
            <></>
          )}
        </Container>
      </div>
      <div className='right-wrapper'>
        <div className='header-right'>
          <LocaleSelect />
          <Divider type='vertical' />
          <ButtonNoti />
        </div>
        <MapGL
          ref={mapRef}
          initialViewState={{
            latitude: DEFAULT_POSITION.lat,
            longitude: DEFAULT_POSITION.lng,
            zoom: 16,
          }}
          maxZoom={20}
          minZoom={2}
          style={{ width: '100%' }}
        >
          <GeolocateControl position='bottom-right' />
          <NavigationControl position='bottom-right' />
          <ScaleControl position='bottom-left' />
          {provinceDetail ? (
            <Source type='geojson' key={selectedProvinceId} data={provinceDetail.geometry as any}>
              <Layer id='province' {...lineStyle('#69b1ff', 1)} />
            </Source>
          ) : (
            <></>
          )}
          {!!districtDetail && selectedDistrictId === districtDetail.address.id ? (
            <>
              {!!districtCenterLatLng ? (
                <>
                  <Marker
                    key={selectedDistrictId + 'Marker'}
                    latitude={districtCenterLatLng[0]}
                    longitude={districtCenterLatLng[1]}
                    color='red'
                    scale={0.8}
                    draggable={!!editDistrictId && editDistrictId === districtDetail.address.id}
                    onDragEnd={(e) => {
                      const newGeoLocation = e.lngLat;
                      reverseGeocodingMutate(newGeoLocation);
                      formUpdateDistrict.setFieldValue(
                        'centerText',
                        `${newGeoLocation.lat},${newGeoLocation.lng}`,
                      );
                    }}
                  />
                  <PopupStyled
                    key={selectedDistrictId + 'Popup'}
                    latitude={districtCenterLatLng[0]}
                    longitude={districtCenterLatLng[1]}
                    anchor='bottom'
                    closeButton={false}
                    closeOnClick={false}
                    offset={[0, -28] as any}
                    onClose={() => {}}
                    style={{ padding: 0 }}
                    className='hide-arrow'
                  >
                    <Typography.Paragraph className='address-name'>
                      {addressName}
                    </Typography.Paragraph>
                  </PopupStyled>
                </>
              ) : (
                <></>
              )}
              <Source
                type='geojson'
                key={selectedDistrictId + 'geojson'}
                data={districtDetail.geometry as any}
              >
                <Layer id='district' {...lineStyle('#4096ff', 2)} />
              </Source>
            </>
          ) : (
            <></>
          )}
          <></>
        </MapGL>
      </div>
    </PageWrapper>
  );
}

const PopupStyled = styled(Popup)`
  .maplibregl-popup-content {
    position: relative;
    width: 36px;
    height: 36px;
    padding: 0;
    background-color: transparent;
    box-shadow: none;
    .address-name {
      position: absolute;
      bottom: 0;
      right: 0;
      transform: translate(100%, 100%);
      width: 220px;
      line-height: 1.3;
      font-size: 13px;
      margin: 0;
      text-shadow: 1px 1px 2px var(--colorBorder);
    }
  }
  &.hide-arrow {
    .maplibregl-popup-tip {
      display: none;
    }
  }
`;

const PageWrapper = styled.div`
  flex: 1 1 auto;
  min-width: 0px;
  display: flex;
  & > .left-wrapper {
    flex: 0 0 360px;
    background-color: var(--bgColor);
    display: flex;
    flex-direction: column;
    box-shadow: var(--boxShadow);
    z-index: 1;
    .breadcrumb-wrapper {
      position: relative;
      padding: 0 12px 0 24px;
      height: 64px;
      flex-shrink: 0;
      display: flex;
      align-items: center;
      border-bottom: 1px solid var(--colorBorder);
      justify-content: space-between;
      .filter-search-compact {
        position: absolute;
        top: 50%;
        right: 12px;
        transform: translateY(-50%);
      }
    }
    .filter-wrapper {
      padding: 12px 12px;
    }
    .list-wrapper {
      padding: 0 6px;
    }
  }
  & > .right-wrapper {
    position: relative;
    z-index: 0;
    flex: 1 1 auto;
    min-width: 0px;
    & > .header-right {
      position: absolute;
      top: 0;
      right: 12px;
      z-index: 1;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      height: 64px;
    }
  }
`;

export default withAuth(Page, true);
