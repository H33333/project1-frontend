//@ts-nocheck
import styled from '@emotion/styled';
import { theme, Typography } from 'antd';
import { useCallback, useEffect, useState } from 'react';
import { FaCoins, FaEye, FaUser } from 'react-icons/fa';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Card from 'src/components/card/Card';
import MoniterListItem from 'src/components/card/MoniterListItem';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import StyledListContainer from 'src/components/list/StyledListContainer';
import ModalView from 'src/components/ModalView';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useDebounce from 'src/hooks/useDebounce';
import withAuth from 'src/hooks/withAuth';
import { useGetFilteredMoniterQuery, useGetOnlineAgencyQuery } from 'src/redux/query/moniter.query';
import { TListFilter } from 'src/redux/query/user.query';
import { TMetaBase } from 'src/types/response.types';

const initialFilterValue: TListFilter = { page: 1, limit: 10 };

function Page() {
  const [view, setView] = useState<any>({});
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [intervalId, setIntervalId] = useState<NodeJS.Timeout | null>(null); // State to store the interval ID
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { i18n } = useChangeLocale();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [usersFilterValue, setUsersFilterValue] = useState<any>(initialFilterValue);
  const debouncedFilter = useDebounce(usersFilterValue, 500);
  const [module, setModule] = useState<string>('Module');
  const [device, setDevice] = useState<string>('Device');

  const {
    data: moniterFilteredQuery,
    isSuccess: getMonitersSuccess,
    isFetching: getMoniterFetching,
  } = useGetFilteredMoniterQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const {
    data: getOnlineAgencyQuery,
    isSuccess: getOnlineAgencySuccess,
    isFetching: getOnlineAgencyFetching,
    refetch: refetchOnlineAgency,
  } = useGetOnlineAgencyQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const monitersFilteredData = getMonitersSuccess
    ? (moniterFilteredQuery?.data?.monitor_list as any) || []
    : [];

  const monitersFilteredMetaData: TMetaBase | undefined = getMonitersSuccess
    ? moniterFilteredQuery?.data?.meta_data
    : undefined;

  const handleLocalSearch = ({ keyword, sort }: { keyword: string; sort: string }) => {
    setUsersFilterValue({ ...usersFilterValue, page: 1, keyword, sort });
  };

  const handleTabChange = (value: string) => {
    setUsersFilterValue({ ...usersFilterValue, status: +value || undefined });
  };

  const handleChangeModule = (module: any) => {
    if (Number(module) === 0) {
      setModule('AUTH');
    }
    if (Number(module) === 1) {
      setModule('COIN');
    }

    setUsersFilterValue({ ...usersFilterValue, page: 1, module });
  };
  const handleChangeDevice = (device: any) => {
    if (Number(device) === 0) {
      setDevice('APP');
    }
    if (Number(device) === 1) {
      setDevice('WEB');
    }
    if (Number(device) === 2) {
      setDevice('CMS');
    }
    setUsersFilterValue({ ...usersFilterValue, page: 1, device });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onClickView = (value: any) => {
    setView(value);
    setIsModalOpen(true);
  };

  const handleChangeRefecthApi = useCallback(
    (value: any) => {
      if (intervalId) {
        clearInterval(intervalId); // Clear any existing interval
        setIntervalId(null);
      }

      let intervalTime;
      if (value === '0') {
        intervalTime = 5000; // 5 seconds
      } else if (value === '1') {
        intervalTime = 10000; // 10 seconds
      } else if (value === '2') {
        intervalTime = 15000;
      }

      if (intervalTime) {
        const newIntervalId = setInterval(() => {
          refetchOnlineAgency(); // Refetch the API
        }, intervalTime);

        setIntervalId(newIntervalId); // Store the new interval ID
      }
    },
    [intervalId, refetchOnlineAgency],
  );

  // Cleanup interval on component unmount
  useEffect(() => {
    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
    };
  }, [intervalId]);

  console.log(module, 'module');

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={i18n['Quản lý hoạt động']} />} ref={setContainer}>
      {!mediaAbove767 && (
        <div className='page-header'>
          <Breadcrumb current={i18n['Quản lý hoạt động']} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div>{i18n['Danh sách quản lý hoạt động']}</div>
              <div style={{ display: 'flex' }}>
                <div style={{ marginTop: '2px' }}>
                  {`${getOnlineAgencyQuery?.data?.countOnlineAgency}`}
                </div>
                <SortDropdown
                  onChange={handleChangeRefecthApi}
                  // title={}
                  items={[
                    { key: '0', label: `5 giây` },
                    { key: '1', label: `10 giây` },
                    { key: '2', label: `20 giây` },
                  ]}
                />
              </div>
            </div>
          </Typography.Title>
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        // activeTabKey={String(usersFilterValue.status || '')}
        onTabChange={(key) => handleTabChange(key)}
        tabProps={{ tabBarGutter: 20 }}
      >
        <div style={{ width: '100%', display: 'flex' }}>
          <div className='header-filter-container'>
            <LocalSearch<{ keyword: string; sort: string }>
              onFinish={handleLocalSearch}
              onValuesChange={(changedValue, values) => handleLocalSearch(values)}
            >
              <FormItem name='keyword'>
                <InputSearch
                  bordered={false}
                  placeholder={
                    mediaAbove400
                      ? i18n['Tìm kiếm theo tiêu đề, mô tả']
                      : i18n['Tên, Số điện thoại']
                  }
                />
              </FormItem>
              {/* <SortDropdown
                // title='Module'
                items={[
                  { key: '0', label: `Module 0 - 10` },
                  { key: '10', label: `Module 10 - 20` },
                  { key: '20', label: 'Module 20 - 30' },
                  { key: '30', label: 'Module 30 - 40' },
                ]}
              /> */}
            </LocalSearch>
          </div>
          <div style={{ width: '20%', display: 'flex', paddingTop: '20px' }}>
            <div style={{ width: '50%' }}>
              <SortDropdown
                onChange={handleChangeModule}
                title={module}
                items={[
                  {
                    key: '0',
                    label: (
                      <>
                        <FaUser /> AUTH
                      </>
                    ),
                  },
                  {
                    key: '1',
                    label: (
                      <>
                        <FaCoins /> COIN
                      </>
                    ),
                  },
                ]}
              />
            </div>
            <div style={{ width: '50%' }}>
              <SortDropdown
                title={device}
                onChange={handleChangeDevice}
                items={[
                  { key: '0', label: `APP` },
                  { key: '1', label: `WEB` },
                  { key: '2', label: 'CMS' },
                ]}
              />
            </div>
          </div>
        </div>
        <StyledListContainer<any>
          loading={getMoniterFetching}
          dataSource={monitersFilteredData}
          pagination={{
            metadata: monitersFilteredMetaData,
            onChange: (page, pageSize) =>
              setUsersFilterValue({ ...usersFilterValue, page, limit: pageSize }),
          }}
          target={() => container}
          renderItem={(item, index) => (
            <MoniterListItem
              key={item._id + index}
              actions={
                <div>
                  <FaEye onClick={() => onClickView(item)} size={24} />
                </div>
              }
              {...item}
            />
          )}
        />
      </Card>
      <ModalView isModalOpen={isModalOpen} view={view} handleCancel={handleCancel} />
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 24px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 80%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page);
