import styled from '@emotion/styled';
import { Divider, theme, Typography } from 'antd';
import { useState } from 'react';
import { BsPlusLg } from 'react-icons/bs';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import Card from 'src/components/card/Card';
import PMListItem from 'src/components/card/PMListItem';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import SegmentedStatus from 'src/components/field/SegmentedStatus';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import StyledListContainer from 'src/components/list/StyledListContainer';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useChangeStatusUser from 'src/hooks/useChangeStatusUser';
import useDebounce from 'src/hooks/useDebounce';
import withAdmin from 'src/hooks/withAdmin';
import { TListFilter, useGetFilteredUsersQuery } from 'src/redux/query/user.query';
import { TMetaBase } from 'src/types/response.types';
import { TUser } from 'src/types/user.types';

const initialFilterValue: TListFilter = { page: 1, limit: 10 };

function HomePage() {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { i18n } = useChangeLocale();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [usersFilterValue, setUsersFilterValue] = useState<TListFilter>(initialFilterValue);
  const debouncedFilter = useDebounce(usersFilterValue, 500);
  const {
    data: usersFilteredQuery,
    isSuccess: getUsersSuccess,
    isFetching: getUsersFetching,
  } = useGetFilteredUsersQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const usersFilteredData = getUsersSuccess ? usersFilteredQuery?.data?.user_list || [] : [];
  const usersFilteredMetaData: TMetaBase | undefined = getUsersSuccess
    ? usersFilteredQuery?.data?.meta_data
    : undefined;

  const { handleChangeUserStatus, blockUserLoading, unBlockUserLoading } = useChangeStatusUser();

  const handleLocalSearch = ({ keyword, sort }: { keyword: string; sort: string }) => {
    setUsersFilterValue({ ...usersFilterValue, page: 1, keyword, sort });
  };
  const handleSelectSort = (value: string) => {
    setUsersFilterValue({ ...usersFilterValue, sort: value });
  };
  const handleTabChange = (value: string) => {
    setUsersFilterValue({ ...usersFilterValue, status: +value || undefined });
  };

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={i18n['Quản lý đối tác']} />} ref={setContainer}>
      {!mediaAbove767 && (
        <div className='page-header'>
          <Breadcrumb current={i18n['Quản lý đối tác']} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            {i18n['Danh sách quản lý đối tác']}
          </Typography.Title>
        }
        extra={
          <Button type='primary' icon={<BsPlusLg />} href='/admin/partner/create'>
            {mediaAbove767 ? i18n['Thêm quản lý đối tác'] : undefined}
          </Button>
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        tabList={[
          {
            key: '',
            tab: i18n['Tất cả'],
          },
          {
            key: '1',
            tab: i18n['Hoạt động'],
          },
          {
            key: '2',
            tab: i18n['Đã chặn'],
          },
        ]}
        activeTabKey={String(usersFilterValue.status || '')}
        onTabChange={(key) => handleTabChange(key)}
        tabProps={{ tabBarGutter: 20 }}
      >
        <div className='header-filter-container'>
          <LocalSearch<{ keyword: string; sort: string }>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch
                bordered={false}
                placeholder={
                  mediaAbove400
                    ? i18n['Tìm kiếm theo Tên, Số điện thoại']
                    : i18n['Tên, Số điện thoại']
                }
              />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='sort'>
              <SortDropdown
                items={[
                  { key: 'name_asc', label: `Tên A->Z` },
                  { key: 'name_desc', label: `Tên Z->A` },
                  { key: 'createdAt_desc', label: 'Ngày tạo mới nhất' },
                  { key: 'createdAt_asc', label: 'Ngày tạo cũ nhất' },
                ]}
              />
            </FormItem>
          </LocalSearch>
        </div>
        <StyledListContainer<TUser>
          loading={getUsersFetching}
          dataSource={usersFilteredData}
          pagination={{
            metadata: usersFilteredMetaData,
            onChange: (page, pageSize) =>
              setUsersFilterValue({ ...usersFilterValue, page, limit: pageSize }),
          }}
          target={() => container}
          renderItem={(item, index) => (
            <PMListItem
              key={item._id + index}
              actions={
                <SegmentedStatus
                  status={item.status}
                  disabled={blockUserLoading || unBlockUserLoading}
                  onChange={(v) => handleChangeUserStatus(item._id, v as string)}
                />
              }
              {...item}
            />
          )}
        />
      </Card>
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 24px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAdmin(HomePage);
