import styled from '@emotion/styled';
import { Card, Form, Input, Radio, Space, Steps, Typography } from 'antd';
import { useRouter } from 'next/router';
import { BsBoxArrowUpRight, BsPlusLg } from 'react-icons/bs';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import PageLayout from 'src/components/layout/PageLayout';
import useApp from 'src/hooks/useApp';
import useChangeLocale from 'src/hooks/useChangeLocale';
import withAdmin from 'src/hooks/withAdmin';
import { useCreateUserMutation } from 'src/redux/query/user.query';
import { ErrorCode } from 'src/types/response.types';
import { mappedErrorToFormError } from 'src/utils/utils-error';

function CreatePage() {
  const { push } = useRouter();
  const { message } = useApp();
  const { i18n } = useChangeLocale();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove1023 = useMediaQuery({ minWidth: 1023 });
  const mediaAbove376 = useMediaQuery({ minWidth: 376 });
  const [form] = Form.useForm();

  const [createUser, { isLoading }] = useCreateUserMutation();

  const handleCreateUser = ({ confirm, ...formData }: any) => {
    createUser({ data: formData })
      .unwrap()
      .then((res) => {
        message.success({
          content: (
            <Space direction='vertical' size={2} align='start'>
              <div>{res.message}</div>
              <Space
                style={{ color: '#52c41a', textDecoration: 'underline' }}
                wrap={false}
                size={2}
              >
                <BsBoxArrowUpRight /> <span>Check now</span>
              </Space>
            </Space>
          ),
          style: { cursor: 'pointer' },
          onClick: () => {
            push('/admin/partner');
            message.destroy();
          },
          duration: 3,
        });
        form.resetFields();
      })
      .catch((err) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          message.error(err.error[0].message);
        if (err.response_code === ErrorCode.RequestValidationError) {
          form.setFields(mappedErrorToFormError(err.error));
        }
      });
  };

  return (
    <PageWrapper
      breadcrumb={
        <Breadcrumb
          current={i18n['Thêm quản lý đối tác']}
          items={{ ['/admin/partner']: i18n['Đối tác'] }}
        />
      }
    >
      {!mediaAbove767 && (
        <div className='page-header'>
          <div className='breadcrumb'>
            <Breadcrumb
              current={i18n['Thêm quản lý đối tác']}
              items={{ ['/admin/partner']: i18n['Đối tác'] }}
            />
          </div>
        </div>
      )}
      <Form
        form={form}
        layout='vertical'
        size='large'
        onFinish={handleCreateUser}
        onFinishFailed={(errorInfo) => {
          console.log(errorInfo);
        }}
        initialValues={{
          role: 'PM',
        }}
        disabled={isLoading}
        autoComplete='off'
      >
        <div className='title-wrapper'>
          <Typography.Title className='page-title' level={2}>
            {i18n['Thêm']}
          </Typography.Title>
          <Form.Item
            name='role'
            noStyle
            label='Role'
            rules={[{ required: true, message: '• Role is required' }]}
          >
            <Radio.Group size={mediaAbove1023 ? 'middle' : mediaAbove376 ? 'middle' : 'small'}>
              <Radio.Button value='PM'>{i18n['Quản lý đối tác']}</Radio.Button>
              <Radio.Button value='EXPERT'>{'Chuyên gia'}</Radio.Button>
            </Radio.Group>
          </Form.Item>
        </div>
        <Steps
          direction='vertical'
          progressDot={!mediaAbove376}
          size={'small'}
          items={[
            {
              status: 'wait',
              title: (
                <Card>
                  <Form.Item
                    name='name'
                    label={i18n['Họ & tên']}
                    rules={[{ required: true, message: '• Name is required' }]}
                  >
                    <Input type='text' placeholder={i18n['Nhập Họ và tên']} />
                  </Form.Item>
                  <Form.Item
                    name='phone'
                    label={i18n['Số điện thoại']}
                    rules={[{ required: true, message: '• Phone is required' }]}
                  >
                    <Input type='tel' placeholder={i18n['Nhập Số điện thoại']} />
                  </Form.Item>
                </Card>
              ),
            },
            {
              status: 'wait',
              title: (
                <Card>
                  <Form.Item
                    name='password'
                    label={i18n['Mật khẩu đăng nhập']}
                    rules={[
                      { required: true, message: '• Password is required' },
                      { min: 8, message: '• Password must be at least 8 characters long' },
                    ]}
                  >
                    <Input.Password placeholder={i18n['Nhập mật khẩu']} />
                  </Form.Item>
                  <Form.Item
                    label={i18n['Xác nhận lại mật khẩu']}
                    validateTrigger={'onKeyUp'}
                    name='confirm'
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                      { required: true, message: '• Confirm Password!' },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue('password') === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(new Error('• Confirm Password not matched!'));
                        },
                      }),
                    ]}
                  >
                    <Input.Password placeholder={i18n['Nhập lại mật khẩu']} />
                  </Form.Item>
                </Card>
              ),
            },
            {
              status: 'wait',
              title: (
                <Form.Item className='actions-container' style={{ marginBottom: 0 }}>
                  <Button
                    htmlType='button'
                    block
                    className='reset-btn'
                    disabled={isLoading}
                    onClick={(e) => {
                      form.resetFields();
                    }}
                  >
                    {i18n['Hủy']}
                  </Button>
                  <Button
                    htmlType='submit'
                    block
                    loading={isLoading}
                    type='primary'
                    icon={<BsPlusLg />}
                  >
                    {i18n['Thêm mới']}
                  </Button>
                </Form.Item>
              ),
            },
          ]}
        />
      </Form>
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0;
  .page-header {
    padding: 0 24px;
    margin-bottom: 24px;
    .breadcrumb {
      margin: 0 0 6px 0;
    }
  }
  .ant-form-vertical {
    padding: 2px 24px 0 24px;
    width: 100%;
    max-width: 640px;
    .title-wrapper {
      display: flex;
      align-items: center;
      margin-bottom: 24px;
      .page-title {
        margin: 0 12px 0 0;
      }
      .role-button svg {
        width: 16px;
        height: 16px;
      }
    }
    .ant-steps-item-title {
      width: 100%;
      padding-bottom: 24px;
      padding-right: 0;
      line-height: 1.4 !important;
      .ant-steps-item-icon {
        margin-inline-end: 12px !important;
      }
      .actions-container {
        .ant-form-item-control-input-content {
          display: flex;
          justify-content: flex-start;
          align-items: center;
          gap: 24px;
        }
        & button.reset-btn {
          width: 120px;
        }
        & button[type='submit'] {
          width: 320px;
        }
      }
    }
  }

  @media screen and (max-width: 1023.98px) {
    .page-header {
      .page-title {
        font-size: 24px;
      }
    }
  }

  @media screen and (max-width: 767.98px) {
    .page-header {
      padding: 12px 32px 0;
      margin-bottom: 12px;
    }
    .ant-form-vertical {
      padding: 0 26px 0 6px;
      .title-wrapper {
        padding-left: 24px;
        margin-bottom: 16px;
      }
      .ant-steps-item-icon {
        margin-inline-end: 6px !important;
      }
      .actions-container .ant-form-item-control-input-content {
        gap: 12px;
      }
    }
  }

  @media screen and (max-width: 376.98px) {
    .page-header {
      padding: 20px 20px 0;
      margin-bottom: 12px;
      .page-title {
        font-size: 22px;
      }
    }
    .ant-form-vertical {
      padding-left: 6px;
      padding-right: 12px;
      .title-wrapper {
        padding-left: 12px;
        margin-bottom: 16px;
      }
      .ant-steps-item-title {
        padding-bottom: 12px;
      }
      .ant-steps-item-icon {
        margin-inline-end: 6px !important;
      }
      .ant-card-body {
        padding: 18px 24px 4px;
      }
      .actions-container {
        .ant-form-item-control-input-content {
          flex-direction: column-reverse;
          gap: 12px;
        }
        & button {
          width: 100% !important;
          flex-shrink: 0;
        }
      }
    }
  }
`;

export default withAdmin(CreatePage);
