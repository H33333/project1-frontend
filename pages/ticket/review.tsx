import styled from '@emotion/styled';
import { useAsyncEffect, useDebounce, useSafeState } from 'ahooks';
import { Affix, Badge, Divider, Empty, Select, theme, Tooltip, Typography } from 'antd';
import { queryTypes, useQueryStates } from 'next-usequerystate';
import { useRouter } from 'next/router';
import { useId } from 'react';
import { BsSendCheckFill } from 'react-icons/bs';
import { LuSettings2 } from 'react-icons/lu';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import Card from 'src/components/card/Card';
import TicketReviewDrawer from 'src/components/card/TicketReviewDrawer';
import TicketReviewListItem from 'src/components/card/TicketReviewListItem';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import CarTypeSelect from 'src/components/field/CarTypeSelect';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import PaginationMetadata from 'src/components/list/PaginationMetadata';
import Tag from 'src/components/tag/Tag';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useGetAddress from 'src/hooks/useGetAddress';
import withAuth from 'src/hooks/withAuth';
import {
  TListFilter,
  useActionsRankedTicketReviewMutation,
  useGetAllTicketReviewsQuery,
  useGetTicketReviewCountQuery,
  useUpdateRankTicketReviewMutation,
} from 'src/redux/query/ticket-review.query';
import { useAppSelector } from 'src/redux/store';
import { ErrorCode } from 'src/types/response.types';

const initialQueryState = {
  page: queryTypes.float.withDefault(1),
  limit: queryTypes.float.withDefault(10),
  ticket_status: queryTypes.integer,
  car_type_id: queryTypes.integer,
  is_ranked: queryTypes.integer,
  status: queryTypes.integer.withDefault(0),
  sort: queryTypes.string,
};

function Page() {
  const uid = useId();
  const { query } = useRouter();
  const { i18n } = useChangeLocale();
  const { notification } = useApp();
  const {
    token: { colorPrimaryBorder, colorBgTextHover },
  } = theme.useToken();

  const mediaAbove1023 = useMediaQuery({ minWidth: 1023 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });

  const { data: userState } = useAppSelector((s) => s.user);
  const { carIconById } = useCarType();
  const { getProvinceDistrictTextById } = useGetAddress();

  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);

  const [filterValue, setFilterValue] = useQueryStates(initialQueryState);
  const [filterKeyword, setFilterKeyword] = useSafeState<string>();
  const debouncedFilterKeyword = useDebounce(filterKeyword, { wait: 500 });
  const { data: getAllTicketReviewListRes, isFetching: getAllTicketListFetching } =
    useGetAllTicketReviewsQuery(
      { ...filterValue, keyword: debouncedFilterKeyword },
      { refetchOnFocus: true, refetchOnMountOrArgChange: true },
    );
  const ticketReviewListData = getAllTicketReviewListRes?.data?.ticket_review_list || [];
  const ticketReviewListMetaData = getAllTicketReviewListRes?.data?.meta_data || undefined;
  const { data: getTicketReviewCountRes } = useGetTicketReviewCountQuery(
    {},
    { refetchOnFocus: true, refetchOnMountOrArgChange: true },
  );
  const ticketReviewListCount = getTicketReviewCountRes?.data.count_list;

  const [updateRankTicketReviewMutate, { isLoading: updateRankTicketReviewLoading }] =
    useUpdateRankTicketReviewMutation();
  const [actionsRankedTicketReviewMutate, { isLoading: actionsRankedTicketReviewLoading }] =
    useActionsRankedTicketReviewMutation();

  const [selectedItemId, setSelectedItemId] = useSafeState<string>();

  const handleUpdateRankTicketReview = () => {
    updateRankTicketReviewMutate({})
      .unwrap()
      .then(({ message }) => {
        notification.success({ message, placement: 'bottomRight' });
      })
      .catch((err: any) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          notification.error({ message: err.error[0].message, placement: 'bottomRight' });
      });
    actionsRankedTicketReviewMutate({})
      .unwrap()
      .then(({ message }) => {
        notification.success({ message, placement: 'bottomRight' });
      })
      .catch((err: any) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          notification.error({ message: err.error[0].message, placement: 'bottomRight' });
      });
  };

  const handleSetFilterValue = (value: any) => {
    setTimeout(() => {
      setFilterValue(value);
    }, 100);
  };

  const handleLocalSearch = ({
    keyword,
    sort,
    car_type_id,
  }: Pick<TListFilter, 'keyword' | 'sort' | 'car_type_id' | 'ticket_status'>) => {
    setFilterKeyword(keyword);
    setTimeout(() => {
      setFilterValue({
        ...filterValue,
        page: 1,
        sort: sort || null,
        car_type_id: car_type_id || null,
      });
    }, 100);
  };

  useAsyncEffect(async () => {
    if (!Object.keys(query).length) {
      await setFilterValue({
        page: 1,
        limit: 10,
      });
    }
  }, [query]);

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={'Lịch chờ đánh giá'} />} ref={setContainer}>
      {!mediaAbove1023 && (
        <div className='page-header'>
          <Breadcrumb current={'Lịch chờ đánh giá'} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            Danh sách Lịch chờ đánh giá
          </Typography.Title>
        }
        extra={
          !!ticketReviewListCount?.length && (
            <Tooltip
              placement='bottomRight'
              arrow={false}
              overlayInnerStyle={{ padding: '6px 8px 8px 8px' }}
              title={
                <>
                  Xếp hạng lịch <br />
                  <Tag
                    bordered={false}
                    color='processing'
                    style={{ height: 48, fontSize: 14, marginTop: 6 }}
                  >
                    {ticketReviewListCount[1]} • Đã đánh giá <br /> và chưa xếp hạng
                  </Tag>
                </>
              }
            >
              <Badge count={ticketReviewListCount[1]} color={colorPrimaryBorder}>
                <Button
                  size='large'
                  type='primary'
                  icon={<BsSendCheckFill size={14} />}
                  style={{ height: 34 }}
                  loading={updateRankTicketReviewLoading || actionsRankedTicketReviewLoading}
                  onClick={() => handleUpdateRankTicketReview()}
                >
                  Xếp hạng
                </Button>
              </Badge>
            </Tooltip>
          )
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        headStyle={{ border: 'none' }}
        tabList={[
          {
            key: JSON.stringify({ status: 0, is_ranked: null }),
            label: `Chưa đánh giá • ${ticketReviewListCount?.[0] || 0}`,
          },
          {
            key: JSON.stringify({ status: 1, is_ranked: 0 }),
            label: `Đã đánh giá & chưa xếp hạng • ${ticketReviewListCount?.[1] || 0}`,
          },
          {
            key: JSON.stringify({ status: 1, is_ranked: 1 }),
            label: `Đã xếp hạng • ${ticketReviewListCount?.[2] || 0}`,
          },
        ]}
        activeTabKey={JSON.stringify({
          status: filterValue.status,
          is_ranked: filterValue.is_ranked,
        })}
        tabProps={{
          onTabClick: (activeKey) =>
            handleSetFilterValue({ ...filterValue, page: 1, ...JSON.parse(activeKey) }),
          tabBarGutter: 20,
          tabBarStyle: { marginBottom: 6 },
        }}
      >
        <div className='header-filter-container'>
          <LocalSearch<Pick<TListFilter, 'keyword' | 'sort' | 'car_type_id' | 'ticket_status'>>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch bordered={false} placeholder={'Tìm kiếm theo Chi tiết lịch'} />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='car_type_id'>
              <Select
                allowClear
                bordered={false}
                popupMatchSelectWidth={220}
                placement='bottomRight'
                placeholder='Trạng thái lịch'
                style={{ padding: 0 }}
                options={[
                  { value: JSON.stringify({ ticket_status: 0 }), label: 'Lịch mới' },
                  { value: JSON.stringify({ ticket_status: 1 }), label: 'Lịch đã giao' },
                  { value: JSON.stringify({ ticket_status: 2 }), label: 'Lịch đã hoàn thành' },
                  { value: JSON.stringify({ ticket_status: 3 }), label: 'Lịch đã hủy' },
                ]}
              />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='car_type_id'>
              <CarTypeSelect
                allowClear
                bordered={false}
                popupMatchSelectWidth={220}
                placement='bottomRight'
                style={{ padding: 0 }}
              />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='sort'>
              <SortDropdown
                items={[
                  { key: 'createdAt_desc', label: i18n['Ngày tạo mới nhất'] },
                  { key: 'createdAt_asc', label: i18n['Ngày tạo cũ nhất'] },
                ]}
              />
            </FormItem>
          </LocalSearch>
        </div>
        <div className='list-wrapper'>
          {!!ticketReviewListData.length ? (
            <div className='list'>
              {ticketReviewListData.map((item, index) => (
                <TicketReviewListItem
                  key={uid + item._id}
                  mappedCarIcon={carIconById}
                  mappedArea={getProvinceDistrictTextById}
                  avatarIcon={index + 1}
                  actions={
                    <Button
                      size='small'
                      type='link'
                      icon={<LuSettings2 />}
                      style={{
                        backgroundColor: 'transparent',
                        fontSize: 15,
                        padding: 0,
                        textDecoration: 'underline',
                      }}
                      loading={selectedItemId === item._id}
                      onClick={() => setSelectedItemId(item._id)}
                    >
                      {item.status === 0 ? `Đánh giá lịch` : `Cập nhật đánh giá`}
                    </Button>
                  }
                  {...item}
                />
              ))}
            </div>
          ) : (
            <Empty description={false} style={{ margin: '24px 0' }} />
          )}
        </div>
        <Affix
          offsetBottom={0.01}
          target={() => container}
          key={JSON.stringify(getAllTicketListFetching)}
        >
          <PaginationMetadata
            showLessItems
            metadata={ticketReviewListMetaData}
            onChange={(page, pageSize) => setFilterValue({ ...filterValue, page, limit: pageSize })}
          />
        </Affix>
      </Card>
      {!!selectedItemId && (
        <TicketReviewDrawer
          ticket_review_id={selectedItemId}
          open={!!selectedItemId}
          setOpen={(open) => !open && setSelectedItemId(undefined)}
        />
      )}
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 22px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
      .list-wrapper {
        padding: 0 24px 0;
      }
      .ant-pagination {
        padding: 20px 24px;
        display: flex;
        align-items: center;
        border-radius: 0 0 8px 8px;
        .ant-pagination-total-text {
          margin-right: auto;
        }
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page, true);
