import styled from '@emotion/styled';
import { useAsyncEffect, useDebounce, useSafeState } from 'ahooks';
import { Divider, Empty, theme, Typography } from 'antd';
import { queryTypes, useQueryStates } from 'next-usequerystate';
import { useRouter } from 'next/router';
import { useId } from 'react';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Card from 'src/components/card/Card';
import TicketRankAgencyListItem from 'src/components/card/TicketRankAgencyListItem';
import TicketReviewDrawer from 'src/components/card/TicketReviewDrawer';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import SegmentedStatus from 'src/components/field/SegmentedStatus';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useChangeStatusAgency from 'src/hooks/useChangeStatusAgency';
import useGetAddress from 'src/hooks/useGetAddress';
import withAuth from 'src/hooks/withAuth';
import {
  TListFilter,
  useGetRankTicketReviewAgencyQuery,
} from 'src/redux/query/ticket-review.query';
import { useAppSelector } from 'src/redux/store';

const initialQueryState = {
  page: queryTypes.float.withDefault(1),
  limit: queryTypes.float.withDefault(10),
  ticket_status: queryTypes.integer,
  car_type_id: queryTypes.integer,
  sort: queryTypes.string,
};

function Page() {
  const uid = useId();
  const { query } = useRouter();
  const { i18n } = useChangeLocale();
  const { notification } = useApp();
  const {
    token: { colorPrimaryBorder, colorBgTextHover },
  } = theme.useToken();

  const mediaAbove1023 = useMediaQuery({ minWidth: 1023 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });

  const { data: userState } = useAppSelector((s) => s.user);
  const { carIconById } = useCarType();
  const { getProvinceDistrictTextById } = useGetAddress();

  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);

  const [filterValue, setFilterValue] = useQueryStates(initialQueryState);
  const [filterKeyword, setFilterKeyword] = useSafeState<string>();
  const debouncedFilterKeyword = useDebounce(filterKeyword, { wait: 500 });
  const { data: getRankTicketReviewAgencyRes, isFetching: getRankTicketReviewAgencyFetching } =
    useGetRankTicketReviewAgencyQuery(
      { ...filterValue, keyword: debouncedFilterKeyword },
      { refetchOnFocus: true, refetchOnMountOrArgChange: true },
    );
  const getRankTicketReviewAgencyData =
    getRankTicketReviewAgencyRes?.data?.agency_review_list || [];

  const { handleChangeStatus, blockLoading, unBlockLoading } = useChangeStatusAgency();

  const [selectedItemId, setSelectedItemId] = useSafeState<string>();

  const handleSetFilterValue = (value: any) => {
    setTimeout(() => {
      setFilterValue(value);
    }, 100);
  };

  const handleLocalSearch = ({ keyword, sort }: Pick<TListFilter, 'keyword' | 'sort'>) => {
    setFilterKeyword(keyword);
    setTimeout(() => {
      setFilterValue({
        ...filterValue,
        sort: sort || null,
      });
    }, 100);
  };

  useAsyncEffect(async () => {
    if (!Object.keys(query).length) {
      await setFilterValue({
        page: 1,
        limit: 10,
      });
    }
  }, [query]);

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={'Đối tác vi phạm'} />} ref={setContainer}>
      {!mediaAbove1023 && (
        <div className='page-header'>
          <Breadcrumb current={'Đối tác vi phạm'} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            Danh sách đối tác vi phạm
          </Typography.Title>
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        headStyle={{ border: 'none' }}
      >
        <div className='header-filter-container'>
          <LocalSearch<Pick<TListFilter, 'keyword' | 'sort'>>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch bordered={false} placeholder={'Tìm kiếm theo Tên, Số điện thoại'} />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='sort'>
              <SortDropdown
                items={[
                  { key: 'createdAt_desc', label: i18n['Ngày tạo mới nhất'] },
                  { key: 'createdAt_asc', label: i18n['Ngày tạo cũ nhất'] },
                ]}
              />
            </FormItem>
          </LocalSearch>
        </div>
        <div className='list-wrapper'>
          {!!getRankTicketReviewAgencyData.length ? (
            <div className='list'>
              {getRankTicketReviewAgencyData.map((item, index) => (
                <TicketRankAgencyListItem
                  key={uid + index}
                  getCarIcon={carIconById}
                  getAddressText={getProvinceDistrictTextById}
                  avatarIcon={index + 1}
                  actions={
                    <SegmentedStatus
                      status={item.agency_info.status}
                      disabled={blockLoading || unBlockLoading}
                      onChange={(v) => handleChangeStatus(item.agency_info._id, v as string)}
                    />
                  }
                  {...item}
                />
              ))}
            </div>
          ) : (
            <Empty description={false} style={{ margin: '24px 0' }} />
          )}
        </div>
      </Card>
      {!!selectedItemId && (
        <TicketReviewDrawer
          ticket_review_id={selectedItemId}
          open={!!selectedItemId}
          setOpen={(open) => !open && setSelectedItemId(undefined)}
        />
      )}
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 22px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
      .list-wrapper {
        padding: 0 24px 12px;
      }
      .ant-pagination {
        padding: 20px 24px;
        display: flex;
        align-items: center;
        border-radius: 0 0 8px 8px;
        .ant-pagination-total-text {
          margin-right: auto;
        }
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page, true);
