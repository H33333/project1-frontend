import styled from '@emotion/styled';
import { useAsyncEffect, useDebounce, useSafeState } from 'ahooks';
import { Affix, Divider, Empty, Space, Tabs, Typography } from 'antd';
import { queryTypes, useQueryStates } from 'next-usequerystate';
import { useRouter } from 'next/router';
import { useId } from 'react';
import { BsXLg } from 'react-icons/bs';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import Card from 'src/components/card/Card';
import TicketListItem from 'src/components/card/TicketListItem';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import MasonryLayout from 'src/components/list/MasonryLayout';
import PaginationMetadata from 'src/components/list/PaginationMetadata';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useGetAddress from 'src/hooks/useGetAddress';
import useModalConfirm from 'src/hooks/useModalConfirm';
import withAuth from 'src/hooks/withAuth';
import {
  TListFilter,
  useCancelTicketMutation,
  useGetAllTicketListQuery,
} from 'src/redux/query/ticket.query';
import { useAppSelector } from 'src/redux/store';
import { ErrorCode, TMetaBase } from 'src/types/response.types';

const initialQueryState = {
  page: queryTypes.float.withDefault(1),
  limit: queryTypes.float.withDefault(10),
  status: queryTypes.integer,
  car_type_id: queryTypes.integer,
  sort: queryTypes.string,
};

function Page() {
  const uid = useId();
  const { query } = useRouter();
  const { i18n } = useChangeLocale();
  const { notification } = useApp();
  const { data: userState } = useAppSelector((s) => s.user);
  const { carIconById } = useCarType();
  const { getProvinceDistrictTextById } = useGetAddress();

  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);

  const [filterValue, setFilterValue] = useQueryStates(initialQueryState);
  const [filterKeyword, setFilterKeyword] = useSafeState<string>();
  const debouncedFilterKeyword = useDebounce(filterKeyword, { wait: 500 });
  const { data: getAllTicketListRes, isFetching: getAllTicketListFetching } =
    useGetAllTicketListQuery(
      { ...filterValue, keyword: debouncedFilterKeyword },
      { refetchOnFocus: true, refetchOnMountOrArgChange: true },
    );
  const ticketListData = getAllTicketListRes?.data?.ticket_list || [];
  const ticketListMetaData: TMetaBase | undefined =
    getAllTicketListRes?.data?.meta_data || undefined;

  const [cancelTicketMutate, { isLoading: cancelTicketLoading }] = useCancelTicketMutation();

  const handleSetFilterValue = (value: any) => {
    setTimeout(() => {
      setFilterValue(value);
    }, 100);
  };

  const handleLocalSearch = async ({ keyword, sort }: Pick<TListFilter, 'keyword' | 'sort'>) => {
    setFilterKeyword(keyword);
    setTimeout(() => {
      setFilterValue({ ...filterValue, page: 1, sort });
    }, 100);
  };

  const { handleConfirm: handleConfirmCancel } = useModalConfirm({
    onOk: (id) => {
      cancelTicketMutate({ ticket_id: id })
        .unwrap()
        .then(({ message, data }) => {
          notification.success({ message, placement: 'bottomRight' });
        })
        .catch((err: any) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
    },
  });

  useAsyncEffect(async () => {
    if (!Object.keys(query).length) {
      await setFilterValue({
        page: 1,
        limit: 10,
      });
    }
  }, [query]);

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={'Lịch'} />} ref={setContainer}>
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            Danh sách Lịch
          </Typography.Title>
        }
        extra={
          <Tabs
            items={[
              // { key: JSON.stringify({ status: null }), label: 'Tất cả' },
              { key: JSON.stringify({ status: 0 }), label: 'Mới' },
              { key: JSON.stringify({ status: 1 }), label: 'Đã giao' },
              { key: JSON.stringify({ status: 6 }), label: 'Đi đón khách' },
              { key: JSON.stringify({ status: 5 }), label: 'Đã đón khách' },
              { key: JSON.stringify({ status: 4 }), label: 'Trả khách' },
              { key: JSON.stringify({ status: 2 }), label: 'Đã hoàn thành' },
              { key: JSON.stringify({ status: 3 }), label: 'Đã hủy' },
            ]}
            activeKey={JSON.stringify({ status: filterValue.status })}
            onTabClick={(activeKey) =>
              handleSetFilterValue({ ...filterValue, ...JSON.parse(activeKey) })
            }
            tabBarGutter={16}
            style={{ marginTop: -10 }}
          />
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        headStyle={{ border: 'none' }}
      >
        <div className='header-filter-container'>
          <LocalSearch<Pick<TListFilter, 'keyword' | 'sort'>>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch bordered={false} placeholder={'Tìm kiếm theo Chi tiết lịch'} />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='sort'>
              <SortDropdown
                items={[
                  { key: 'createdAt_desc', label: i18n['Ngày tạo mới nhất'] },
                  { key: 'createdAt_asc', label: i18n['Ngày tạo cũ nhất'] },
                ]}
              />
            </FormItem>
          </LocalSearch>
        </div>
        <div className='list-wrapper'>
          {!!ticketListData.length ? (
            <MasonryLayout columns={2} gap={12}>
              {ticketListData.map((item, index) => (
                <TicketListItem
                  key={uid + item._id + index}
                  carIcon={carIconById(item.car_type_id)}
                  mappedAddress={getProvinceDistrictTextById(
                    item.pickup_province,
                    item.pickup_district,
                  )
                    .map((item) => item.name)
                    .join(',')}
                  avatarIcon={index + 1}
                  actions={
                    <Space.Compact size='small'>
                      <Button type='text'>Xem chi tiết</Button>
                      {![2, 3].includes(item.status) && (
                        <Button
                          danger
                          type='text'
                          icon={<BsXLg />}
                          onClick={() => handleConfirmCancel(item._id, item._id, 'hủy lịch')}
                        >
                          Hủy lịch
                        </Button>
                      )}
                    </Space.Compact>
                  }
                  {...item}
                />
              ))}
            </MasonryLayout>
          ) : (
            <Empty description={false} style={{ margin: '24px 0' }} />
          )}
        </div>
        <Affix
          offsetBottom={0.01}
          target={() => container}
          key={JSON.stringify(getAllTicketListFetching)}
        >
          <PaginationMetadata
            showLessItems
            metadata={ticketListMetaData}
            onChange={(page, pageSize) => setFilterValue({ ...filterValue, page, limit: pageSize })}
          />
        </Affix>
      </Card>
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 22px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
      .list-wrapper {
        padding: 0 24px 0;
      }
      .ant-pagination {
        padding: 20px 24px;
        display: flex;
        align-items: center;
        border-radius: 0 0 8px 8px;
        .ant-pagination-total-text {
          margin-right: auto;
        }
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page);
