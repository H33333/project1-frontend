import styled from '@emotion/styled';
import { theme, Typography } from 'antd';
import { useState } from 'react';
import { FaEye } from 'react-icons/fa';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Card from 'src/components/card/Card';
import NotificationListItem from 'src/components/card/NotificationListItem';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import StyledListContainer from 'src/components/list/StyledListContainer';
import ModalNotification from 'src/components/ModalNotification';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useDebounce from 'src/hooks/useDebounce';
import withAuth from 'src/hooks/withAuth';
import { useGetListNotificationQuery } from 'src/redux/query/notification.query';
import { TListFilter } from 'src/redux/query/user.query';

const initialFilterValue: TListFilter = { page: 1, limit: 10 };

function Page() {
  const [view, setView] = useState<any>({});
  const [isModalOpen, setIsModalOpen] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { i18n } = useChangeLocale();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [usersFilterValue, setUsersFilterValue] = useState<any>(initialFilterValue);
  const debouncedFilter = useDebounce(usersFilterValue, 500);
  const handleLocalSearch = ({ keyword, sort }: { keyword: string; sort: string }) => {
    setUsersFilterValue({ ...usersFilterValue, page: 1, keyword, sort });
  };

  const {
    data: NotificationQuery,
    isSuccess: getNotificationSuccess,
    isFetching: getNotificationFetching,
  } = useGetListNotificationQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const notificationData = getNotificationSuccess
    ? (NotificationQuery?.data?.notification_list as any) || []
    : [];

  const handleTabChange = (value: string) => {
    setUsersFilterValue({ ...usersFilterValue, status: +value || undefined });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onClickView = (value: any) => {
    setView(value);
    setIsModalOpen(true);
  };

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={i18n['Quản lý thông báo']} />} ref={setContainer}>
      {!mediaAbove767 && (
        <div className='page-header'>
          <Breadcrumb current={i18n['Quản lý thông báo']} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <div>{i18n['Danh sách quản lý thông báo']}</div>
            </div>
          </Typography.Title>
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        // activeTabKey={String(usersFilterValue.status || '')}
        onTabChange={(key) => handleTabChange(key)}
        tabProps={{ tabBarGutter: 20 }}
      >
        <div className='header-filter-container'>
          <LocalSearch<{ keyword: string; sort: string }>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch
                bordered={false}
                placeholder={
                  mediaAbove400 ? i18n['Tìm kiếm theo tiêu đề, mô tả'] : i18n['Tên, Số điện thoại']
                }
              />
            </FormItem>
          </LocalSearch>
        </div>
        <StyledListContainer<any>
          loading={getNotificationFetching}
          dataSource={notificationData}
          target={() => container}
          renderItem={(item, index) => (
            <NotificationListItem
              key={item._id + index}
              {...item}
              index={index}
              actions={
                <div onClick={() => onClickView(item)}>
                  <FaEye size={24} />
                </div>
              }
            />
          )}
        />
      </Card>
      <ModalNotification isModalOpen={isModalOpen} view={view} handleCancel={handleCancel} />
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 24px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page);
