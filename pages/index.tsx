import styled from '@emotion/styled';
import Link from 'next/link';
import { useGetAgencyStatisticQuery } from 'src/redux/query/agency.query';
import { useGetRegisterStatisticQuery } from 'src/redux/query/register.query';
import { HiOutlineBuildingOffice2 } from 'react-icons/hi2';
import { IoMdCar } from 'react-icons/io';
import { MdEmojiPeople, MdEmojiTransportation } from 'react-icons/md';
import PageLayout from 'src/components/layout/PageLayout';
import withAuth from 'src/hooks/withAuth';
import { Chart, ChartType, ChartConfiguration } from 'chart.js/auto';
import { useEffect, useRef, useState } from 'react';
import { useGetTicketStatisticMonthQuery, useGetTicketStatisticQuery } from 'src/redux/query/ticket.query';
import { Bar } from 'react-chartjs-2';

function Page() {
  const { data: agencyStatistic } = useGetAgencyStatisticQuery({});
  const { data: resgisterStatistic } = useGetRegisterStatisticQuery({});

  const chartRef = useRef<Chart>();

  const currentYear = new Date().getFullYear();
  const years = Array.from({ length: 20 }, (_, index) => currentYear - index);

  const [selectedYear, setSelectedYear] = useState<number>(currentYear);

  const { data: ticketStatistic, refetch } = useGetTicketStatisticQuery({
    year: selectedYear !== null ? selectedYear : undefined,
  });
  const { data: ticketStatisticMonth, refetch: refetchTicketStatisticMonth } = useGetTicketStatisticMonthQuery({
    year: selectedYear !== null ? selectedYear : undefined,
  });

  useEffect(() => {
    refetch();
  }, []);

  const handleYearChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const year = parseInt(event.target.value);
    setSelectedYear(year);
    refetch();
    refetchTicketStatisticMonth();
  }

  useEffect(() => {
    if (ticketStatistic && ticketStatistic.data) {
      const ticketStatistics = ticketStatistic?.data;
      const data = {
        labels: ['Mới', 'Đã giao', 'Đang chạy', 'Hoàn thành', 'Hủy'],
        datasets: [
          {
            label: 'Ticket Statistics',
            data: [ticketStatistics.new, ticketStatistics.delevered, ticketStatistics.running, ticketStatistics.completed, ticketStatistics.canceled],
            backgroundColor: [
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };
      const config: ChartConfiguration<ChartType> = {
        type: 'pie',
        data: data,
      };
      const canvas = document.getElementById('ticketStatisticsChart') as HTMLCanvasElement;
      const ctx = canvas.getContext('2d');
      if (ctx !== null) {
        if (chartRef.current) {
          chartRef.current.destroy();
        }
        chartRef.current = new Chart(ctx, config);
      } else {
        console.error('getContext returned null');
      }
    }
  }, [ticketStatistic]);

  const newData = ticketStatisticMonth && ticketStatisticMonth.data ? Object.keys(ticketStatisticMonth.data).map(month => [
    ticketStatisticMonth?.data[parseInt(month)].new,
    ticketStatisticMonth?.data[parseInt(month)].delevered,
    ticketStatisticMonth?.data[parseInt(month)].running,
    ticketStatisticMonth?.data[parseInt(month)].completed,
    ticketStatisticMonth?.data[parseInt(month)].canceled
  ]) : [];
  
  const months = ticketStatisticMonth && ticketStatisticMonth.data ? Object.keys(ticketStatisticMonth.data).map(month => `Tháng ${month}`) : [];
  const columnChartData = {
    labels: months,
    datasets: [{
      label: 'Mới',
      data: newData.map(item => item[0]),
      backgroundColor: 'rgba(54, 162, 235, 0.5)'
    }, {
      label: 'Đã giao',
      data: newData.map(item => item[1]),
      backgroundColor: 'rgba(255, 206, 86, 0.5)'
    }, {
      label: 'Đang chạy',
      data: newData.map(item => item[2]),
      backgroundColor: 'rgba(75, 192, 192, 0.5)'
    }, {
      label: 'Hoàn thành',
      data: newData.map(item => item[3]),
      backgroundColor: 'rgba(153, 102, 255, 0.5)'
    }, {
      label: 'Hủy',
      data: newData.map(item => item[4]),
      backgroundColor: 'rgba(255, 99, 132, 0.5)'
    }]
  };

  return (
    <PageWrapper>
      <Statistic>Thống kê</Statistic>
      <canvas id="ticketStatisticsChart" style={{ width: '400px', height: '400px', maxWidth: '80%', margin: 'auto' }}></canvas>
      <Bar data={columnChartData} options={{ indexAxis: 'x', plugins: { legend: { position: 'right',  display: false } }, scales: { x: { stacked: true }, y: { stacked: true } }}} />
      <select style={{ display: 'block', margin: 'auto', marginTop: '8px'}} value={selectedYear} onChange={handleYearChange}>
        {years.map((year) => (
          <option key={year} value={year}>{year}</option>
        ))}
      </select>
      <Statistic>Tài khoản</Statistic>
      <div className='cards-wrapper'>
        <LinkPath
          className='link'
          href={{
            pathname: '/agency',
            query: {
              page: 1,
              limit: 10,
              isDriver: false,
              isTransportation: false,
              status: 1,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A1 (Đối tác)</div>
            <div className='role-count'>{agencyStatistic?.data?.A1}</div>
            <MdEmojiPeople className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/agency',
            query: {
              page: 1,
              limit: 10,
              isDriver: false,
              isTransportation: true,
              status: 1,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A2 (Công ty vận tải)</div>
            <div className='role-count'>{agencyStatistic?.data?.A2}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/agency',
            query: {
              page: 1,
              limit: 10,
              isDriver: true,
              isTransportation: true,
              status: 1,
              hasCar: 1,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A3 (Lái xe kiêm chủ xe)</div>
            <div className='role-count'>{agencyStatistic?.data?.A3}</div>
            <IoMdCar className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/agency',
            query: {
              page: 1,
              limit: 10,
              isDriver: true,
              isTransportation: false,
              status: 1,
            },
          }}
        >
          <CardWrapper style={{ marginRight: '0px' }}>
            <div className='role-name'>A4 (Lái xe thuê)</div>
            <div className='role-count'>{agencyStatistic?.data?.A4}</div>
            <MdEmojiPeople className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/agency',
            query: {
              page: 1,
              limit: 10,
              isDriver: true,
              isTransportation: true,
              status: 1,
              hasCar: 2,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A5 (Chủ công ty vận tải, đồng thời cũng lái xe)</div>
            <div className='role-count'>{agencyStatistic?.data?.A5}</div>
            <MdEmojiTransportation className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
      </div>
      <Statistic>Đăng ký mới</Statistic>
      <div className='cards-wrapper'>
        <LinkPath
          className='link'
          href={{
            pathname: '/register',
            query: {
              page: 1,
              limit: 10,
              status: 0,
              isDriver: false,
              isTransportation: false,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A1 (Đối tác)</div>
            <div className='role-count'>{resgisterStatistic?.data?.A1}</div>
            <MdEmojiPeople className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/register',
            query: {
              page: 1,
              limit: 10,
              isDriver: false,
              isTransportation: true,
              status: 0,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A2 (Công ty vận tải)</div>
            <div className='role-count'>{resgisterStatistic?.data?.A2}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/register',
            query: {
              page: 1,
              limit: 10,
              isDriver: true,
              isTransportation: true,
              status: 0,
            },
          }}
        >
          <CardWrapper>
            <div className='role-name'>A3 (Lái xe kiêm chủ xe)</div>
            <div className='role-count'>{resgisterStatistic?.data?.A3}</div>
            <IoMdCar className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/register',
            query: {
              page: 1,
              limit: 10,
              isDriver: true,
              isTransportation: false,
              status: 0,
            },
          }}
        >
          <CardWrapper style={{ marginRight: '0px' }}>
            <div className='role-name'>A4 (Lái xe thuê)</div>
            <div className='role-count'>{resgisterStatistic?.data?.A4}</div>
            <MdEmojiPeople className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
      </div>
    </PageWrapper>
  );
}

const Statistic = styled.div`
  color: rgba(0, 0, 0, 0.88);
  font-weight: 600;
  font-size: 36px;
  line-height: 1.5;
  margin: 20px 0;
  padding-bottom: 8px;
  border-bottom: 2px solid #ddd;
`;

const PageWrapper = styled(PageLayout)`
  padding: 24px;
  & > .cards-wrapper {
    --f-columns: 3;
    --f-gap: 24px;
    display: flex;
    flex-wrap: wrap;
    margin-left: calc(-1 * var(--f-gap));
    margin-bottom: calc(-1 * var(--f-gap));
    & > * {
      margin-left: var(--f-gap);
      margin-bottom: var(--f-gap);
      width: calc((100% / var(--f-columns) - var(--f-gap)));
    }
  }
`;

const CardWrapper = styled.main`
  :hover {
    cursor: pointer;
    box-shadow: 5px 5px 5px 5px #ddd;
  }
  border: 1px solid white;
  border-radius: 10px;
  height: 220px;
  width: 30%;
  margin: 1%;
  background-color: white;
  .role-arrow {
    border: 1px blue;
    border-radius: 50%;
    background-color: #dbdbff;
    padding: 5px;
  }
  .role-name {
    margin: 10px 0 0 20px;
    font-size: 24px;
    font-weight: 450;
  }
  .role-avatar {
    position: absolute;
    bottom: 0;
    right: 0;
    margin: 10px 15px;
    padding: 3px;
    float: right;
    color: #5d5dfe;
    background-color: #dbdbff;
    border: 1px solid #dbdbff;
    border-radius: 50%;
    scale: 150%;
  }
  .role-count {
    position: relative;
    font-size: 80px;
    font-weight: 700;
    margin: 0 0 0 20px;
  }
  // .link {
  //   position: absolute;
  //   bottom: 0;
  //   right: 0;
  //   margin: 0 10px 40px 0;
  //   font-size: 30px;
  //   height: 35px;
  // }
`;
const LinkPath = styled(Link)`
  :hover {
    color: black;
  }
  color: black;
`;

export default withAuth(Page);
