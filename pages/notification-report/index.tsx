import styled from '@emotion/styled';
import { Select, Space, theme } from 'antd';
import Link from 'next/link';
import { useState } from 'react';
import { HiOutlineBuildingOffice2 } from 'react-icons/hi2';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import PageLayout from 'src/components/layout/PageLayout';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useDebounce from 'src/hooks/useDebounce';
import withAuth from 'src/hooks/withAuth';
import { useGetListNotificationReportQuery } from 'src/redux/query/notification.query';

const initialFilterValue: any = { year: 2024, month: 0 };

function Page() {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { i18n } = useChangeLocale();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [usersFilterValue, setUsersFilterValue] = useState<any>(initialFilterValue);
  const debouncedFilter = useDebounce(usersFilterValue, 500);

  const {
    data: NotificationReportQuery,
    isSuccess: getNotificationReportSuccess,
    isFetching: getNotificationReportFetching,
  } = useGetListNotificationReportQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const notificationReportData = getNotificationReportSuccess
    ? (NotificationReportQuery?.data as any)
    : [];

  const handleChange = (month: string) => {
    setUsersFilterValue({ ...usersFilterValue, month });
  };

  const currentYear = new Date().getFullYear();
  const years = Array.from({ length: 20 }, (_, index) => currentYear - index);
  const optionSelect = years.map((item: any) => {
    return {
      label: item,
      value: item,
    };
  });

  const handleChangeYear = (year: any) => {
    setUsersFilterValue({ ...usersFilterValue, year });
  };

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={i18n['Quản lý thống kê']} />} ref={setContainer}>
      <Space wrap style={{ marginBottom: '10px' }}>
        <Select
          defaultValue='Month'
          style={{ width: 120 }}
          onChange={handleChange}
          options={[
            { value: 0, label: 'all' },
            { value: 1, label: 'Tháng 1' },
            { value: 2, label: 'Tháng 2' },
            { value: 3, label: 'Tháng 3' },
            { value: 4, label: 'Tháng 4' },
            { value: 5, label: 'Tháng 5' },
            { value: 6, label: 'Tháng 6' },
            { value: 7, label: 'Tháng 7' },
            { value: 8, label: 'Tháng 8' },
            { value: 9, label: 'Tháng 9' },
            { value: 10, label: 'Tháng 10' },
            { value: 11, label: 'Tháng 11' },
            { value: 12, label: 'Tháng 12' },
          ]}
        />
        <Select
          defaultValue='Year'
          style={{ minWidth: 120 }}
          onChange={handleChangeYear}
          options={optionSelect}
        />
      </Space>
      <div className='cards-wrapper'>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo trên toàn hệ thống</div>
            <div className='role-count'>{notificationReportData?.notification_total}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo có tỉ lệ thành công dưới 50%</div>
            <div className='role-count'>{notificationReportData?.notification_peer}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo có tỉ lệ thành công hơn 50%</div>
            <div className='role-count'>{notificationReportData?.notification_poor}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo có tỉ lệ thành công hơn 90%</div>
            <div className='role-count'>{notificationReportData?.notification_stable}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo có tỉ lệ thành công 100%</div>
            <div className='role-count'>{notificationReportData?.notification_success}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
        <LinkPath
          className='link'
          href={{
            pathname: '/notification-report',
          }}
        >
          <CardWrapper>
            <div className='role-name'>Tổng số thông báo có tỉ lệ thành công 0%</div>
            <div className='role-count'>{notificationReportData?.notification_fail}</div>
            <HiOutlineBuildingOffice2 className='role-avatar' size={28} />
          </CardWrapper>
        </LinkPath>
      </div>
    </PageWrapper>
  );
}

const CardWrapper = styled.main`
  :hover {
    cursor: pointer;
    box-shadow: 5px 5px 5px 5px #ddd;
  }
  border: 1px solid white;
  border-radius: 10px;
  height: 220px;
  width: 30%;
  margin: 1%;
  background-color: white;
  .role-arrow {
    border: 1px blue;
    border-radius: 50%;
    background-color: #dbdbff;
    padding: 5px;
  }
  .role-name {
    margin: 10px 0 0 20px;
    font-size: 24px;
    font-weight: 450;
  }
  .role-avatar {
    position: absolute;
    bottom: 0;
    right: 0;
    margin: 10px 15px;
    padding: 3px;
    float: right;
    color: #5d5dfe;
    background-color: #dbdbff;
    border: 1px solid #dbdbff;
    border-radius: 50%;
    scale: 150%;
  }
  .role-count {
    position: relative;
    font-size: 80px;
    font-weight: 700;
    margin: 0 0 0 20px;
  }
  // .link {
  //   position: absolute;
  //   bottom: 0;
  //   right: 0;
  //   margin: 0 10px 40px 0;
  //   font-size: 30px;
  //   height: 35px;
  // }
`;
const LinkPath = styled(Link)`
  :hover {
    color: black;
  }
  color: black;
`;

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  & > .cards-wrapper {
    --f-columns: 3;
    --f-gap: 24px;
    display: flex;
    flex-wrap: wrap;
    margin-left: calc(-1 * var(--f-gap));
    margin-bottom: calc(-1 * var(--f-gap));
    & > * {
      margin-left: var(--f-gap);
      margin-bottom: var(--f-gap);
      width: calc((100% / var(--f-columns) - var(--f-gap)));
    }
  }
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 24px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(Page);
