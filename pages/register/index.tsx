import styled from '@emotion/styled';
import { useAsyncEffect, useDebounce, useSafeState } from 'ahooks';
import { Affix, Card, Divider, theme, Typography } from 'antd';
import { queryTypes, useQueryStates } from 'next-usequerystate';
import { useRouter } from 'next/router';
import { useId } from 'react';
import { BiLogoTelegram } from 'react-icons/bi';
import { BsFillCheckCircleFill } from 'react-icons/bs';
import { useMediaQuery } from 'react-responsive';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import SortDropdown from 'src/components/dropdown/SortDropdown';
import InputSearch from 'src/components/input/InputSearch';
import LocalSearch, { FormItem } from 'src/components/input/LocalSearch';
import PageLayout from 'src/components/layout/PageLayout';
import PaginationMetadata from 'src/components/list/PaginationMetadata';
import StyledTable, { TitleActionsWrapper } from 'src/components/table/StyledTable';
import useRegisterColumns from 'src/components/table/useRegisterColumns';
import useChangeLocale from 'src/hooks/useChangeLocale';
import withAuth from 'src/hooks/withAuth';
import { TListFilter, useGetRegisterListQuery } from 'src/redux/query/register.query';
import { TMetaBase } from 'src/types/response.types';

const initialQueryState = {
  page: queryTypes.float.withDefault(1),
  limit: queryTypes.float.withDefault(10),
  status: queryTypes.integer.withDefault(0),
  isTransportation: queryTypes.boolean,
  isDriver: queryTypes.boolean,
  sort: queryTypes.string,
};
const mappedTabValue = ({
  isTransportation = null,
  isDriver = null,
  status = null,
}: {
  isTransportation: boolean | null;
  isDriver: boolean | null;
  status: number | null;
}) => ({
  isTransportation,
  isDriver,
  status,
});

function RegisterListPage() {
  const uid = useId();
  const { query } = useRouter();
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const { i18n } = useChangeLocale();

  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const mediaAbove400 = useMediaQuery({ minWidth: 400 });
  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);

  const [filterValue, setFilterValue] = useQueryStates(initialQueryState);
  const [filterKeyword, setFilterKeyword] = useSafeState<string>();
  const debouncedFilterKeyword = useDebounce(filterKeyword, { wait: 500 });

  const [selectedItems, setSelectedItems] = useSafeState<string[]>([]);
  const {
    data: registersFilteredQuery,
    isSuccess: getRegistersSuccess,
    isFetching: getRegistersFetching,
  } = useGetRegisterListQuery(
    { ...filterValue, keyword: debouncedFilterKeyword },
    { refetchOnMountOrArgChange: true },
  );
  const registersFilteredData = getRegistersSuccess
    ? registersFilteredQuery?.data?.register_list || []
    : [];
  const registersFilteredMetaData: TMetaBase | undefined = getRegistersSuccess
    ? registersFilteredQuery?.data?.meta_data
    : undefined;

  const columns = useRegisterColumns();

  const handleLocalSearch = async ({ keyword, sort }: Pick<TListFilter, 'keyword' | 'sort'>) => {
    setFilterKeyword(keyword);
    await setFilterValue({ ...filterValue, page: 1, sort });
  };

  const handleTabChange = async (key: string) => {
    const mappedFilter = JSON.parse(key) as any;
    const newFilter = { ...filterValue, ...mappedFilter };
    await setFilterValue(newFilter);
    setSelectedItems([]);
  };

  useAsyncEffect(async () => {
    if (!Object.keys(query).length) {
      await setFilterValue({
        page: 1,
        limit: 10,
        isDriver: false,
        isTransportation: false,
        status: 0,
      });
    }
  }, [query]);

  return (
    <PageWrapper breadcrumb={<Breadcrumb current={i18n['Đối tác đăng ký']} />} ref={setContainer}>
      {!mediaAbove767 && (
        <div className='page-header'>
          <Breadcrumb current={i18n['Đối tác đăng ký']} />
        </div>
      )}
      <Card
        title={
          <Typography.Title className='title' level={3} style={{ margin: 0 }}>
            {i18n['Danh sách đối tác đăng ký']}
          </Typography.Title>
        }
        style={{ width: '100%' }}
        bodyStyle={{ padding: 0 }}
        tabList={[
          {
            key: JSON.stringify({
              isTransportation: false,
              isDriver: false,
              status: 0,
            }),
            tab: i18n['Đối tác du lịch (A1)'],
          },
          {
            key: JSON.stringify({
              isTransportation: true,
              isDriver: false,
              status: 0,
            }),
            tab: i18n['Công ty vận tải (A2)'],
          },
          {
            key: JSON.stringify({ isTransportation: true, isDriver: true, status: 0 }),
            tab: i18n['Lái xe kiêm chủ xe (A3)'],
          },
          {
            key: JSON.stringify({
              isTransportation: false,
              isDriver: true,
              status: 0,
            }),
            tab: i18n['Lái xe thuê (A4)'],
          },
        ]}
        activeTabKey={JSON.stringify(mappedTabValue(filterValue))}
        onTabChange={(key) => handleTabChange(key)}
        tabProps={{ tabBarGutter: 20 }}
      >
        <div className='header-filter-container'>
          <LocalSearch<Pick<TListFilter, 'keyword' | 'sort'>>
            onFinish={handleLocalSearch}
            onValuesChange={(changedValue, values) => handleLocalSearch(values)}
          >
            <FormItem name='keyword'>
              <InputSearch
                bordered={false}
                placeholder={
                  mediaAbove400
                    ? i18n['Tìm kiếm theo Tên, Số điện thoại']
                    : i18n['Tên, Số điện thoại']
                }
              />
            </FormItem>
            <Divider type='vertical' style={{ height: 22 }} />
            <FormItem name='sort'>
              <SortDropdown
                items={[
                  { key: 'name_asc', label: i18n['Tên A->Z'] },
                  { key: 'name_desc', label: i18n['Tên Z->A'] },
                  { key: 'createdAt_desc', label: i18n['Ngày tạo mới nhất'] },
                  { key: 'createdAt_asc', label: i18n['Ngày tạo cũ nhất'] },
                ]}
              />
            </FormItem>
          </LocalSearch>
        </div>
        <StyledTable
          className='hide-title-border has-title-actions'
          scroll={{ x: true }}
          loading={getRegistersFetching}
          dataSource={registersFilteredData}
          columns={[
            {
              ...columns.nameCombined1,
              width: 240,
              title: !!selectedItems.length ? (
                <TitleActionsWrapper>
                  <span>
                    {i18n['Đã chọn']} <u>{selectedItems.length}</u>
                  </span>
                  <div className='actions-wrapper'>
                    <div className='actions'>
                      <Button size='small' icon={<BsFillCheckCircleFill size={12} />}>
                        {i18n['Xác nhận']}
                      </Button>
                      <Button size='small' icon={<BiLogoTelegram size={14} />}>
                        {i18n['Gửi mã đăng nhập']}
                      </Button>
                    </div>
                  </div>
                </TitleActionsWrapper>
              ) : (
                ''
              ),
            },
            columns.type,
            { ...columns.code, align: 'right', ellipsis: true },
            { ...columns.actions, width: 160, fixed: 'right' },
          ]}
          style={{ padding: '0 16px' }}
          rowKey={(item: any) => item._id}
          pagination={false}
          showHeader={!!selectedItems.length}
          size='middle'
          rowSelection={{
            selectedRowKeys: selectedItems,
            onChange: (selectedRowKeys, selectedRows) => {
              setSelectedItems(selectedRowKeys as any[]);
            },
          }}
        />
        <Affix offsetBottom={0.01} target={() => container}>
          <PaginationMetadata
            showLessItems
            metadata={registersFilteredMetaData}
            onChange={(page, pageSize) => setFilterValue({ ...filterValue, page, limit: pageSize })}
          />
        </Affix>
      </Card>
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px 24px;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  .page-header {
    padding: 24px 0;
  }
  .ant-card {
    .ant-card-head {
      .ant-card-head-wrapper > * {
        padding-top: 20px;
      }
    }
    .ant-card-body {
      .header-filter-container {
        width: 100%;
        padding: 24px 24px 12px;
        .ant-space-item:first-of-type {
          flex: 1 1 auto;
        }
      }
      .ant-list {
        padding: 0 24px 0 28px;
      }
      .ant-pagination {
        padding: 20px 24px;
        display: flex;
        align-items: center;
        border-radius: 0 0 8px 8px;
        .ant-pagination-total-text {
          margin-right: auto;
        }
      }
    }
  }
  @media screen and (max-width: 767.98px) {
    padding: 0 12px 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      .ant-card-head {
        .ant-card-head-wrapper > * {
          padding-top: 24px;
        }
      }
    }
  }

  @media screen and (max-width: 400.98px) {
    padding: 0 0 12px;
    .page-header {
      padding: 16px 12px;
    }
    .ant-card {
      border: none !important;
      border-radius: 0;
      .ant-card-head {
        padding: 0 12px;
        .ant-card-head-wrapper > * {
          padding-top: 12px;
        }
      }
      .ant-card-body {
        .header-filter-container {
          padding: 12px 12px 0px;
        }
        .ant-list {
          padding: 0 12px 0 14px;
        }
      }
    }
  }
`;

export default withAuth(RegisterListPage);
