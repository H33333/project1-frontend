import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import { useDebounce, useSafeState } from 'ahooks';
import {
  Alert,
  Card,
  Checkbox,
  Divider,
  Dropdown,
  Empty,
  Form,
  Input,
  InputNumber,
  Space,
  Tabs,
  Tour,
  Typography,
} from 'antd';
import { queryTypes, useQueryState } from 'next-usequerystate';
import { useRouter } from 'next/router';
import React, { useEffect, useId, useRef, useState } from 'react';
import { BsArrowUp, BsPersonFill, BsStar, BsThreeDots } from 'react-icons/bs';
import { GiCheckboxTree } from 'react-icons/gi';
import { IoMdCar } from 'react-icons/io';
import { MdEmojiTransportation, MdMyLocation } from 'react-icons/md';
import { TbTrash } from 'react-icons/tb';
import { useMediaQuery } from 'react-responsive';
import TypeAgencyAvatar from 'src/components/avatar/TypeAgencyAvatar';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import CarListItem from 'src/components/card/CarListItem';
import DriverListItem from 'src/components/card/DriverListItem';
import FilterSearchCompact from 'src/components/field/FilterSearchCompact';
import MapPicker from 'src/components/field/MapPicker';
import SegmentedStatus from 'src/components/field/SegmentedStatus';
import CreateCarModal from 'src/components/form/CreateCarModal';
import CreateDriverModal from 'src/components/form/CreateDriverModal';
import UpdateGpsCarsModal from 'src/components/form/UpdateGpsCarsModal';
import { CoinAltIcon, CoinIcon } from 'src/components/icons';
import InputCode from 'src/components/input/InputCode';
import InputSearch from 'src/components/input/InputSearch';
import PageLayout from 'src/components/layout/PageLayout';
import Container from 'src/components/shared/Container';
import {
  BreadcrumbNodeWrapper,
  cssCheckboxTypeGroup,
  cssTabDriverCar,
} from 'src/components/shared/ItemStyled';
import SelectedCarTag from 'src/components/tag/SelectedCarTag';
import Tag from 'src/components/tag/Tag';
import TimeText, { TimeTextPopover } from 'src/components/text/TimeText';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useChangeStatusAgency from 'src/hooks/useChangeStatusAgency';
import useModalConfirm from 'src/hooks/useModalConfirm';
import withAuth from 'src/hooks/withAuth';
import {
  useGetAgencyDetailQuery,
  useUpdateAgencyDetailMutation,
} from 'src/redux/query/agency.query';
import { TCarsFilter, useDeleteCarMutation, useGetCarListQuery } from 'src/redux/query/car.query';
import {
  TDriversFilter,
  useDeleteDriverMutation,
  useDeliverDriverMutation,
  useGetDriverListQuery,
  useUnDeliverDriverMutation,
} from 'src/redux/query/driver.query';
import { TCar } from 'src/types/car.types';
import { TDriver } from 'src/types/driver.types';
import { ErrorCode } from 'src/types/response.types';
import { getRandomInt } from 'src/utils/utils';
import { dayjs } from 'src/utils/utils-date';
import { mappedErrorToFormError } from 'src/utils/utils-error';

function AgencyDetailPage() {
  const uid = useId();
  const {
    query: { agencyId },
  } = useRouter();
  const { i18n } = useChangeLocale();
  const { generatedColors } = useTheme();
  const { notification } = useApp();
  const mediaAbove1023 = useMediaQuery({ minWidth: 1023 });
  const [selectedTab, setSelectedTab] = useQueryState(
    'tab',
    queryTypes.string.withDefault('Profile'),
  );
  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);
  const [form] = Form.useForm();
  const isTransportationWatch = Form.useWatch('isTransportation', form);
  const isDriverWatch = Form.useWatch('isDriver', form);

  const { handleChangeStatus, blockLoading, unBlockLoading } = useChangeStatusAgency();
  const { isFetching, refetch, data } = useGetAgencyDetailQuery(String(agencyId), {
    skip: !agencyId,
    refetchOnMountOrArgChange: true,
  });
  const agencyData = data?.data.agency_detail;
  const mappedData = {
    ...agencyData,
    geoLocation:
      agencyData?.lat_address && agencyData?.long_address
        ? {
            lat: +agencyData.lat_address,
            lng: +agencyData.long_address,
          }
        : undefined,
  };

  const [updateAgency, { isLoading }] = useUpdateAgencyDetailMutation();
  const handleUpdateInfo = ({ geoLocation, _id, ...formData }: any) => {
    const newFormData = {
      ...formData,
      lat_address: geoLocation?.lat ? String(geoLocation.lat) : '',
      long_address: geoLocation?.lng ? String(geoLocation.lng) : '',
    };
    !!_id &&
      updateAgency({ id: _id, data: newFormData })
        .unwrap()
        .then((res) => {
          notification.success({ message: res.message, placement: 'bottomRight' });
        })
        .catch((err) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
          if (err.response_code === ErrorCode.RequestValidationError) {
            form.setFields(mappedErrorToFormError(err.error));
          }
        });
  };
  const handleReset = () => {
    form.resetFields();
  };

  const [avatarUrl, setAvatarUrl] = useState<string>('');

  const handleAvatarUrlChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAvatarUrl(String(e.target.value));
  };

  const [selectedDriver, setSelectedDriver] = useSafeState<TDriver>();
  const [selectedDrivers, setSelectedDrivers] = useSafeState<string[]>([]);
  const [activeTabKey, setActiveTabKey] = useSafeState<string>('driver-list');
  const [filterValue, setFilterValue] = useSafeState<TDriversFilter>({ page: 1, keyword: '' });
  const debouncedFilter = useDebounce(filterValue, { wait: 500 });
  const { isFetching: isFetchingGetDriverList, data: getDriverListRes } = useGetDriverListQuery(
    !!agencyId ? { ...debouncedFilter, agency: agencyId as string } : skipToken,
    {
      refetchOnMountOrArgChange: true,
    },
  );
  const driverList = getDriverListRes?.data?.driver_list || [];
  const driverCarListIds = driverList.map((d) => (d.car_id as any)?._id).filter((i) => !!i) || [];
  const driverListMetaData = getDriverListRes?.data?.meta_data || undefined;

  const carListRef = useRef<HTMLDivElement>(null);
  const [openSelectedCar, setOpenSelectedCar] = useSafeState<boolean>(false);
  const [selectedCar, setSelectedCar] = useSafeState<TCar>();
  const [selectedCars, setSelectedCars] = useSafeState<string[]>([]);
  const [filterCarsValue, setFilterCarsValue] = useSafeState<TCarsFilter>({ page: 1, keyword: '' });
  const debouncedFilterCars = useDebounce(filterCarsValue, { wait: 500 });
  const { data: getCarListRes } = useGetCarListQuery(
    !!agencyId ? { ...debouncedFilterCars, agency: agencyId as string } : skipToken,
    {
      refetchOnMountOrArgChange: true,
    },
  );
  const carList = getCarListRes?.data?.car_list || [];
  const carListMetaData = getCarListRes?.data?.meta_data || undefined;
  const { data: getAllCarListRes } = useGetCarListQuery(
    !!agencyId ? { page: 1, agency: agencyId as string } : skipToken,
  );
  const { data: getAllDriverListRes } = useGetDriverListQuery(
    !!agencyId ? { page: 1, agency: agencyId as string } : skipToken,
  );
  const { carIcon } = useCarType();

  const [deliverMutate, { isLoading: isLoadingDeliver }] = useDeliverDriverMutation();
  const [unDeliverMutate, { isLoading: isLoadingUndeliver }] = useUnDeliverDriverMutation();
  const [deleteDriverMutate, {}] = useDeleteDriverMutation();
  const [deleteCarMutate, {}] = useDeleteCarMutation();
  const handleDeliverDriver = (driverId?: string, carId?: string) => {
    if (!!driverId && !!carId)
      deliverMutate({ driverId, car: carId })
        .unwrap()
        .then(({ data, message }) => {
          notification.success({ message, placement: 'bottomRight' });
          setSelectedCars([]);
          setSelectedDriver(undefined);
        })
        .catch((err) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
  };
  const { handleConfirm: handleConfirmUnDeliver } = useModalConfirm({
    onOk: (id) => {
      unDeliverMutate(id)
        .unwrap()
        .then(({ data, message }) => {
          notification.success({ message, placement: 'bottomRight' });
        })
        .catch((err) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
    },
  });
  const { handleConfirm: handleConfirmDeleteDriver } = useModalConfirm({
    onOk: (id) => {
      deleteDriverMutate(id)
        .unwrap()
        .then(({ data, message }) => {
          // dispatch(carApi.util.invalidateTags([{ type: 'Cars', id: 'LIST' }]));
          notification.success({ message, placement: 'bottomRight' });
        })
        .catch((err) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
    },
  });
  const { handleConfirm: handleConfirmDeleteCar } = useModalConfirm({
    onOk: (id) => {
      deleteCarMutate(id)
        .unwrap()
        .then(({ data, message }) => {
          notification.success({ message, placement: 'bottomRight' });
        })
        .catch((err) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
    },
  });

  const [selectedItemId, setSelectedItemId] = useSafeState<string>();

  useEffect(() => {
    form.resetFields();
  }, [agencyData]);

  useEffect(() => {
    if (agencyData?.agency_avatar !== undefined) {
      setAvatarUrl(agencyData?.agency_avatar);
    }
  }, [agencyData]);

  return (
    <PageWrapper
      ref={setContainer}
      breadcrumb={
        <Breadcrumb
          style={{ marginTop: 7 }}
          items={{
            ['/agency']: i18n['Đối tác'],
            [`/agency/${agencyId}`]: {
              node: (
                <BreadcrumbNodeWrapper>
                  <div className='left-wrapper'>
                    {agencyData && (
                      <div className='basic-wrapper'>
                        <TypeAgencyAvatar
                          size={24}
                          isActive={agencyData.status === 1}
                          isDriver={agencyData.isDriver}
                          isTransportation={agencyData.isTransportation}
                        ></TypeAgencyAvatar>
                        <div className='detail-wrapper'>
                          <Typography.Title className='name' level={2} ellipsis>
                            {agencyData.name}
                          </Typography.Title>
                          <Typography.Text type='secondary' className='phone'>
                            {agencyData.phone}
                          </Typography.Text>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className='right-wrapper'>
                    <Tabs
                      activeKey={selectedTab}
                      onChange={(t) => setSelectedTab(t)}
                      size='small'
                      style={{
                        background: 'transparent',
                      }}
                      tabBarStyle={{
                        marginBottom: 0,
                        padding: 0,
                      }}
                      tabBarGutter={16}
                      items={
                        [
                          {
                            key: 'Profile',
                            label: i18n['Thông tin cá nhân'],
                            disabled: isFetching || isLoading,
                          },
                          !!agencyData?.isTransportation
                            ? {
                                key: 'Driver_Car',
                                label: (
                                  <div css={cssTabDriverCar}>
                                    <span className='text'>{i18n['Xe và Lái xe']}</span>
                                    <Tag
                                      color='blue'
                                      icon={<IoMdCar size={15} />}
                                      className='tag-car'
                                    >
                                      {String(getAllCarListRes?.data?.meta_data.total || 0)}
                                    </Tag>
                                    <Tag
                                      color='geekblue'
                                      icon={<BsPersonFill size={14} />}
                                      className='tag-driver'
                                    >
                                      {String(getAllDriverListRes?.data?.meta_data.total || 0)}
                                    </Tag>
                                  </div>
                                ),
                                disabled: isFetching || isLoading,
                              }
                            : undefined,
                        ].filter((i) => !!i) as any
                      }
                    />
                  </div>
                </BreadcrumbNodeWrapper>
              ),
            },
          }}
        />
      }
    >
      {!mediaAbove1023 && (
        <div className='page-header'>
          <div className='breadcrumb-wrapper' style={{ margin: '16px 0 16px 0' }}>
            <Breadcrumb items={{ ['/agency']: i18n['Đối tác'] }} current={agencyData?.name} />
          </div>
          <BreadcrumbNodeWrapper>
            <div className='left-wrapper'>
              {agencyData && (
                <div className='basic-wrapper'>
                  <TypeAgencyAvatar
                    size={24}
                    isActive={agencyData.status === 1}
                    isDriver={agencyData.isDriver}
                    isTransportation={agencyData.isTransportation}
                  ></TypeAgencyAvatar>
                  <div className='detail-wrapper'>
                    <Typography.Title className='name' level={2} ellipsis>
                      {agencyData.name}
                    </Typography.Title>
                    <Typography.Text type='secondary' className='phone'>
                      {agencyData.phone}
                    </Typography.Text>
                  </div>
                </div>
              )}
            </div>
            <div className='right-wrapper'>
              <Tabs
                activeKey={selectedTab}
                onChange={(t) => setSelectedTab(t)}
                size='small'
                style={{
                  background: 'transparent',
                }}
                tabBarStyle={{
                  marginBottom: 0,
                  padding: 0,
                }}
                tabBarGutter={16}
                items={
                  [
                    {
                      key: 'Profile',
                      label: i18n['Thông tin cá nhân'],
                      disabled: isFetching || isLoading,
                    },
                    !!agencyData?.isTransportation
                      ? {
                          key: 'Driver_Car',
                          label: (
                            <div css={cssTabDriverCar}>
                              <span className='text'>{i18n['Xe và Lái xe']}</span>
                              <Tag color='blue' icon={<IoMdCar size={15} />} className='tag-car'>
                                {String(getAllCarListRes?.data?.meta_data.total || 0)}
                              </Tag>
                              <Tag
                                color='geekblue'
                                icon={<BsPersonFill size={14} />}
                                className='tag-driver'
                              >
                                {String(getAllDriverListRes?.data?.meta_data.total || 0)}
                              </Tag>
                            </div>
                          ),
                          disabled: isFetching || isLoading,
                        }
                      : undefined,
                  ].filter((i) => !!i) as any
                }
              />
            </div>
          </BreadcrumbNodeWrapper>
        </div>
      )}
      {selectedTab === 'Profile' && (
        <Form
          form={form}
          layout='vertical'
          size='large'
          onFinish={handleUpdateInfo}
          disabled={isFetching || isLoading || selectedTab !== 'Profile'}
          autoComplete='off'
          requiredMark={false}
          initialValues={mappedData}
          className='profile-form'
        >
          <Form.Item name='_id' hidden>
            <Input />
          </Form.Item>
          <div className='profile-wrapper'>
            <div className='left-wrapper'>
              <Card style={{ marginBottom: 24 }}>
                <Form.Item
                  name='name'
                  label={i18n['Họ & tên']}
                  rules={[{ required: true, message: '• Name is required' }]}
                >
                  <Input type='text' placeholder={i18n['Nhập Họ và tên']} />
                </Form.Item>
                <Form.Item
                  name='phone'
                  label={i18n['Số điện thoại']}
                  rules={[{ required: true, message: '• Phone is required' }]}
                >
                  <Input type='tel' placeholder={i18n['Nhập Số điện thoại']} />
                </Form.Item>
              </Card>

              <Card style={{ marginBottom: 24 }}>
                <Form.Item name='agency_avatar' label={i18n['Avatar']}>
                  <Input
                    type='text'
                    placeholder={i18n['Nhập URL avatar']}
                    value={avatarUrl}
                    onChange={handleAvatarUrlChange}
                  />
                </Form.Item>
                {avatarUrl && (
                  <div>
                    <p>Preview Avatar</p>
                    <PreviewAvatar src={avatarUrl} alt='URL sai' />
                  </div>
                )}
              </Card>

              <Card style={{ marginBottom: 24 }}>
                <Form.Item name='address' label={i18n['Địa chỉ chi tiết']}>
                  <Input.TextArea
                    placeholder={i18n['Lấy gợi ý từ bản đồ hoặc Nhập địa chỉ chi tiết']}
                    autoSize={{ minRows: 2 }}
                    showCount
                  />
                </Form.Item>
                <Form.Item label={i18n['Tọa độ địa chỉ']} name={'geoLocation'}>
                  <MapPicker
                    style={{ height: 180 }}
                    draggable
                    onChangeAddressName={(v) => form.setFieldValue('address', v)}
                  />
                </Form.Item>
              </Card>

              <Card>
                <Form.Item
                  name='code'
                  label={i18n['Mã đăng nhập']}
                  rules={[
                    {
                      required: true,
                      type: 'string',
                      whitespace: false,
                      message: '• Code is required',
                    },
                    {
                      min: 6,
                      message: '• Code has at least 06 characters',
                    },
                  ]}
                >
                  <InputCode
                    onClickGenerate={() => {
                      form.setFieldValue('code', String(getRandomInt()));
                    }}
                  />
                </Form.Item>
              </Card>

              {!mediaAbove1023 && (
                <Card style={{ marginTop: 24 }}>
                  <Form.Item
                    label={i18n['Chức năng, vai trò']}
                    required
                    help=''
                    style={{ marginBottom: 12 }}
                  >
                    <div css={cssCheckboxTypeGroup}>
                      <Form.Item
                        name='isDriver'
                        valuePropName='checked'
                        noStyle
                        rules={[{ type: 'boolean', message: '• isDriver is invalid' }]}
                      >
                        <Checkbox className='checkbox-item'>
                          <IoMdCar size={20} />
                          <span>{i18n['Lái xe']}</span>
                        </Checkbox>
                      </Form.Item>
                      <Form.Item
                        name='isTransportation'
                        valuePropName='checked'
                        noStyle
                        rules={[{ type: 'boolean', message: '• isTransportation is invalid' }]}
                      >
                        <Checkbox className='checkbox-item'>
                          <MdEmojiTransportation size={24} />
                          <span>{i18n['Chức năng vận tải']}</span>
                        </Checkbox>
                      </Form.Item>
                    </div>
                    {!!isTransportationWatch && (
                      <Alert
                        style={{ marginTop: 12 }}
                        banner
                        type='info'
                        message='Có thể quản lý Xe và Lái xe'
                      />
                    )}
                  </Form.Item>
                  <Divider />
                  <Form.Item name='coin' label={'Coin'}>
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={<CoinIcon width={20} height={20} style={{ marginLeft: -3 }} />}
                    />
                  </Form.Item>
                  <Form.Item name='point' label={i18n['Điểm']}>
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={
                        <CoinAltIcon
                          width={20}
                          height={20}
                          style={{ marginLeft: -3 }}
                          color={generatedColors[6]}
                        />
                      }
                    />
                  </Form.Item>
                  <Divider />
                  <Form.Item
                    name='rank'
                    label={i18n['Hạng']}
                    tooltip={{
                      title: i18n['Xếp hạng đối tác trong danh sách tìm kiếm'],
                      arrow: false,
                      placement: 'rightBottom',
                    }}
                  >
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={<BsStar />}
                    />
                  </Form.Item>
                </Card>
              )}

              <Divider />
              <div className='actions-wrapper' style={{ marginBottom: 24 }}>
                <Button htmlType='button' block disabled={isLoading} onClick={() => handleReset()}>
                  {i18n['Hủy']}
                </Button>
                <Button
                  htmlType='submit'
                  block
                  loading={isLoading}
                  type='primary'
                  icon={<BsArrowUp />}
                >
                  {i18n['Cập nhật']}
                </Button>
              </div>
            </div>
            {mediaAbove1023 && (
              <div className='right-wrapper'>
                <Card>
                  {!!agencyData && (
                    <>
                      <SegmentedStatus
                        status={agencyData.status}
                        disabled={blockLoading || unBlockLoading}
                        onChange={(v) => handleChangeStatus(agencyData._id, v as string)}
                      />
                      <Divider />
                    </>
                  )}
                  <Form.Item
                    label={i18n['Chức năng, vai trò']}
                    required
                    help=''
                    style={{ marginBottom: 12 }}
                  >
                    <div css={cssCheckboxTypeGroup}>
                      <Form.Item
                        name='isDriver'
                        valuePropName='checked'
                        noStyle
                        rules={[{ type: 'boolean', message: '• isDriver is invalid' }]}
                      >
                        <Checkbox className='checkbox-item'>
                          <IoMdCar size={20} />
                          <span>{i18n['Lái xe']}</span>
                        </Checkbox>
                      </Form.Item>
                      <Form.Item
                        name='isTransportation'
                        valuePropName='checked'
                        noStyle
                        rules={[{ type: 'boolean', message: '• isTransportation is invalid' }]}
                      >
                        <Checkbox className='checkbox-item'>
                          <MdEmojiTransportation size={24} />
                          <span>{i18n['Chức năng vận tải']}</span>
                        </Checkbox>
                      </Form.Item>
                    </div>
                    {!!isTransportationWatch && (
                      <Alert
                        style={{ marginTop: 12 }}
                        banner
                        type='info'
                        message='Có thể quản lý Xe và Lái xe'
                      />
                    )}
                  </Form.Item>
                  <Divider />
                  <Form.Item name='coin' label={'Coin'}>
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={<CoinIcon width={20} height={20} style={{ marginLeft: -3 }} />}
                    />
                  </Form.Item>
                  <Form.Item name='point' label={i18n['Điểm']}>
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={
                        <CoinAltIcon
                          width={20}
                          height={20}
                          style={{ marginLeft: -3 }}
                          color={generatedColors[6]}
                        />
                      }
                    />
                  </Form.Item>
                  <Divider />
                  <Form.Item
                    name='rank'
                    label={i18n['Hạng']}
                    tooltip={{
                      title: i18n['Xếp hạng đối tác trong danh sách tìm kiếm'],
                      arrow: false,
                      placement: 'rightBottom',
                    }}
                  >
                    <InputNumber
                      step={1}
                      style={{ width: '100%' }}
                      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                      prefix={<BsStar />}
                    />
                  </Form.Item>
                </Card>
                {!!agencyData && (
                  <div className='bottom-wrapper'>
                    <Typography.Paragraph type='secondary'>
                      Ngày tạo: {dayjs(agencyData.createdAt).format('DD/MM/YYYY HH:mm')}
                    </Typography.Paragraph>
                    <Typography.Paragraph type='secondary'>
                      Ngày cập nhật: {dayjs(agencyData.updatedAt).format('DD/MM/YYYY HH:mm')}
                    </Typography.Paragraph>
                  </div>
                )}
              </div>
            )}
          </div>
        </Form>
      )}
      {selectedTab === 'Driver_Car' && (
        <>
          <div className='driver-car-wrapper'>
            <div className='left-wrapper'>
              <div className='header-wrapper'>
                <Typography.Title
                  level={3}
                  type='secondary'
                  style={{ margin: '0 0 -4px', lineHeight: 1, fontSize: 24 }}
                >
                  Danh sách lái xe
                </Typography.Title>
                <Tabs
                  size={'large'}
                  activeKey={activeTabKey}
                  onChange={setActiveTabKey}
                  tabBarGutter={16}
                  tabBarStyle={{ marginBottom: 6 }}
                  items={[
                    {
                      key: 'driver-list',
                      label: `Tất cả`,
                    },
                    {
                      key: 'driver-delivered',
                      label: `Đang lái`,
                    },
                    {
                      key: 'driver-undelivered',
                      label: `Chưa lái`,
                    },
                  ]}
                  tabBarExtraContent={
                    <FilterSearchCompact
                      placeholder='Tìm kiếm xe...'
                      size='middle'
                      block
                      extra={<CreateDriverModal buttonProps={{ size: 'middle' }} />}
                    />
                  }
                />
              </div>
              {!!driverList.length ? (
                <>
                  <div className='container-header'>
                    <Typography.Text>
                      Đã chọn{' '}
                      <Typography.Text underline strong>
                        {selectedDrivers.length}
                      </Typography.Text>{' '}
                      lái xe
                    </Typography.Text>
                    <Divider type='vertical' style={{ margin: '0 0 0 8px' }} />
                    <Button type='text' size='small' icon={<MdMyLocation size={16} />}>
                      Cập nhật vị trí
                    </Button>
                  </div>
                  <Container className='container'>
                    <Checkbox.Group
                      value={selectedDrivers}
                      onChange={(checkedValue) => {
                        setSelectedDrivers((checkedValue || []) as string[]);
                      }}
                    >
                      {driverList.map((item, index) => (
                        <DriverListItem
                          key={uid + item._id + 'driverList' + index}
                          actions={
                            <Space.Compact>
                              {!item.car_id && (
                                <Button
                                  loading={openSelectedCar && selectedDriver?._id === item._id}
                                  type='text'
                                  style={{
                                    height: 26,
                                    padding: '0 10px',
                                    fontSize: 13,
                                  }}
                                  icon={<GiCheckboxTree size={16} />}
                                  onClick={() => {
                                    setOpenSelectedCar(true);
                                    setSelectedDriver(item);
                                    setSelectedCars([]);
                                  }}
                                >
                                  Giao xe
                                </Button>
                              )}
                              <Dropdown
                                placement='bottomRight'
                                trigger={['click']}
                                mouseEnterDelay={0.01}
                                mouseLeaveDelay={0.01}
                                menu={{
                                  items: [
                                    !!item.car_id
                                      ? {
                                          key: 'undeliver',
                                          label: 'Hủy giao xe',
                                          danger: true,
                                          icon: <GiCheckboxTree size={16} />,
                                          onClick: () =>
                                            handleConfirmUnDeliver(
                                              item._id,
                                              item.name,
                                              'hủy giao xe',
                                            ),
                                        }
                                      : {
                                          key: 'delete',
                                          label: 'Xóa lái xe',
                                          danger: true,
                                          icon: <TbTrash size={16} />,
                                          onClick: () =>
                                            handleConfirmDeleteDriver(item._id, item.name),
                                        },
                                  ].filter((i) => !!i) as any,
                                }}
                              >
                                <Button
                                  type='text'
                                  style={{
                                    height: 26,
                                  }}
                                  icon={<BsThreeDots size={18} />}
                                ></Button>
                              </Dropdown>
                            </Space.Compact>
                          }
                          actionsBottom={
                            mediaAbove1023 ? (
                              <TimeText
                                underline
                                {...item}
                                {...(mediaAbove1023
                                  ? { onClick: () => setSelectedItemId(item._id) }
                                  : {})}
                              />
                            ) : (
                              <TimeTextPopover {...item} />
                            )
                          }
                          {...item}
                        ></DriverListItem>
                      ))}
                    </Checkbox.Group>
                  </Container>
                </>
              ) : (
                <div className='container-empty'>
                  <Empty style={{ borderRadius: 8 }} description={false} />
                </div>
              )}
            </div>
            <div className='right-wrapper'>
              <div className='container' ref={carListRef}>
                <div className='header-wrapper'>
                  <InputSearch size='large' placeholder='Tìm kiếm xe...' transparent />
                </div>
                {!!carList.length ? (
                  <>
                    <div className='container-header'>
                      <Typography.Text>
                        Đã chọn{' '}
                        <Typography.Text underline strong>
                          {selectedCars.length}
                        </Typography.Text>{' '}
                        xe
                      </Typography.Text>
                      <Divider type='vertical' style={{ margin: '0 0 0 8px' }} />
                      <UpdateGpsCarsModal
                        initialValues={carList
                          .filter(({ _id }) => selectedCars.includes(_id))
                          .map((item) => {
                            return {
                              _id: item._id,
                              name: item.name,
                              plates: item.plates,
                              type: item.type,
                              geoLocation:
                                !!item?.lat && !!item?.long
                                  ? {
                                      lat: +item.lat,
                                      lng: +item.long,
                                    }
                                  : undefined,
                            };
                          })}
                        buttonProps={{
                          type: 'text',
                          size: 'small',
                          icon: <MdMyLocation size={16} />,
                          disabled: !selectedCars.length,
                        }}
                      />
                    </div>
                    <Container className='container'>
                      <Checkbox.Group
                        value={selectedCars}
                        onChange={(checkedValue) => {
                          const newValue = (checkedValue || []) as string[];
                          if (openSelectedCar) {
                            const last = newValue.at(-1);
                            setSelectedCars(!!last ? [last] : []);
                          } else {
                            setSelectedCars(newValue);
                          }
                        }}
                      >
                        {carList.map((item, index) => {
                          const isDriven = driverCarListIds.includes(item._id);
                          return (
                            <CarListItem
                              style={{ opacity: isDriven ? 0.5 : 1, padding: '0 12px 12px' }}
                              disabled={isDriven}
                              key={uid + item._id + 'carList' + index + String(selectedCars)}
                              carIcon={carIcon(item.type)}
                              actions={
                                <Dropdown
                                  placement='bottomRight'
                                  trigger={['click']}
                                  mouseEnterDelay={0.01}
                                  mouseLeaveDelay={0.01}
                                  menu={{
                                    items: [
                                      !!isDriven
                                        ? undefined
                                        : {
                                            key: 'delete',
                                            label: 'Xóa xe',
                                            danger: true,
                                            icon: <TbTrash size={16} />,
                                            onClick: () =>
                                              handleConfirmDeleteCar(item._id, item.name),
                                          },
                                    ].filter((i) => !!i) as any,
                                  }}
                                >
                                  <Button
                                    type='text'
                                    style={{
                                      backgroundColor: 'rgba(0,0,0,0.05)',
                                      height: 28,
                                    }}
                                    icon={<BsThreeDots size={18} />}
                                  ></Button>
                                </Dropdown>
                              }
                              actionsBottom={<TimeTextPopover {...item} />}
                              {...item}
                            ></CarListItem>
                          );
                        })}
                      </Checkbox.Group>
                    </Container>
                  </>
                ) : (
                  <div className='container-empty'>
                    <Empty style={{ borderRadius: 8 }} description={false} />
                  </div>
                )}

                <div className='footer-wrapper'>
                  <CreateCarModal agencyData={agencyData} />
                </div>
              </div>
            </div>
          </div>
          <Tour
            open={openSelectedCar}
            onClose={() => {
              setOpenSelectedCar(false);
              setSelectedCars([]);
              setSelectedDriver(undefined);
            }}
            arrow={false}
            placement='left'
            steps={[
              {
                title: `Giao xe • ${selectedDriver?.name}`,
                description: (
                  <SelectedCarWrapper>
                    <Typography.Paragraph style={{ margin: 0 }}>
                      Hãy chọn{' '}
                      <Typography.Text strong underline>
                        1
                      </Typography.Text>{' '}
                      xe để giao
                    </Typography.Paragraph>
                    <div className='select-wrapper'>
                      <SelectedCarTag
                        {...(carList?.find((item) => item._id === selectedCars?.[0]) || {})}
                      />
                    </div>
                  </SelectedCarWrapper>
                ),
                target: () => carListRef.current!,
                placement: 'left',
                nextButtonProps: {
                  children: 'Xác nhận',
                  style: { height: 36, padding: '0 12px' },
                  onClick: () => {
                    handleDeliverDriver(selectedDriver?._id, selectedCars[0]);
                  },
                },
              },
            ]}
          />
        </>
      )}
    </PageWrapper>
  );
}

const SelectedCarWrapper = styled.div`
  position: relative;
  .select-wrapper {
    position: absolute;
    bottom: 0;
    left: 0;
    transform: translateY(44px);
  }
`;

const PreviewAvatar = styled.img`
  border-radius: 50%;
  height: 10em;
  width: 10em;
  display: block;
  margin: auto;
`;

const PageWrapper = styled(PageLayout)`
  padding: 0 24px;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  & > .page-header {
    padding: 0px 0 24px;
    .basic-wrapper {
      display: flex;
      align-items: center;
      margin-right: 24px;
      padding: 8px 0 0 0;
      .detail-wrapper {
        display: flex;
        flex-direction: column;
        margin: 0 0 0 12px;
        .name {
          margin: 0;
          font-size: 22px;
        }
        .phone {
          font-size: 12px;
          display: none;
        }
      }
    }
  }
  .page-header-info {
    display: flex;
    align-items: flex-start;
    flex-wrap: nowrap;
    padding-left: 4px;
    .page-title {
      margin-left: 18px;
    }
    .ant-badge:last-child {
      margin-left: auto;
    }
  }
  & > .profile-form {
    & > .profile-wrapper {
      display: flex;
      & > .left-wrapper {
        flex: 1 1 auto;
        min-width: 0px;
        display: flex;
        flex-direction: column;

        & > .actions-wrapper {
          display: flex;
          align-items: center;
          justify-content: flex-end;
          gap: 24px;
          & button[type='button'] {
            width: 160px;
          }
          & button[type='submit'] {
            width: 296px;
          }
        }
      }
      & > .right-wrapper {
        margin-left: 24px;
        flex: 0 0 300px;
        display: flex;
        flex-direction: column;
        gap: 24px;
        & > .bottom-wrapper {
          padding: 0 24px;
          display: flex;
          flex-direction: column;
          align-items: flex-start;
        }
      }
    }
    .point-container {
      position: relative;
      .point-actions {
        position: absolute;
        top: 0;
        right: 0;
      }
    }
    .ant-input-textarea-show-count::after {
      position: absolute;
      top: 0;
      right: 0;
      transform: translateY(calc(-100% - 8px));
    }
    .actions-container {
      .ant-form-item-control-input-content {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        gap: 24px;
      }
      & button[type='button'] {
        width: 160px;
      }
      & button[type='submit'] {
        width: 296px;
      }
    }
  }
  & > .driver-car-wrapper {
    padding: 12px 0;
    display: flex;
    z-index: 1;
    background-color: var(--bgColor);
    border-radius: 8px;
    height: calc(100vh - 64px - 24px);
    & > .left-wrapper {
      flex: 1 1 auto;
      min-width: 0px;
      border-right: 1px solid var(--colorBorder);
      display: flex;
      flex-direction: column;
      & > .header-wrapper {
        padding: 2px 12px 0 14px;
      }
      & > .container-header {
        display: flex;
        align-items: center;
        padding: 0 12px 6px;
      }
      & > .container {
        padding: 0 12px;
        .ant-checkbox-group {
          display: flex;
          flex-direction: column;
        }
      }
      & > .footer-wrapper {
        margin-top: 12px;
        padding-top: 12px;
        border-top: 1px solid var(--colorBorder);
        display: flex;
        justify-content: flex-end;
        align-items: center;
      }
    }
    & > .right-wrapper {
      padding: 0 12px;
      flex: 0 0 400px;

      & > .container {
        --bg-color: ${({ theme }) =>
          theme.mode === 'light' ? 'rgba(0,0,0,0.03)' : 'rgba(255,255,255,0.03)'};
        height: 100%;
        background-color: var(--bg-color);
        border-radius: 8px;
        padding: 0;
        display: flex;
        flex-direction: column;
        & > .header-wrapper {
          margin-bottom: 6px;
          padding: 12px 12px;
          border-bottom: 1px solid var(--colorBorder);
        }
        & > .container-header {
          display: flex;
          align-items: center;
          padding: 0 12px 6px;
        }
        & > .container {
          flex: 1 1 auto;
          min-height: 0px;
          .ant-checkbox-group {
            display: flex;
            flex-direction: column;
          }
        }
        & > .footer-wrapper {
          padding: 12px;
          display: flex;
          justify-content: flex-end;
          align-items: center;
        }
      }
    }
  }
  .container-empty {
    flex: 1 1 auto;
    min-height: 0px;
    .ant-empty {
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
    }
  }

  @media screen and (max-width: 1023.98px) {
    .ant-form-vertical {
      .col-right {
        flex: 0 0 auto !important;
      }
    }
  }
  @media screen and (max-width: 400.98px) {
    padding: 0 12px;
    .page-header-info {
      .page-title {
        font-size: 22px;
      }
    }
  }
`;

export default withAuth(AgencyDetailPage);
