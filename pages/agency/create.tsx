import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import {
  Alert,
  Card,
  Checkbox,
  Divider,
  Form,
  Input,
  InputNumber,
  Space,
  theme,
  Typography,
} from 'antd';
import type { DefaultOptionType } from 'antd/es/cascader';
import { useRouter } from 'next/router';
import { useId } from 'react';
import { BsDashLg, BsPlusLg, BsStar } from 'react-icons/bs';
import { IoMdCar } from 'react-icons/io';
import { MdEmojiTransportation } from 'react-icons/md';
import { useMediaQuery } from 'react-responsive';
import Avatar from 'src/components/avatar/Avatar';
import Breadcrumb from 'src/components/breadcrumb/Breadcrumb';
import Button from 'src/components/button/Button';
import InputPhoneWithCheck from 'src/components/field/InputPhoneWithCheck';
import MapPicker from 'src/components/field/MapPicker';
import { CoinAltIcon, CoinIcon } from 'src/components/icons';
import PageLayout from 'src/components/layout/PageLayout';
import SoftCreateCarModal from 'src/components/modal/SoftCreateCarModal';
import SoftCreateDriverModal from 'src/components/modal/SoftCreateDriverModal';
import Link from 'src/components/next/Link';
import { cssCheckboxTypeGroup, cssListItemCreate } from 'src/components/shared/ItemStyled';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useChangeLocale from 'src/hooks/useChangeLocale';
import withAuth from 'src/hooks/withAuth';
import { useCreateAgencyMutation } from 'src/redux/query/agency.query';
import { ErrorCode } from 'src/types/response.types';
import { removeAllSpecial, vietnameseSlug } from 'src/utils/utils';

import { mappedErrorToFormError } from 'src/utils/utils-error';

const filter = (inputValue: string, path: DefaultOptionType[]) =>
  path.some(
    (option) =>
      vietnameseSlug(option.label as string, ' ').indexOf(vietnameseSlug(inputValue, ' ')) > -1,
  );

interface ICar {
  id: string;
  name: string;
  plates: string;
  type: string;
  lat?: string;
  long?: string;
}

function CreateAgencyPage() {
  const uid = useId();
  const { push } = useRouter();
  const { i18n } = useChangeLocale();
  const { notification, message } = useApp();
  const [form] = Form.useForm();
  const {
    token: { colorTextSecondary, colorWarningActive },
  } = theme.useToken();
  const { generatedColors } = useTheme();
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const [container, setContainer] = useSafeState<HTMLDivElement | null>(null);

  const [selectedTab, setSelectedTab] = useSafeState('Profile');
  const isTransportationWatch = Form.useWatch('isTransportation', form);
  const isDriverWatch = Form.useWatch('isDriver', form);
  const carListWatch = Form.useWatch('carList', form);
  const driverListWatch = Form.useWatch('driverList', form);
  const { carIconById } = useCarType();
  const [createAgency, { isLoading }] = useCreateAgencyMutation();

  const handleCreateAgency = ({ geoLocation, ...formData }: any) => {
    const newFormData = {
      lat_address: geoLocation?.lat ? String(geoLocation.lat) : '',
      long_address: geoLocation?.lng ? String(geoLocation.lng) : '',
      ...formData,
    };
    console.log(newFormData);
    createAgency({ data: newFormData })
      .unwrap()
      .then((res) => {
        notification.success({
          message: res.message,
          description: (
            <Space wrap size={[4, 2]}>
              <Typography.Text type='secondary'>Go to</Typography.Text>
              <Link href={`/agency/${res.data.agency_id}?tab=Driver_Car`} underline>
                Created Agency
              </Link>
              <Typography.Text type='secondary'>or</Typography.Text>
              <Link href={'/agency'} underline>
                Back to Agency list
              </Link>
            </Space>
          ),
          placement: 'bottomRight',
        });
        form.resetFields();

        console.log('create res: ', res);
      })
      .catch((err) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        if (err.response_code === ErrorCode.RequestValidationError) {
          form.setFields(mappedErrorToFormError(err.error));
        }
      });
  };

  return (
    <PageWrapper
      breadcrumb={
        <Breadcrumb
          style={{ marginTop: 7 }}
          items={{
            '/agency': i18n['Danh sách Đối tác'],
            '/agency/create': i18n['Thêm đối tác'],
          }}
        />
      }
      ref={setContainer}
    >
      <div className='page-header' style={{ marginBottom: 16 }}>
        {!mediaAbove767 && (
          <div className='breadcrumb-wrapper'>
            <Breadcrumb
              items={{ '/agency': i18n['Danh sách Đối tác'] }}
              current={i18n['Thêm mới']}
            />
          </div>
        )}
        <Typography.Title level={2} style={{ marginBottom: 0 }}>
          {i18n['Thêm đối tác']}
        </Typography.Title>
      </div>
      <Form
        form={form}
        layout='vertical'
        size='large'
        onFinish={handleCreateAgency}
        disabled={isLoading}
        autoComplete='off'
        initialValues={{
          // hasCar: 0,
          coin: 0,
          point: 0,
          rank: 0,
          isDriver: false,
          isTransportation: false,
          address: '',
          updated_gps_time: 0,
        }}
      >
        <div className='profile-wrapper'>
          <div className='left-wrapper'>
            <Card style={{ marginBottom: 24 }}>
              <Form.Item
                name='name'
                label={i18n['Họ & tên']}
                rules={[{ required: true, message: '• Name is required' }]}
              >
                <Input type='text' placeholder={i18n['Nhập Họ và tên']} />
              </Form.Item>

              <InputPhoneWithCheck
                name='phone'
                label={i18n['Số điện thoại']}
                rules={[{ required: true, message: '• Phone is required' }]}
                inputProps={{ placeholder: i18n['Nhập Số điện thoại'] }}
              />
            </Card>

            <Card style={{ marginBottom: 24 }}>
              <Form.Item name='address' label={i18n['Địa chỉ chi tiết']}>
                <Input.TextArea
                  placeholder={i18n['Lấy gợi ý từ bản đồ hoặc Nhập địa chỉ chi tiết']}
                  autoSize={{ minRows: 2 }}
                  showCount
                />
              </Form.Item>
              <Form.Item label={i18n['Tọa độ địa chỉ']} name={'geoLocation'}>
                <MapPicker
                  style={{ height: 180 }}
                  draggable
                  onChangeAddressName={(v) => form.setFieldValue('address', v)}
                />
              </Form.Item>
            </Card>

            <Card>
              <Form.Item name='coin' label={'Coin'}>
                <InputNumber
                  step={1}
                  style={{ width: '100%' }}
                  formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                  prefix={<CoinIcon width={20} height={20} style={{ marginLeft: -3 }} />}
                />
              </Form.Item>
              <Form.Item name='point' label={i18n['Điểm']}>
                <InputNumber
                  step={1}
                  style={{ width: '100%' }}
                  formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                  prefix={
                    <CoinAltIcon
                      width={20}
                      height={20}
                      color={generatedColors[6]}
                      style={{ marginLeft: -3 }}
                    />
                  }
                />
              </Form.Item>
              <Form.Item
                name='rank'
                label={i18n['Hạng']}
                tooltip={{
                  title: i18n['Xếp hạng đối tác trong danh sách tìm kiếm'],
                  arrow: false,
                  placement: 'rightBottom',
                }}
              >
                <InputNumber
                  step={1}
                  style={{ width: '100%' }}
                  formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                  prefix={<BsStar />}
                />
              </Form.Item>
            </Card>
            <Divider />
            <div className='actions-wrapper' style={{ marginBottom: 24 }}>
              <Button
                htmlType='reset'
                block
                disabled={isLoading}
                onClick={(e) => {
                  e.stopPropagation();
                }}
              >
                {i18n['Hủy']}
              </Button>
              <Button
                htmlType='submit'
                block
                loading={isLoading}
                type='primary'
                icon={<BsPlusLg />}
              >
                {i18n['Thêm mới']}
              </Button>
            </div>
          </div>
          <div className='right-wrapper'>
            <Card>
              <Form.Item
                label={i18n['Chức năng, vai trò']}
                required
                help=''
                style={{ marginBottom: 12 }}
              >
                <div css={cssCheckboxTypeGroup}>
                  <Form.Item
                    name='isDriver'
                    valuePropName='checked'
                    noStyle
                    rules={[{ type: 'boolean', message: '• isDriver is invalid' }]}
                  >
                    <Checkbox className='checkbox-item'>
                      <IoMdCar size={20} />
                      <span>{i18n['Lái xe']}</span>
                    </Checkbox>
                  </Form.Item>
                  <Form.Item
                    name='isTransportation'
                    valuePropName='checked'
                    noStyle
                    rules={[{ type: 'boolean', message: '• isTransportation is invalid' }]}
                  >
                    <Checkbox className='checkbox-item'>
                      <MdEmojiTransportation size={24} />
                      <span>{i18n['Chức năng vận tải']}</span>
                    </Checkbox>
                  </Form.Item>
                </div>
                {!!isTransportationWatch && (
                  <Alert
                    style={{ marginTop: 12 }}
                    banner
                    type='info'
                    message='Có thể quản lý Xe và Lái xe'
                  />
                )}
              </Form.Item>
              {!!isTransportationWatch && (
                <Form.List name='driverList'>
                  {(fields, { add, remove }) => (
                    <>
                      <Divider
                        orientation='left'
                        orientationMargin={0}
                        style={{ fontSize: 15, color: colorTextSecondary }}
                      >
                        Danh sách Lái xe
                      </Divider>
                      <SoftCreateDriverModal
                        onOk={(formData) => {
                          
                          const foundDriver = (driverListWatch || []).find(
                            (d: any) =>
                              removeAllSpecial(d.phone).toLowerCase() ===
                              removeAllSpecial(formData.phone).toLowerCase(),
                          );
                          if (!!foundDriver) {
                            message.error(`Đã có lái xe với ${foundDriver.phone}`);
                          } else {
                            add(formData);
                            message.success(`Thêm lái xe thành công`);
                          }
                        }}
                      />
                      <div className='list'>
                        {(driverListWatch as any[])?.map((item, index) => (
                          <div key={uid + 'driverList' + index} css={cssListItemCreate}>
                            <Avatar
                              bg={generatedColors[1]}
                              size={60}
                              shape='square'
                              style={{ fontSize: 22 }}
                            >
                              {index + 1}
                            </Avatar>
                            <div className='right-wrapper'>
                              <Typography.Text strong>{item.name}</Typography.Text>
                              <Typography.Text type='secondary'>{item.phone}</Typography.Text>
                              <Typography.Text type='secondary'>{item.license_id}</Typography.Text>
                            </div>
                            <div className='actions-top'>
                              <Button
                                size='small'
                                type='dashed'
                                icon={<BsDashLg />}
                                onClick={() => remove(index)}
                              ></Button>
                            </div>
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                </Form.List>
              )}
              {!!isTransportationWatch && (
                <Form.List name='carList'>
                  {(fields, { add, remove }) => (
                    <>
                      <Divider
                        orientation='left'
                        orientationMargin={0}
                        style={{ fontSize: 15, color: colorTextSecondary }}
                      >
                        Danh sách Xe 
                      </Divider>
                      <SoftCreateCarModal
                        onOk={(formData) => {
                          const foundCar = (carListWatch || []).find(
                            (d: any) =>
                              removeAllSpecial(d.plates).toLowerCase() ===
                              removeAllSpecial(formData.plates).toLowerCase(),
                          );
                          if (!!foundCar) {
                            message.error(`Đã có xe với ${foundCar.plates}`);
                          } else {
                            add(formData);
                            message.success(`Thêm xe thành công`);
                          }
                        }}
                      />
                      <div className='list'>
                        {(carListWatch as any[])?.map((item, index) => (
                          <div key={uid + 'carList' + index} css={cssListItemCreate}>
                            <Avatar
                              bg={generatedColors[1]}
                              size={60}
                              shape='square'
                              style={{ fontSize: 22 }}
                            >
                              {index + 1}
                            </Avatar>
                            <div className='right-wrapper'>
                              <Typography.Text strong>{item.name}</Typography.Text>
                              <Typography.Text type='secondary'>{item.plates}</Typography.Text>
                              {carIconById(item?.car_type_id)}
                            </div>
                            <div className='actions-top'>
                              <Button
                                size='small'
                                type='dashed'
                                icon={<BsDashLg />}
                                onClick={() => remove(index)}
                              ></Button>
                            </div>
                          </div>
                        ))}
                      </div>
                    </>
                  )}
                </Form.List>
              )}
            </Card>
          </div>
        </div>
      </Form>
    </PageWrapper>
  );
}

const PageWrapper = styled(PageLayout)`
  padding: 0 24px;
  .ant-card-body {
    position: relative;
  }
  .ant-form-vertical {
    & > .profile-wrapper {
      display: flex;
      & > .left-wrapper {
        flex: 1 1 auto;
        min-width: 0px;
        display: flex;
        flex-direction: column;
        margin-right: 24px;
        & > .actions-wrapper {
          display: flex;
          align-items: center;
          justify-content: flex-end;
          gap: 24px;
          & button[type='reset'] {
            width: 160px;
          }
          & button[type='submit'] {
            width: 296px;
          }
        }
      }
      & > .right-wrapper {
        flex: 0 0 300px;
        display: flex;
        flex-direction: column;
        gap: 24px;
      }
    }
    & > .driver-car-wrapper {
      background-color: var(--bgColor);
      min-height: 600px;
      border-radius: 8px;
      overflow: hidden;
      display: flex;
      & > .left-wrapper {
        padding: 0px;
        flex: 0 0 360px;
        display: flex;
        flex-direction: column;
        & > .title-wrapper {
          padding: 12px 12px;
        }
        & > .list-wrapper {
          flex: 1 1 auto;
          min-height: 0px;
          & > .list {
            padding: 0 12px;
          }
        }
        & > .footer-wrapper {
          margin-top: auto;
          padding: 12px;
        }
      }
      & > .right-wrapper {
        flex: 1 1 auto;
        min-width: 0px;
      }
    }
    .point-container {
      position: relative;
      .point-actions {
        position: absolute;
        top: 0;
        right: 0;
      }
    }
    .ant-input-textarea-show-count::after {
      position: absolute;
      top: 0;
      right: 0;
      transform: translateY(calc(-100% - 8px));
    }
    .car-number-wrapper {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
    }
  }
  .horizontal-form-item {
    .ant-form-item-row {
      flex-direction: row;
    }
  }

  .list-container {
    margin-bottom: 10px;
    .item-container {
      padding: 8px;
      display: flex;
      align-items: flex-start;
      gap: 8px;
      border: 1px solid ${({ theme }) => (theme.mode === 'dark' ? '#424242' : '#d9d9d9')};
      border-radius: 8px;
      .ant-avatar-square {
        border-radius: 4px;
        flex-shrink: 0;
      }
      .info-container {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        .ant-typography-ellipsis {
          max-width: 124px;
        }
        .ant-tag {
          margin-top: 2px;
          padding-left: 6px;
          padding-right: 6px;
          border-radius: 4px;
        }
      }
      .action-container {
        margin-left: auto;
        display: grid;
        grid-template-columns: auto auto;
        align-items: center;
        gap: 8px;
        .location {
          margin-left: 16px;
          color: #1677ff;
          cursor: pointer;
          :hover {
            color: #8fbeff;
          }
        }
      }

      &:not(:last-child) {
        margin-bottom: 8px;
      }
    }
  }

  @media screen and (max-width: 767.98px) {
    .ant-form-vertical {
      .col-right {
        flex: 0 0 auto !important;
      }
    }
  }
  @media screen and (max-width: 400.98px) {
    padding: 0 12px;
    .page-header {
      .page-title {
        font-size: 22px;
      }
    }
  }
`;

export default withAuth(CreateAgencyPage);
