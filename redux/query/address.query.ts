import { createApi } from '@reduxjs/toolkit/query/react';
import { TResponse, TResponseWithMeta } from 'src/types/response.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TAddressFilter = {
  keyword?: string;
  p?: string;
};

export type TCreateAddressDAta = any;
export type TUpdateCarDetailData = {};

export type TCarType = {
  name: string;
  name_id: number;
  id: string;
};

export type TUpdateAddressData = {
  id: string;
  name: string;
  province_id?: string;
  center?: number[];
  status?: number;
  priority?: number;
};

export type TAddress = {
  id: string;
  name: string;
  province_id?: string;
  center_lat?: number;
  center_long?: number;
  status?: number;
  priority?: number;
};

export const addressApi = createApi({
  reducerPath: 'addressApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Address'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // []
    getProvinceDistrictList: builder.query<
      TResponseWithMeta<{ address_list: TAddress[] }>,
      { keyword?: string }
    >({
      query: (filter) => ({ url: '/address', method: 'get', params: { ...filter, level: '2' } }),
      providesTags: (result) =>
        !!result?.data?.address_list.length
          ? [
              ...result.data.address_list.map(({ id }) => ({
                type: 'Address' as const,
                id,
              })),
              { type: 'Address', id: 'LIST' },
            ]
          : [{ type: 'Address', id: 'LIST' }],
    }),
    // []
    getProvinceList: builder.query<
      TResponseWithMeta<{ address_list: TAddress[] }>,
      Pick<TAddressFilter, 'keyword'>
    >({
      query: (filter) => ({ url: '/address', method: 'get', params: filter }),
      providesTags: (result) =>
        !!result?.data?.address_list.length
          ? [
              ...result.data.address_list.map(({ id }) => ({
                type: 'Address' as const,
                id,
              })),
              { type: 'Address', id: 'LIST' },
            ]
          : [{ type: 'Address', id: 'LIST' }],
    }),
    // []
    getDistrictList: builder.query<TResponseWithMeta<{ address_list: TAddress[] }>, TAddressFilter>(
      {
        query: (filter) => ({ url: '/address', method: 'get', params: filter }),
        providesTags: (result) =>
          !!result?.data?.address_list.length
            ? [
                ...result.data.address_list.map(({ id }) => ({
                  type: 'Address' as const,
                  id,
                })),
                { type: 'Address', id: 'LIST' },
              ]
            : [{ type: 'Address', id: 'LIST' }],
      },
    ),
    // []
    getAddressDetail: builder.query<
      TResponse<{
        address: TAddress;
        geometry: { type: string; coordinates: any };
        bbox: number[];
      }>,
      string
    >({
      query: (id) => ({ url: `/address/${id}`, method: 'get' }),
      providesTags: (result, error, id) => [{ type: 'Address', id }],
    }),
    // []
    addressDetail: builder.mutation<
      TResponse<{
        address: TAddress;
        geometry: { type: string; coordinates: any };
        bbox: number[];
      }>,
      string
    >({
      query: (id) => ({ url: `/address/${id}`, method: 'get' }),
      invalidatesTags: (result, error, id) => [{ type: 'Address', id }],
    }),
    // [ADMIN,PM]
    createAddress: builder.mutation<TResponse, TCreateAddressDAta>({
      query: (data) => ({
        url: `/address/create`,
        method: 'post',
        data: data,
      }),
      invalidatesTags: [{ type: 'Address', id: 'LIST' }],
    }),
    updateAddress: builder.mutation<TResponse, TUpdateAddressData>({
      query: ({ id, ...data }) => ({
        url: `/address/update/${id}`,
        method: 'put',
        data,
      }),
      invalidatesTags: (result, error, { id }) => [{ type: 'Address', id }],
    }),
    // [ADMIN,PM]
    deleteAddress: builder.mutation<TResponse, string>({
      query: (carId) => ({ url: `/address/${carId}`, method: 'delete' }),
      invalidatesTags: [{ type: 'Address', id: 'LIST' }],
    }),
  }),
});
export const {
  useGetDistrictListQuery,
  useGetProvinceListQuery,
  useCreateAddressMutation,
  useDeleteAddressMutation,
  useGetAddressDetailQuery,
  useUpdateAddressMutation,
  useAddressDetailMutation,
  useGetProvinceDistrictListQuery,
} = addressApi;
