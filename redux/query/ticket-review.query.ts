import { createApi } from '@reduxjs/toolkit/query/react';
import { TBaseFilter } from 'src/types/global.types';
import { TResponse, TResponseWithMeta } from 'src/types/response.types';
import { TAllTicketReviewItem, TRankTicketReviewAgencyItem, TTicket } from 'src/types/ticket.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TListFilter = {
  status?: number;
  car_type_id?: number;
  rank?: number;
  is_ranked?: number;
  ticket_status?: number;
} & TBaseFilter;

export const ticketReviewApi = createApi({
  reducerPath: 'ticketReviewApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['TicketReviews'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // [ADMIN,PM]
    getAllTicketReviews: builder.query<
      TResponseWithMeta<{ ticket_review_list: TAllTicketReviewItem[] }>,
      any
    >({
      query: (filter) => ({ url: '/adm/ticket_review', method: 'get', params: filter }),
      providesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    getTicketReviewDetail: builder.query<
      TResponse<{ ticket_info: TTicket; ticket_review_info: any; ticket_rank_info: any }>,
      { ticket_review_id: string }
    >({
      query: ({ ticket_review_id }) => ({
        url: `/adm/ticket_review/${ticket_review_id}`,
        method: 'get',
      }),
      providesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    getTicketReviewCount: builder.query<TResponse<{ count_list: number[] }>, any>({
      query: () => ({
        url: `/adm/ticket_review/count`,
        method: 'get',
      }),
      providesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    getRankTicketReviewAgency: builder.query<
      TResponse<{ agency_review_list: TRankTicketReviewAgencyItem[] }>,
      any
    >({
      query: (filter) => ({
        url: `/adm/ticket_review/rank/agency`,
        method: 'get',
        params: filter,
      }),
      providesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    updateRankTicketReview: builder.mutation<TResponse<{ count: number }>, any>({
      query: () => ({
        url: `/adm/ticket_review/rank`,
        method: 'get',
      }),
      invalidatesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    actionsRankedTicketReview: builder.mutation<TResponse<{ count: number }>, any>({
      query: () => ({
        url: `/adm/ticket_review/rank/actions`,
        method: 'get',
      }),
      invalidatesTags: [{ type: 'TicketReviews', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    updateTicketReview: builder.mutation<
      TResponse,
      { ticket_review_id: string } & Record<string, any>
    >({
      query: ({ ticket_review_id, ...data }) => ({
        url: `/adm/ticket_review/${ticket_review_id}`,
        method: 'put',
        data: data,
      }),
      invalidatesTags: (result, error, { ticket_review_id }) => [
        { type: 'TicketReviews', id: 'LIST' },
      ],
    }),
  }),
});
export const {
  useGetAllTicketReviewsQuery,
  useGetTicketReviewDetailQuery,
  useUpdateTicketReviewMutation,
  useGetTicketReviewCountQuery,
  useUpdateRankTicketReviewMutation,
  useActionsRankedTicketReviewMutation,
  useGetRankTicketReviewAgencyQuery,
} = ticketReviewApi;
