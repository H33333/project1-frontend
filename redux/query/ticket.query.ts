import { createApi } from '@reduxjs/toolkit/query/react';
import { TBaseFilter } from 'src/types/global.types';
import { TResponse, TResponseWithMeta } from 'src/types/response.types';
import { TAllTicketItem } from 'src/types/ticket.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TListFilter = {
  status?: number;
  car_type_id?: number;
} & TBaseFilter;

interface TicketStatisticData {
  canceled: number;
  completed: number;
  delevered: number;
  new: number;
  running: number;
  total: number;
}

interface TicketStatisticMonthData {
  [key: string]: {
    new: number;
    delevered: number;
    running: number;
    completed: number;
    canceled: number;
  };
}


export const ticketApi = createApi({
  reducerPath: 'ticketApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Tickets'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // [ADMIN,PM]
    getAllTicketList: builder.query<TResponseWithMeta<{ ticket_list: TAllTicketItem[] }>, any>({
      query: (filter) => ({ url: '/adm/ticket/all', method: 'get', params: filter }),
      providesTags: (result) =>
        !!result?.data?.ticket_list.length
          ? [
              ...result.data.ticket_list.map(({ _id }) => ({
                type: 'Tickets' as const,
                id: _id,
              })),
              { type: 'Tickets', id: 'LIST' },
            ]
          : [{ type: 'Tickets', id: 'LIST' }],
    }),
    //[ADMIN, PM]
    getTicketStatistic: builder.query<TResponse<TicketStatisticData>, any>({
      query: ({ year }) => ({ url: `/adm/ticket/statistic${year ? `?year=${year}` : ''}`, method: 'get' }),
      providesTags: (result) => [{ type: 'Tickets', id: 'STATISTIC' }],
    }),
    //[ADMIN, PM]
    getTicketStatisticMonth: builder.query<TResponse<TicketStatisticMonthData>, any>({
      query: ({ year }) => ({ url: `/adm/ticket/statistic/month${year ? `?year=${year}` : ''}`, method: 'get' }),
      providesTags: (result) => [{ type: 'Tickets', id: 'STATISTIC' }],
    }),
    // [ADMIN,PM]
    getTicketDetail: builder.query<
      TResponse<{ ticket_info: any; list_driver: any[]; customer_info: any }>,
      { ticket_id: string }
    >({
      query: ({ ticket_id }) => ({
        url: `/adm/ticket/detail/${ticket_id}`,
        method: 'get',
      }),
      providesTags: (result, error, { ticket_id }) => [{ type: 'Tickets', id: ticket_id }],
    }),
    // [ADMIN,PM]
    cancelTicket: builder.mutation<TResponse, { ticket_id: string }>({
      query: ({ ticket_id }) => ({
        url: `/adm/ticket/cancel/${ticket_id}`,
        method: 'post',
      }),
      invalidatesTags: (result, error, { ticket_id }) => [{ type: 'Tickets', id: ticket_id }],
    }),
  }),
});
export const { useCancelTicketMutation, useGetAllTicketListQuery, useGetTicketDetailQuery, useGetTicketStatisticQuery, useGetTicketStatisticMonthQuery } =
  ticketApi;
