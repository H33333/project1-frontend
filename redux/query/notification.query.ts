import { createApi } from '@reduxjs/toolkit/query/react';
import { TBaseFilter } from 'src/types/global.types';
import { TResponseWithMeta } from 'src/types/response.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TListFilter = TBaseFilter & {
  status?: number;
};

export const notificationApi = createApi({
  reducerPath: 'notificationApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Notification'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // [ADMIN, PM]
    getListNotification: builder.query<TResponseWithMeta<{ notification_list: any[] }>, any>({
      query: (filter) => ({ url: '/notification', method: 'get', params: filter }),
      providesTags: (result) =>
        !!result?.data?.notification_list?.length
          ? [
              ...result?.data?.notification_list?.map(({ _id }) => ({
                type: 'Notification' as const,
                id: _id,
              })),
              { type: 'Notification', id: 'LIST' },
            ]
          : [{ type: 'Notification', id: 'LIST' }],
    }),
    getListNotificationReport: builder.query<TResponseWithMeta<{ notification_list: any[] }>, any>({
      query: (filter) => ({ url: '/notification/report', method: 'get', params: filter }),
    }),
    getNotificationResend: builder.query<TResponseWithMeta<{ notification_list: any[] }>, any>({
      query: (filter) => ({
        url: `/notification/resend/${filter.notification_id as string}`,
        method: 'get',
      }),
    }),
    getNotificationList: builder.query<TResponseWithMeta<{ notification_list_error: any[] }>, any>({
      query: (filter) => ({
        url: `/notification/list_agency/${filter.notification_id as string}`,
        method: 'get',
      }),
    }),
  }),
});
export const {
  useGetListNotificationQuery,
  useGetListNotificationReportQuery,
  useLazyGetNotificationResendQuery,
  useGetNotificationListQuery,
} = notificationApi;
