import { createApi } from '@reduxjs/toolkit/query/react';
import { TBaseFilter } from 'src/types/global.types';
import { TResponseWithMeta } from 'src/types/response.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TListFilter = TBaseFilter & {
  status?: number;
};

export const moniterApi = createApi({
  reducerPath: 'moniterApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Moniter'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // [ADMIN, PM]
    getFilteredMoniter: builder.query<TResponseWithMeta<{ monitor_list: any[] }>, any>({
      query: (filter) => ({ url: '/monitor', method: 'get', params: filter }),
      providesTags: (result) =>
        !!result?.data?.monitor_list?.length
          ? [
              ...result?.data?.monitor_list?.map(({ _id }) => ({
                type: 'Moniter' as const,
                id: _id,
              })),
              { type: 'Moniter', id: 'LIST' },
            ]
          : [{ type: 'Moniter', id: 'LIST' }],
    }),
    getOnlineAgency: builder.query<TResponseWithMeta<{ monitor_list: any[] }>, any>({
      query: (filter) => ({ url: '/monitor/online', method: 'get' }),
    }),
  }),
});
export const { useGetFilteredMoniterQuery, useGetOnlineAgencyQuery } = moniterApi;
