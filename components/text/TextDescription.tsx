import styled from '@emotion/styled';
import { Typography } from 'antd';

type TProps = {
  children?: React.ReactNode;
  icon?: React.ReactNode;
  split?: string;
  className?: string;
  textProps?: any;
  style?: React.CSSProperties;
};

function TextDescription({ children, icon, split, style, className, textProps }: TProps) {
  return (
    <TextDescriptionWrapper className={className} style={style}>
      {icon}
      {split && <span className='split'>{split}</span>}
      <Typography.Paragraph {...textProps}>{children}</Typography.Paragraph>
    </TextDescriptionWrapper>
  );
}
const TextDescriptionWrapper = styled.div`
  display: flex;
  flex-wrap: nowrap;
  & > svg {
    flex-shrink: 0;
    align-self: flex-start;
  }
  .split {
    flex-shrink: 0;
  }
  .ant-typography {
    word-break: break-word;
    line-height: 1.15;
    margin: 0;
    flex: 1 1 auto;
    min-width: 0px;
  }
`;

export default TextDescription;
