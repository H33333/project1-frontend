import styled from '@emotion/styled';
import { Popover, Typography } from 'antd';
import { FcGoogle } from 'react-icons/fc';
import { MdLocationPin } from 'react-icons/md';
import { FullscreenControl, Marker } from 'react-map-gl/maplibre';
import { DEFAULT_POSITION } from 'src/utils/constant';
import { dayjs, formatDate, formatFromNow } from 'src/utils/utils-date';
import MapGL from '../map/MapGL';
import MapPopupName from '../map/MapPopupName';

type TTimeTextProps = {
  updated_gps_time?: number;
  lat?: string;
  long?: string;
  onClick?: () => void;
  underline?: boolean;
};

const TimeText = ({ updated_gps_time, lat, long, onClick, underline }: TTimeTextProps) => {
  if (!updated_gps_time) return <></>;
  return (
    <TimeTextStyled
      ellipsis
      type='secondary'
      className='text'
      onClick={onClick}
      underline={underline}
    >
      <MdLocationPin size={12} style={{ margin: '0 -1px -1px 0' }} />
      {dayjs(updated_gps_time).isBefore(dayjs().startOf('day'))
        ? formatDate(updated_gps_time, 'HH:mm DD-MM-YYYY')
        : formatFromNow(updated_gps_time)}
    </TimeTextStyled>
  );
};

export const TimeTextPopover = ({ updated_gps_time, lat, long, underline }: TTimeTextProps) => {
  if (!updated_gps_time) return <></>;
  return (
    <Popover
      arrow={false}
      placement='bottomRight'
      trigger={['click']}
      content={
        <PopoverContentStyled>
          <Typography.Paragraph style={{ marginBottom: 10 }}>
            <span>Cập nhật vị trí lúc:</span>{' '}
            <u>{formatDate(updated_gps_time, 'HH:mm DD-MM-YYYY')}</u>
          </Typography.Paragraph>
          <div className='map-wrapper' key={JSON.stringify({ lat, long })}>
            <MapGL
              latitude={!!lat ? +lat : DEFAULT_POSITION.lat}
              longitude={!!long ? +long : DEFAULT_POSITION.lng}
              zoom={17}
              padding={{ top: 120, right: 240, bottom: 0, left: 0 }}
              interactive={false}
              style={{ height: 140 }}
            >
              <FullscreenControl position='bottom-right' />
              {!!lat && !!long ? (
                <>
                  <Marker color='red' scale={0.8} latitude={+lat} longitude={+long} />
                  <MapPopupName
                    latitude={+lat}
                    longitude={+long}
                    className='hide-arrow'
                    offset={[0, -28] as any}
                    nameProps={{ style: { right: 2, bottom: 4 } }}
                  />
                </>
              ) : (
                <MapPopupName
                  latitude={DEFAULT_POSITION.lat}
                  longitude={DEFAULT_POSITION.lng}
                  nameProps={{
                    text: 'Chưa có địa chỉ',
                    style: { right: 2, bottom: 8 },
                  }}
                  className='hide-arrow'
                />
              )}
            </MapGL>
            {!!lat && !!long && (
              <a
                className='google-map-link'
                target='_blank'
                href={`https://www.google.com/maps/search/?api=1&query=${
                  lat || DEFAULT_POSITION.lat
                },${long || DEFAULT_POSITION.lng}`}
                rel='noreferrer'
              >
                Xem trên bản đồ GoogleMap <FcGoogle size={14} style={{ margin: '0 0 0 4px' }} />
              </a>
            )}
          </div>
        </PopoverContentStyled>
      }
    >
      <TimeTextStyled
        ellipsis
        type='secondary'
        className='text'
        underline={underline}
        style={{ margin: '0 -1px -1px 0' }}
      >
        <MdLocationPin size={12} />
        {dayjs(updated_gps_time).isBefore(dayjs().startOf('day'))
          ? formatDate(updated_gps_time, 'HH:mm DD-MM-YYYY')
          : formatFromNow(updated_gps_time)}
      </TimeTextStyled>
    </Popover>
  );
};

const PopoverContentStyled = styled.div`
  width: 330px;
  .maplibregl-ctrl-attrib {
    display: none;
  }
  .map-wrapper {
    position: relative;
  }
  .google-map-link {
    display: flex;
    align-items: center;
    text-decoration: underline;
  }
`;
const TimeTextStyled = styled(Typography.Text)`
  cursor: pointer;
  user-select: none;
  display: inline-flex;
  align-items: flex-end;
  flex-shrink: 0;
  &.ant-typography-ellipsis {
    max-width: calc(100% + 12px);
  }
  &:hover {
    text-decoration: underline;
  }
`;

export default TimeText;
