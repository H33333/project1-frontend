import styled from '@emotion/styled';
import { Table } from 'antd';

const StyledTable = styled(Table<any>)`
  .ant-table-tbody > tr.ant-table-row-selected > td {
    background: rgba(0, 0, 0, 0.05);
    backdrop-filter: blur(10px);
  }
  .ant-table-tbody > tr.ant-table-row-selected:hover > td {
    background: rgba(0, 0, 0, 0.05) !important;
    backdrop-filter: blur(10px);
  }
  &.hide-title-border {
    .ant-table-thead > tr > td,
    .ant-table-thead > tr > th {
      &::before {
        display: none;
      }
    }
  }
  &.has-title-actions {
    .ant-table-thead > tr > th.ant-table-cell-ellipsis {
      overflow-x: unset;
    }
  }
`;
export default StyledTable;

export const TitleActionsWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  & > .actions-wrapper {
    position: relative;
    margin-left: 12px;
    .actions {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      display: flex;
      align-items: center;
      gap: 12px;
      z-index: 1;
      & > .ant-tag {
        cursor: pointer;
      }
    }
  }
`;

export const NameCombined1Wrapper = styled.div`
  display: inline-flex;
  max-width: 240px;
  & > .image-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .detail-wrapper {
    flex: 1 1 auto;
    min-width: 0px;
    display: flex;
    flex-direction: column;
    margin-left: 12px;
    & > .name {
      overflow: hidden;
      text-overflow: ellipsis;
      font-weight: 500;
      font-size: 16px;
      white-space: nowrap;
    }
    & > .phone {
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
  }
`;

export const RegisterTypeWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  gap: 12px;
  flex-direction: column;
  
`;

export const CodeWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  .ant-tag {
    flex-shrink: 0;
  }
  .ant-typography-copy {
    color: inherit;
  }
`;

export const CountsWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  flex-wrap: wrap;
`;

export const RegisterActionsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ActionsWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;
