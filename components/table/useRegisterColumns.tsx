import { Typography } from 'antd';
import { ColumnGroupType, ColumnType } from 'antd/es/table';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { TRegister } from 'src/types/register.types';
import { dayjs } from 'src/utils/utils-date';
import TypeAgencyAvatar from '../avatar/TypeAgencyAvatar';
import ConfirmRegisterModal2 from '../form/ConfirmRegisterModal2';
import Link from '../next/Link';
import Tag from '../tag/Tag';
import {
  CodeWrapper,
  NameCombined1Wrapper,
  RegisterActionsWrapper,
  RegisterTypeWrapper,
} from './StyledTable';

export type TRegisterColumn = ColumnGroupType<TRegister> | ColumnType<TRegister>;

type TRes = Record<'nameCombined1' | 'type' | 'code' | 'actions' | 'createdAt', TRegisterColumn>;

function useRegisterColumns(): TRes {
  const { i18n } = useChangeLocale();

  return {
    nameCombined1: {
      dataIndex: 'name',
      key: 'name',
      render: (text, item) => {
        return (
          <NameCombined1Wrapper>
            <Link href={`/register/${item._id}`} className='image-link'>
              <TypeAgencyAvatar
                size={48}
                offset={[0, 34]}
                isActive={item.status === 1}
                isDriver={item.isDriver}
                isTransportation={item.isTransportation}
              ></TypeAgencyAvatar>
            </Link>
            <div className='detail-wrapper'>
              <Link href={`/register/${item._id}`} className='name'>
                {item.name}
              </Link>
              <Typography.Text type='secondary' className='phone' style={{ lineHeight: 1 }}>
                {item.phone}
              </Typography.Text>
              <Typography.Paragraph
                type='secondary'
                style={{ margin: '0px 0 0 0', fontSize: 11, lineHeight: 1 }}
              >
                Ngày tạo: {dayjs(item.createdAt).format('DD/MM/YYYY HH:mm')}
              </Typography.Paragraph>
            </div>
          </NameCombined1Wrapper>
        );
      },
    },
    type: {
      dataIndex: 'name',
      key: 'isTransportation-isDriver',
      render: (text, item) => {
        return (
          <RegisterTypeWrapper>
            {item.isTransportation && <Tag color='geekblue'>{i18n['Chức năng vận tải']}</Tag>}
            {item.isDriver && <Tag color='blue'>{i18n['Lái xe']}</Tag>}
          </RegisterTypeWrapper>
        );
      },
    },
    code: {
      dataIndex: 'code',
      key: 'code',
      render: (text, item) => {
        return (
          <CodeWrapper>
            <Tag color='success'>
              <Typography.Text style={{ color: 'inherit' }} copyable={{ tooltips: false }}>
                {item.code}
              </Typography.Text>
            </Tag>
          </CodeWrapper>
        );
      },
    },
    createdAt: {
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (text, item) => {
        return (
          <>
            <Typography.Paragraph type='secondary' style={{ margin: 0 }}>
              {dayjs(item.createdAt).format('DD/MM/YYYY')}
            </Typography.Paragraph>
            <Typography.Paragraph type='secondary' style={{ margin: 0 }}>
              {dayjs(item.createdAt).format('HH:mm')}
            </Typography.Paragraph>
          </>
        );
      },
    },
    actions: {
      dataIndex: '_id',
      key: 'actions',
      render: (text, item) => {
        return (
          <RegisterActionsWrapper>
            <ConfirmRegisterModal2
              {...item}
              buttonProps={{
                block: true,
                style: { justifyContent: 'flex-start', padding: '0 8px', gap: 2 },
              }}
            />
          </RegisterActionsWrapper>
        );
      },
    },
  };
}

export default useRegisterColumns;
