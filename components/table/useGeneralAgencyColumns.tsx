import { Typography } from 'antd';
import { ColumnGroupType, ColumnType } from 'antd/es/table';
import { BsPersonFill } from 'react-icons/bs';
import { IoMdCar } from 'react-icons/io';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useChangeStatusAgency from 'src/hooks/useChangeStatusAgency';
import { TAgencyItem } from 'src/redux/query/agency.query';
import TypeAgencyAvatar from '../avatar/TypeAgencyAvatar';
import SegmentedStatus from '../field/SegmentedStatus';
import Link from '../next/Link';
import Tag from '../tag/Tag';
import {
  CodeWrapper,
  CountsWrapper,
  NameCombined1Wrapper,
  RegisterTypeWrapper,
} from './StyledTable';

export type TAgencyColumn = ColumnGroupType<TAgencyItem> | ColumnType<TAgencyItem>;

type TRes = Record<'nameCombined1' | 'type' | 'countInfo' | 'code' | 'actions', TAgencyColumn>;

function useGeneralAgencyColumns(): TRes {
  const { i18n } = useChangeLocale();
  const { handleChangeStatus, blockLoading, unBlockLoading } = useChangeStatusAgency();

  return {
    nameCombined1: {
      dataIndex: 'name',
      key: 'name',
      render: (text, item) => {
        return (
          <NameCombined1Wrapper>
            <Link href={`/agency/${item._id}`} className='image-link'>
              <TypeAgencyAvatar
                size={48}
                offset={[0, 34]}
                isActive={item.status === 1}
                isDriver={item.isDriver}
                isTransportation={item.isTransportation}
              ></TypeAgencyAvatar>
            </Link>
            <div className='detail-wrapper'>
              <Link href={`/agency/${item._id}`} className='name'>
                {item.name}
              </Link>
              <Typography.Text type='secondary' className='phone'>
                {item.phone}
              </Typography.Text>
              
            </div>
          </NameCombined1Wrapper>
        );
      },
    },
    countInfo: {
      dataIndex: '_id',
      key: 'countInfo',
      render: (text, item) => {
        return (
          <CountsWrapper>
            {/* <Tag
              color='blue'
              icon={<IoMdCar size={15} />}
              className='tag-car'
              style={{ margin: '0 6px 0 0' }}
            >
              {item?.car_list?.length || '0'}
            </Tag>
            <Tag color='geekblue' icon={<BsPersonFill size={14} />} className='tag-driver'>
              {item?.driver_list?.length || '0'}
            </Tag> */}
            <Tag color='geekblue' icon={<BsPersonFill size={14} />} className='tag-type'>
              {item?.agency_type || 'None'}
            </Tag>
          </CountsWrapper>
        );
      },
    },
    type: {
      dataIndex: 'name',
      key: 'isTransportation-isDriver',
      render: (text, item) => {
        return (
          <RegisterTypeWrapper>
            {item.updated_login_time_string && (
              <Tag color='geekblue' >Login: {item.updated_login_time_string}</Tag>
            )}
            {item.updated_gps_time_string && (
              <Tag color='pink'>GPS: {item.updated_gps_time_string}</Tag>
            )}
            { item.remain_login_time_string && (
              <Tag color='orange'>Expired: {item.remain_login_time_string}</Tag>
            )}
            {item.rank != null && (
              <Tag color='blue' > Level:{item.rank}</Tag>
            )}
          </RegisterTypeWrapper>
        );
      },
    },
    code: {
      dataIndex: 'code',
      key: 'code',
      render: (text, item) => {
        return (
          <CodeWrapper>
            {/* <Tag color='success'>
              <Typography.Text style={{ color: 'inherit' }} copyable={{ tooltips: false }}>
                {item.code}
              </Typography.Text>
            </Tag> */}
            {item.device_id == null && (
                <Tag color='success'>
                <Typography.Text style={{ color: 'inherit' }} >
                  DeviceID: null
                </Typography.Text>
              </Tag>
            )}
            {item.device_id != null && (
                <Tag color='success'>
                <Typography.Text style={{ color: 'inherit' }} >
                  DeviceID: {item.device_id}
                </Typography.Text>
              </Tag>
            )}
          </CodeWrapper>
        );
      },
    },
    actions: {
      dataIndex: '_id',
      key: 'actions',
      render: (text, item) => {
        return (
          <SegmentedStatus
            status={item.status}
            disabled={blockLoading || unBlockLoading}
            onChange={(v) => handleChangeStatus(item._id, v as string)}
          />
        );
      },
    },
  };
}

export default useGeneralAgencyColumns;
