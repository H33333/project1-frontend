import styled from '@emotion/styled';
import { Menu } from 'antd';
import { useRouter } from 'next/router';
import { BsGear, BsHouseDoor, BsPeople, BsPersonExclamation, BsPersonPlus } from 'react-icons/bs';
import { FaRegListAlt } from 'react-icons/fa';
import { LiaMapMarkedAltSolid } from 'react-icons/lia';
import { LuCalendarClock, LuCalendarDays } from 'react-icons/lu';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { useAppSelector } from 'src/redux/store';
import Link from '../next/Link';

function PMMenuNav({ collapsed = false }: { collapsed?: boolean }) {
  const { asPath, locale, push } = useRouter();
  const { i18n } = useChangeLocale();
  const userData = useAppSelector((s) => s.user.data);
  return !!userData ? (
    <MenuWrapper className={`${collapsed ? 'collapsed ' : ''}${locale || ''}`}>
      <Menu
        selectable={false}
        theme='light'
        defaultSelectedKeys={[asPath.split('?')[0]]}
        style={{ backgroundColor: 'transparent' }}
        mode='inline'
        className='side-menu'
        inlineIndent={23}
        items={
          [
            {
              label: <Link href='/'>{i18n['Tổng quan']}</Link>,
              icon: <BsHouseDoor size={22} />,
              key: '/',
              className: 'side-menu-item',
            },
            userData.role === 'PM'
              ? {
                label: i18n['Đối tác'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/agency'>{i18n['Danh sách']}</Link>,
                    icon: <BsPeople size={22} />,
                    key: '/agency',
                    className: 'side-menu-item',
                  },
                  {
                    label: <Link href='/agency/create'>{i18n['Thêm mới']}</Link>,
                    icon: <BsPersonPlus size={22} />,
                    key: '/agency/create',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'PM'
              ? {
                label: i18n['Đối tác đăng ký'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/register'>{i18n['Danh sách']}</Link>,
                    icon: <BsPeople size={22} />,
                    key: '/register',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'PM'
              ? {
                label: i18n['Khu vực'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/address'>{i18n['Danh sách']}</Link>,
                    icon: <LiaMapMarkedAltSolid size={22} />,
                    key: '/address',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'PM'
              ? {
                label: i18n['Lịch'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/ticket'>{i18n['Danh sách']}</Link>,
                    icon: <LuCalendarDays size={21} />,
                    key: '/ticket',
                    className: 'side-menu-item',
                  },
                  {
                    label: <Link href='/ticket/agency'>{'Đối tác vi phạm'}</Link>,
                    icon: <BsPersonExclamation size={22} />,
                    key: '/ticket/agency',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'PM'
              ? {
                label: i18n['Quản lý thông báo'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/notification'>{i18n['Danh sách']}</Link>,
                    icon: <LuCalendarDays size={21} />,
                    key: '/notification',
                    className: 'side-menu-item',
                  },
                  {
                    label: <Link href='/notification-report'>{'Thống kê'}</Link>,
                    icon: <FaRegListAlt size={22} />,
                    key: '/notification-report',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'PM'
              ? {
                label: i18n['Hoạt động'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/monitor'>{i18n['Danh sách']}</Link>,
                    icon: <FaRegListAlt size={22} />,
                    key: '/monitor',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            userData.role === 'EXPERT'
              ? {
                label: i18n['Lịch chờ đánh giá'],
                type: 'group',
                children: [
                  {
                    label: <Link href='/ticket/review'>{i18n['Danh sách']}</Link>,
                    icon: <LuCalendarClock size={21} />,
                    key: '/ticket/review',
                    className: 'side-menu-item',
                  },
                ],
              }
              : undefined,
            {
              label: i18n['Tài khoản'],
              type: 'group',
              children: [
                {
                  label: <Link href='/setting'>{i18n['Cài đặt']}</Link>,
                  icon: <BsGear size={22} />,
                  key: '/setting',
                  className: 'side-menu-item',
                },
              ],
            },
          ].filter((i) => !!i) as any
        }
      />
    </MenuWrapper>
  ) : (
    <></>
  );
}

const MenuWrapper = styled.div`
  overflow-y: scroll;
  -ms-overflow-style: none;
  scrollbar-width: none;
  padding: 0 4px;
  &::-webkit-scrollbar {
    display: none;
  }
  & > .ant-menu-light {
    border-inline-end: none !important;
    .ant-menu-item-selected {
      color: ${({ theme }) =>
    theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.85)' : theme.generatedColors[6]};
      background-color: ${({ theme }) =>
    theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.12)' : theme.generatedColors[0]};
    }
  }
  .side-menu-item,
  .ant-menu-submenu-title {
    display: flex;
    align-items: center;
    & > * {
      flex-shrink: 0;
    }
  }
  .ant-menu-item-group-title {
    padding-left: 27px;
  }

  &.collapsed {
    .ant-menu-item-group-title {
      padding-left: 18px;
      font-size: 9px;
    }
    &.en .ant-menu-item-group-title {
      padding-left: 20px;
      font-size: 10px;
    }
    .ant-menu-inline-collapsed .ant-menu-item {
      padding-inline: calc(50% - 8px - 8px);
    }
  }
`;

export default PMMenuNav;
