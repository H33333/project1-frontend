import styled from '@emotion/styled';
import { Menu } from 'antd';
import { useRouter } from 'next/router';
import { BsGear, BsHouseDoor, BsPeople, BsPersonPlus } from 'react-icons/bs';
import { LiaMapMarkedAltSolid } from 'react-icons/lia';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { useAppSelector } from 'src/redux/store';
import Link from '../next/Link';

function AdminMenuNav() {
  const { asPath, locale, push } = useRouter();
  const { i18n } = useChangeLocale();
  const isSiderCollapsed = useAppSelector((s) => s.visible.isSiderCollapsed);

  return (
    <MenuWrapper className={`${isSiderCollapsed ? 'collapsed ' : ''}${locale || ''}`}>
      <Menu
        selectable={false}
        theme='light'
        defaultSelectedKeys={[asPath.split('?')[0]]}
        style={{ backgroundColor: 'transparent' }}
        mode='inline'
        className='side-menu'
        inlineIndent={23}
        items={[
          {
            label: <Link href='/admin'>{i18n['Tổng quan']}</Link>,
            icon: <BsHouseDoor size={22} />,
            key: '/admin',
            className: 'side-menu-item',
          },
          {
            label: i18n['Quản lý đối tác'],
            type: 'group',
            children: [
              {
                label: <Link href='/admin/partner'>{i18n['Danh sách']}</Link>,
                icon: <BsPeople size={22} />,
                key: '/admin/partner',
                className: 'side-menu-item',
              },
              {
                label: <Link href='/admin/partner/create'>{i18n['Thêm mới']}</Link>,
                icon: <BsPersonPlus size={22} />,
                key: '/admin/partner/create',
                className: 'side-menu-item',
              },
            ],
          },
          {
            label: i18n['Khu vực'],
            type: 'group',
            children: [
              {
                label: <Link href='/address'>{i18n['Danh sách']}</Link>,
                icon: <LiaMapMarkedAltSolid size={22} />,
                key: '/address',
                className: 'side-menu-item',
              },
            ],
          },
          {
            label: i18n['Tài khoản'],
            type: 'group',
            children: [
              {
                label: <Link href='/admin/setting'>{i18n['Cài đặt']}</Link>,
                icon: <BsGear size={22} style={{ marginLeft: 0 }} />,
                key: '/admin/setting',
                className: 'side-menu-item',
              },
            ],
          },
        ]}
      />
    </MenuWrapper>
  );
}

const MenuWrapper = styled.div`
  overflow-y: scroll;
  -ms-overflow-style: none;
  scrollbar-width: none;
  padding: 0 4px;
  &::-webkit-scrollbar {
    display: none;
  }
  & > .ant-menu-light {
    border-inline-end: none !important;
    .ant-menu-item-selected {
      color: ${({ theme }) =>
        theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.85)' : theme.generatedColors[6]};
      background-color: ${({ theme }) =>
        theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.12)' : theme.generatedColors[0]};
    }
  }
  .side-menu-item,
  .ant-menu-submenu-title {
    display: flex;
    align-items: center;
    & > * {
      flex-shrink: 0;
    }
  }
  .ant-menu-item-group-title {
    padding-left: 27px;
  }

  &.collapsed {
    .ant-menu-item-group-title {
      padding-left: 18px;
      font-size: 9px;
    }
    &.en .ant-menu-item-group-title {
      padding-left: 20px;
      font-size: 10px;
    }
    .ant-menu-inline-collapsed .ant-menu-item {
      padding-inline: calc(50% - 8px - 8px);
    }
  }
`;

export default AdminMenuNav;
