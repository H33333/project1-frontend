import styled from '@emotion/styled';
import { Divider, Drawer, DrawerProps } from 'antd';
import { BsXLg } from 'react-icons/bs';
import { setVisibleItem } from 'src/redux/reducer/visible.reducer';
import { useAppDispatch, useAppSelector } from 'src/redux/store';
import SideTheme from '../button/SideTheme';
import SiderUserDropdown from '../dropdown/SiderUserDropdown';
import AdminMenuNav from '../menu/AdminMenuNav';
import LogoWithText from '../shared/LogoWithText';

function AdminSiderDrawer(props: Pick<DrawerProps, 'open' | 'onClose'>) {
  const { visibleItem } = useAppSelector((s) => s.visible);

  const dispatch = useAppDispatch();
  return (
    <StyledDrawer
      closeIcon={<BsXLg size={20} />}
      title={<LogoWithText fontSize={28} logoSize={32} />}
      placement={'left'}
      width={220}
      onClose={() => dispatch(setVisibleItem(null))}
      open={visibleItem === 'side-ADMIN-drawer-visible'}
      {...props}
    >
      <AdminMenuNav />
      <footer style={{ marginTop: 'auto', padding: 8 }}>
        <SiderUserDropdown />
        <Divider style={{ margin: '8px 0' }} />
        <SideTheme />
      </footer>
    </StyledDrawer>
  );
}

const StyledDrawer = styled(Drawer)`
  .ant-drawer-header-title {
    flex-direction: row-reverse;
  }
  .ant-drawer-body {
    padding: 0;
    display: flex;
    flex-direction: column;
  }
  .ant-drawer-close {
    margin: 0;
  }
  .ant-drawer-header {
    border: none;
  }
`;

export default AdminSiderDrawer;
