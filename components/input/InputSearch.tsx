import styled from '@emotion/styled';
import { Input, InputProps, theme, Typography } from 'antd';
import { BsSearch } from 'react-icons/bs';

type TInputSearchProps = InputProps & {
  transparent?: boolean;
};

function InputSearch({ value, transparent, ...props }: TInputSearchProps) {
  const {
    token: { colorTextPlaceholder, colorTextSecondary },
  } = theme.useToken();
  return (
    <SearchInputStyled
      className={`input-search ${transparent ? 'transparent' : ''}`}
      allowClear
      placeholder={'...'}
      prefix={<BsSearch size={16} />}
      suffix={
        !!value ? (
          <button type='submit' className='btn-submit' style={{ color: colorTextSecondary }}>
            <Typography.Text code style={{ margin: '0' }}>
              Enter
            </Typography.Text>
          </button>
        ) : (
          <></>
        )
      }
      value={value}
      {...props}
    />
  );
}

const SearchInputStyled = styled(Input)`
  &.transparent {
    background-color: transparent;
    input {
      background-color: transparent;
    }
  }
  .btn-submit {
    margin: -2.2px -6px 0 0;
    padding: 0;
    background-color: transparent;
    outline: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .ant-input-prefix {
    color: var(--colorTextPlaceholder);
  }
  &:focus-within {
    .ant-input-prefix {
      color: ${({ theme }) => theme.colorPrimary};
    }
  }
`;

export default InputSearch;
