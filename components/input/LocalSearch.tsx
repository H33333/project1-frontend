import styled from '@emotion/styled';
import { Form, FormItemProps, FormProps } from 'antd';
import { useId } from 'react';

type TLocalSearchProps<TFormData> = { children: React.ReactNode } & Omit<
  FormProps<TFormData>,
  'children'
>;

const LocalSearch = <T,>({
  onFinish,
  onValuesChange,
  children,
  ...props
}: TLocalSearchProps<T>) => {
  const uid = useId();
  const [form] = Form.useForm<T>();
  return (
    <ContentWrapper className='localsearch'>
      <Form
        id={uid}
        form={form}
        size='large'
        onFinish={onFinish}
        onValuesChange={onValuesChange}
        {...props}
      >
        {children}
      </Form>
    </ContentWrapper>
  );
};

export const FormItem = ({ children, ...props }: FormItemProps) => {
  return (
    <Form.Item noStyle {...props}>
      {children}
    </Form.Item>
  );
};

const ContentWrapper = styled.div`
  border: 1px solid ${({ theme }) => (theme.mode === 'dark' ? '#424242' : '#d9d9d9')};
  background: ${({ theme }) => (theme.mode === 'dark' ? '#141414' : '#fff')};
  border-radius: 8px;
  & > form {
    display: flex;
    align-items: center;
  }
  .ant-input-prefix {
    color: #d9d9d9;
    margin-inline-end: 8px;
  }
  &:focus-within {
    border-color: ${({ theme }) => theme.generatedColors[4]};
  }
  &:hover {
    border-color: ${({ theme }) => theme.generatedColors[3]};
  }
`;

export default LocalSearch;
