import { ButtonProps, Form, Input, Typography } from 'antd';
import { Fragment, useState } from 'react';
import { BsPlusLg } from 'react-icons/bs';
import useApp from 'src/hooks/useApp';
import Button from '../button/Button';
import CarTypeSelect from '../field/CarTypeSelect';
import MapPicker from '../field/MapPicker';
import Modal from '../modal/Modal';

function SoftCreateCarModal({
  buttonProps,
  onOk,
}: {
  buttonProps?: Omit<ButtonProps, 'onClick'>;
  onOk?: (v: any) => void;
}) {
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { notification } = useApp();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then(({ geoLocation, ...formData }) => {
        return onOk?.({
          ...formData,
          lat: String(geoLocation?.lat || ''),
          long: String(geoLocation?.lng || ''),
        });
      })
      .catch((info) => {
        console.log(info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalOpen(false);
  };

  return (
    <Fragment>
      <Button
        loading={isModalOpen}
        block
        icon={<BsPlusLg />}
        type='dashed'
        size='large'
        onClick={showModal}
        {...buttonProps}
      >
        Thêm xe mới
      </Button>
      {isModalOpen && (
        <Modal
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          title='Thêm 01 xe mới'
          okText='Thêm xe mới'
          cancelText='Hủy'
          okButtonProps={{ size: 'large' }}
          cancelButtonProps={{ size: 'large' }}
          style={{ top: 24 }}
        >
          <Form
            form={form}
            layout='vertical'
            size='large'
            initialValues={{
              geoLocation: undefined,
            }}
          >
            <Form.Item name='agency_id' hidden>
              <Input />
            </Form.Item>
            <Form.Item
              name='car_type_id'
              label={
                <div>
                  Loại xe, <Typography.Text type='secondary'>vd: 5 chỗ</Typography.Text>
                </div>
              }
              rules={[{ required: true, message: '• Type is required' }]}
            >
              <CarTypeSelect placeholder='Chọn loại xe' allowClear></CarTypeSelect>
            </Form.Item>
            <Form.Item
              name='name'
              label='Tên xe'
              rules={[{ required: true, message: '• Name is required' }]}
            >
              <Input placeholder='Nhập tên xe..., vd: Toyota Vios' />
            </Form.Item>
            <Form.Item
              name='plates'
              label='Biển số xe'
              rules={[{ required: true, message: '• Plates is required' }]}
            >
              <Input placeholder='Nhập biển số xe..., vd: 29A-12345' />
            </Form.Item>
            <Form.Item name='geoLocation' label='Vị trí xe' help='' style={{ marginBottom: 10 }}>
              <MapPicker style={{ height: 140, zIndex: 0, width: '100%' }} draggable={true} />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Fragment>
  );
}

export default SoftCreateCarModal;
