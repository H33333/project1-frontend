import { ButtonProps, Form, Input } from 'antd';
import { useRouter } from 'next/router';
import { Fragment, useState } from 'react';
import { BsFillPersonPlusFill } from 'react-icons/bs';
import useApp from 'src/hooks/useApp';
import Button from '../button/Button';
import MapPicker from '../field/MapPicker';
import Modal from '../modal/Modal';

function SoftCreateDriverModal({
  buttonProps,
  onOk,
}: {
  buttonProps?: Omit<ButtonProps, 'onClick'>;
  onOk?: (v: any) => void;
}) {
  const { query } = useRouter();
  const agencyId = !!query?.agencyId ? String(query.agencyId) : undefined;
  const { notification } = useApp();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [form] = Form.useForm();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then(({ geoLocation, ...formData }) => {
        return onOk?.({
          ...formData,
          lat_address: String(geoLocation?.lat || ''),
          long_address: String(geoLocation?.lng || ''),
        });
      })
      .catch((info) => {
        console.log(info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalOpen(false);
  };

  return (
    <Fragment>
      <Button
        loading={isModalOpen}
        block
        icon={<BsFillPersonPlusFill />}
        type='dashed'
        size='large'
        onClick={showModal}
        {...buttonProps}
      >
        Thêm lái xe
      </Button>
      {isModalOpen && (
        <Modal
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          title='Thêm 01 lái xe'
          okText='Thêm lái xe'
          cancelText='Hủy'
          okButtonProps={{ size: 'large' }}
          cancelButtonProps={{ size: 'large' }}
          style={{ top: 24 }}
        >
          <Form
            form={form}
            layout='vertical'
            size='large'
            initialValues={{
              lat_long: '',
            }}
          >
            <Form.Item
              name='name'
              label='Tên lái xe'
              rules={[{ required: true, message: '• Name is required' }]}
            >
              <Input placeholder='Tên lái xe..., vd: Nguyen Van A' />
            </Form.Item>
            <Form.Item
              name='phone'
              label='Số điện thoại'
              rules={[{ required: true, message: '• Phone is required' }]}
            >
              <Input type='tel' placeholder='Số điện thoại..., vd: 0989123456' />
            </Form.Item>
            <Form.Item name='license_id' label='Số bằng lái xe'>
              <Input type='tel' placeholder='Số bằng lái xe..., vd: 123400006789' />
            </Form.Item>
            <Form.Item name='geoLocation' label='Địa chỉ' help='' style={{ marginBottom: 10 }}>
              <MapPicker
                style={{ height: 140, zIndex: 0, width: '100%' }}
                draggable={true}
                onChangeAddressName={(name) => form.setFieldValue('address', name)}
              />
            </Form.Item>
            <Form.Item name='address' label='Địa chỉ chi tiết'>
              <Input.TextArea placeholder='Nhập địa chỉ...' autoSize={{ minRows: 2 }} showCount />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Fragment>
  );
}

export default SoftCreateDriverModal;
