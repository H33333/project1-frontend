import styled from '@emotion/styled';
import { useId } from 'react';

type TMasonryLayoutProps = {
  id?: string;
  className?: string;
  columns?: number;
  gap?: number;
  children: React.ReactElement[];
};

const MasonryLayout = ({ id, className, children, columns = 3, gap = 24 }: TMasonryLayoutProps) => {
  const uid = useId();
  const columnWrapper: Record<string, React.ReactElement[]> = {};
  const result = [];

  // create columns
  for (let i = 0; i < columns; i++) {
    columnWrapper[`column${i}`] = [];
  }

  // divide children into columns
  for (let i = 0; i < children.length; i++) {
    const columnIndex = i % columns;
    columnWrapper[`column${columnIndex}`].push(
      <div key={`${uid}:${Math.random() * children.length}`} style={{ marginBottom: `${gap}px` }}>
        {children[i]}
      </div>,
    );
  }

  // wrap children in each column with a div
  for (let i = 0; i < columns; i++) {
    result.push(
      <div
        key={`${uid}:column${i}`}
        style={{
          marginLeft: `${i > 0 ? gap : 0}px`,
          flex: 1,
        }}
      >
        {columnWrapper[`column${i}`]}
      </div>,
    );
  }

  return (
    <div id={id} className={className} style={{ display: 'flex' }}>
      {result}
    </div>
  );
};
const MasonryLayoutStyled = styled.div``;

export default MasonryLayout;
