import styled from '@emotion/styled';
import { Pagination, PaginationProps, theme, Typography } from 'antd';
import { useId } from 'react';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { TMetaBase } from 'src/types/response.types';

type TPaginationMetadataProps = { metadata?: TMetaBase } & PaginationProps;

const PaginationMetadata = ({ metadata, ...props }: TPaginationMetadataProps) => {
  const uid = useId();
  const { i18n } = useChangeLocale();
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  if (!metadata) return <></>;
  return (
    <Pagination
      style={{
        backgroundColor: colorBgContainer,
      }}
      total={metadata.total || 0}
      pageSize={metadata.limit}
      current={metadata.page}
      showTotal={(total, range) => (
        <Typography.Text>
          {i18n['Hiển thị']} <b>{range[0]}</b> {i18n['Đến'].toLowerCase()} <b>{range[1]}</b>{' '}
          {i18n['Trong'].toLowerCase()}{' '}
          <u>
            <b>{total}</b>
          </u>
        </Typography.Text>
      )}
      defaultPageSize={10}
      showSizeChanger={true}
      pageSizeOptions={['10', '20', '50']}
      {...props}
    />
  );
};
const PaginationMetadataStyled = styled(Pagination)``;

export default PaginationMetadata;
