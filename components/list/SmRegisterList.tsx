import { Empty, List } from 'antd';
import { useRouter } from 'next/router';
import VirtualList from 'rc-virtual-list';
import { Fragment, useState } from 'react';
import { BsCaretRightFill } from 'react-icons/bs';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useDebounce from 'src/hooks/useDebounce';
import { TListFilter, useGetRegisterListQuery } from 'src/redux/query/register.query';
import { TRegister } from 'src/types/register.types';
import { TMetaBase } from 'src/types/response.types';
import Button from '../button/Button';
import SmRegisterListItem from '../card/SmRegisterListItem';
import InputSearch from '../input/InputSearch';
import LocalSearch, { FormItem } from '../input/LocalSearch';
import Link from '../next/Link';

const initialFilterValue: TListFilter = { page: 1, limit: 10 };

function SmRegisterList() {
  const {
    query: { registerId },
  } = useRouter();
  const { i18n } = useChangeLocale();
  const [filterValue, setFilterValue] = useState<TListFilter>(initialFilterValue);
  const debouncedFilter = useDebounce(filterValue, 500);
  const {
    data: filteredQuery,
    isSuccess: getSuccess,
    isFetching: getFetching,
  } = useGetRegisterListQuery(debouncedFilter, {
    refetchOnMountOrArgChange: true,
  });
  const filteredData = getSuccess ? filteredQuery?.data?.register_list || [] : [];
  const filteredMetaData: TMetaBase | undefined = getSuccess
    ? filteredQuery?.data?.meta_data
    : undefined;
  const handleLocalSearch = ({ keyword }: { keyword: string }) => {
    setFilterValue({ ...filterValue, keyword });
  };
  return (
    <Fragment>
      <LocalSearch<{ keyword: string }>
        onFinish={handleLocalSearch}
        onValuesChange={(changedValue, values) => handleLocalSearch(values)}
      >
        <FormItem name='keyword'>
          <InputSearch bordered={false} placeholder={i18n['Tìm kiếm theo Tên, Số điện thoại']} />
        </FormItem>
      </LocalSearch>
      {!!filteredData.length && !!filteredMetaData ? (
        <List<TRegister>
          loading={getFetching}
          pagination={
            filteredMetaData.totalPage > 1 && {
              onChange: (page, pageSize) =>
                setFilterValue({ ...filterValue, page, limit: pageSize }),
              total: filteredMetaData.total || 0,
              pageSize: filteredMetaData.limit,
              current: filteredMetaData.page,
            }
          }
        >
          <VirtualList
            data={filteredData}
            height={filteredData.length > 8 ? 73 * 7.7 : 73 * filteredData.length}
            itemHeight={73}
            itemKey='_id'
          >
            {(item) => (
              <SmRegisterListItem
                key={item._id}
                {...item}
                actions={
                  <Link href={`/register/${item._id}`}>
                    <Button
                      type='link'
                      disabled={item._id === String(registerId)}
                      style={{ padding: 0 }}
                    >
                      <BsCaretRightFill size={20} />
                    </Button>
                  </Link>
                }
              />
            )}
          </VirtualList>
        </List>
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
    </Fragment>
  );
}

export default SmRegisterList;
