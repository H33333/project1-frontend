import styled from '@emotion/styled';
import { Affix, List, ListProps, Pagination, PaginationProps, theme, Typography } from 'antd';
import { useId, useState } from 'react';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { TMetaBase } from 'src/types/response.types';

type TPagination = PaginationProps & { metadata?: TMetaBase };

type TProps<TItem> = Omit<ListProps<TItem>, 'pagination'> & {
  pagination?: TPagination;
  target?: () => any;
};
function StyledListContainer<TItem>({ pagination, target, ...props }: TProps<TItem>) {
  const uid = useId();
  const { i18n } = useChangeLocale();
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [affixedPagination, setAffixedPagination] = useState<boolean>();

  return (
    <ListContainer>
      <List<TItem> style={{ backgroundColor: colorBgContainer }} pagination={false} {...props} />
      {!!pagination?.metadata && (
        <Affix
          offsetBottom={0.001}
          onChange={(affixed) => setAffixedPagination(affixed)}
          target={target}
          key={uid + String(affixedPagination)}
        >
          <Pagination
            style={{
              backgroundColor: colorBgContainer,
            }}
            className={affixedPagination ? 'affixed' : ''}
            total={pagination.metadata.total || 0}
            pageSize={pagination.metadata.limit}
            current={pagination.metadata.page}
            showTotal={(total, range) => (
              <Typography.Text>
                {i18n['Hiển thị']} <b>{range[0]}</b> {i18n['Đến'].toLowerCase()} <b>{range[1]}</b>{' '}
                {i18n['Trong'].toLowerCase()}{' '}
                <u>
                  <b>{total}</b>
                </u>
              </Typography.Text>
            )}
            defaultPageSize={10}
            showSizeChanger={true}
            pageSizeOptions={['2', '10', '20', '30', '50']}
            {...pagination}
          />
        </Affix>
      )}
    </ListContainer>
  );
}

const ListContainer = styled.div`
  .list-header {
    padding: 0 24px 12px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .list-header-item {
    color: #8e8e8e;
  }
  .list-header-item:first-of-type span {
    font-size: 14px;
  }
  .ant-list {
    border-radius: 8px 8px 0 0;
    padding: 0 24px;
  }
  .ant-pagination {
    padding: 20px 24px;
    display: flex;
    align-items: center;
    border-radius: 0 0 8px 8px;
    .ant-pagination-total-text {
      margin-right: auto;
    }
    &.affixed {
      border-radius: 0;
      border-top: 1px solid rgba(5, 5, 5, 0.06);
      padding: 12px 24px;
    }
  }
  .ant-pagination-total-text {
    display: flex;
    align-items: center;
  }
`;

export default StyledListContainer;
