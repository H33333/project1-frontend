import type { FormProps } from 'antd';
import { Button, Checkbox, Form, Input, Modal } from 'antd';
import { useEffect, useState } from 'react';
import useApp from 'src/hooks/useApp';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useDebounce from 'src/hooks/useDebounce';
import {
  useGetNotificationListQuery,
  useLazyGetNotificationResendQuery,
} from 'src/redux/query/notification.query';
import ListAgencyCheck from '../card/ListAgencyCheck';
import StyledListContainer from '../list/StyledListContainer';
type FieldType = {
  _id?: string;
  title?: string;
  body?: string;
  fail?: number;
  success?: number;
};

interface IProps {
  isModalOpen: boolean;
  view: FieldType;
  handleCancel: any;
}

const ModalNotification = (props: IProps) => {
  const { message } = useApp();
  const { i18n } = useChangeLocale();
  const { isModalOpen, view, handleCancel } = props;
  const [form] = Form.useForm();
  const [trigger, { data, error, isLoading }] = useLazyGetNotificationResendQuery();
  const [usersFilterValue, setUsersFilterValue] = useState<any>();
  const [check, setCheck] = useState<any>([]);


  useEffect(() => {
    if (view._id) {
      setUsersFilterValue({ notification_id: view._id });
    }
  }, [view._id]);

  const debouncedFilter = useDebounce(usersFilterValue, 100);
  const {
    data: NotificationQuery,
    isSuccess: getNotificationSuccess,
    isFetching: getNotificationFetching,
  } = useGetNotificationListQuery(debouncedFilter, { refetchOnMountOrArgChange: true });

  const notificationData = getNotificationSuccess ? (NotificationQuery?.data as any) || [] : [];

  useEffect(() => {
    if (isModalOpen === true) {
      getNotificationSuccess;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [view._id, isModalOpen]);

  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  const handleReSend = () => {
    check.map((item: any) => {
      trigger({ notification_id: item });
    });


    message.success(`${data?.message}`);
  };

  useEffect(() => {
    form.setFieldValue('title', view.title);
    form.setFieldValue('body', view.body);
    form.setFieldValue('fail', view.fail);
    form.setFieldValue('success', view.success);
  }, [view]);

  const onCheckboxChange = (item: any) => (e: any) => {
    // console.log(`Item:`, item);
    // console.log(`Checked:`, e.target.checked);
    // // set;
    if (e.target.checked) {
      setCheck((prevCheck: any) => [...prevCheck, item.id]);
    } else {
      setCheck((prevCheck: any) => prevCheck.filter((id: any) => id !== item.id));
    }
  };

  return (
    <Modal onCancel={handleCancel} title={i18n['Chi tiết']} open={isModalOpen} footer={false}>
      <Form
        form={form}
        name='basic'
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        style={{ maxWidth: 600 }}
        onFinish={onFinish}
        initialValues={{ remember: true }}
        onFinishFailed={onFinishFailed}
        autoComplete='off'
      >
        <Form.Item<FieldType> label='Tiêu đề' name='title'>
          <Input disabled />
        </Form.Item>
        <Form.Item<FieldType> label='Mô tả' name='body'>
          <Input disabled />
        </Form.Item>
        <Form.Item<FieldType> label='Không thành công' name='fail'>
          <Input disabled />
        </Form.Item>

        <Form.Item<FieldType> label='Thành công' name='success'>
          <Input disabled />
        </Form.Item>

        <StyledListContainer<any>
          loading={getNotificationFetching}
          dataSource={notificationData}
          //   target={() => container}
          renderItem={(item, index) => (
            <ListAgencyCheck
              key={item._id + index}
              {...item}
              index={index}
              actions={<Checkbox onChange={onCheckboxChange(item)}></Checkbox>}
            />
          )}
        />

        <Form.Item style={{ marginTop: '10px' }} wrapperCol={{ offset: 16, span: 8 }}>
          <Button type='dashed' onClick={handleCancel}>
            {i18n['Đóng']}
          </Button>
          <Button
            loading={isLoading}
            style={{ marginLeft: '10px' }}
            type='primary'
            onClick={handleReSend}
          >
            Gửi
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ModalNotification;
