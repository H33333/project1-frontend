import styled from '@emotion/styled';
import { useCreation } from 'ahooks';
import { Cascader, CascaderProps, Divider, Typography } from 'antd';
import { useGetProvinceDistrictListQuery } from 'src/redux/query/address.query';
import { vietnameseSlug } from 'src/utils/utils';

type TAddressCombinedSelectProps = {
  hideTitle?: boolean;
  actionsTop?: React.ReactNode;
} & CascaderProps;

const AddressCombinedSelect = ({
  hideTitle,
  actionsTop,
  ...props
}: TAddressCombinedSelectProps) => {
  const { data: getProvinceDistrictListRes, isFetching: getProvinceDistrictListFetching } =
    useGetProvinceDistrictListQuery({ keyword: '' });

  const options = useCreation(() => {
    const provinceDistrictList = getProvinceDistrictListRes?.data?.address_list || [];
    return provinceDistrictList
      .filter((item) => !item.province_id)
      .map((item) => ({
        value: item.id,
        label: item.name,
        isLeaf: false,
        children: provinceDistrictList
          .filter((pd) => !!pd.province_id && pd.province_id === item.id)
          .map((item1) => ({ value: item1.id, label: item1.name, isLeaf: true })),
      }));
  }, [getProvinceDistrictListRes]);

  return (
    <CascaderStyled
      loading={getProvinceDistrictListFetching}
      options={options}
      dropdownRender={(menus) => (
        <DropdownCascaderStyled>
          {!hideTitle && (
            <>
              <div className='title-wrapper'>
                <Typography.Paragraph
                  style={{ margin: 0, lineHeight: 1, fontSize: 15 }}
                  type='secondary'
                  className='title'
                >
                  Chọn khu vực Tỉnh thành, Quận huyện đón:
                </Typography.Paragraph>
                <div className='actions-top'>{actionsTop}</div>
              </div>

              <Divider style={{ margin: 0 }} />
            </>
          )}
          {menus}
        </DropdownCascaderStyled>
      )}
      expandTrigger={'click'}
      showSearch={{
        filter: (inputValue, path) => {
          return path.some(
            (option) =>
              vietnameseSlug(String(option.label), ' ').indexOf(vietnameseSlug(inputValue, ' ')) >
              -1,
          );
        },
      }}
      {...props}
    />
  );
};

const CascaderStyled = styled(Cascader)``;

const DropdownCascaderStyled = styled.div`
  min-width: 216px;
  position: relative;
  .title-wrapper {
    display: flex;
    align-items: center;
    padding: 0 4px 0 10px;
    height: 45px;
    & > .title {
      flex: 1 1 auto;
      min-width: 0px;
      padding: 1px 4px 0 0;
    }
    & > .actions-top {
      flex: 0 0 auto;
      min-width: 0px;
      margin-left: auto;
    }
  }
  .ant-cascader-menu {
    min-width: 200px;
    min-height: 262px;
    &:first-of-type {
      min-width: 216px;
    }
    &::-webkit-scrollbar {
      width: 6px;
    }
    &::-webkit-scrollbar-track {
      background-color: #f1f3f4;
    }
    &::-webkit-scrollbar-thumb {
      background-color: #70757a;
      border-radius: 8px;
    }
  }
`;

export default AddressCombinedSelect;
