import styled from '@emotion/styled';
import { Divider, Input, Tooltip, Typography } from 'antd';
import { BsQuestionCircleFill } from 'react-icons/bs';
import { FcGoogle } from 'react-icons/fc';
import { DEFAULT_POSITION } from 'src/utils/constant';

type TLatLngInputProps = {
  value?: string;
  onChange?: (v?: string) => void;
  placeholder?: string;
  bordered?: boolean;
  autoSize?: boolean | { minRows?: number; maxRows?: number };
  classNameWrapper?: string;
};

const LatLngInput = ({
  onChange,
  placeholder,
  value,
  classNameWrapper,
  ...props
}: TLatLngInputProps) => {
  return (
    <LatLngInputStyled className={classNameWrapper}>
      <Input.TextArea
        value={value}
        onChange={(e) => {
          onChange?.(e.target.value?.replace(/\s/g, '') || undefined);
        }}
        autoSize={{ maxRows: 2 }}
        style={{ resize: 'none' }}
        placeholder={placeholder}
        {...props}
      />
      <div className='help-text'>
        <Tooltip title='Xem vị trí trên bản đồ GoogleMap'>
          <a
            className='google-map-link'
            target='_blank'
            rel='noopener noreferrer'
            href={`https://www.google.com/maps/search/?api=1&query=${
              value || String(DEFAULT_POSITION.lat) + ',' + String(DEFAULT_POSITION.lng)
            }`}
          >
            <FcGoogle size={16} />
          </a>
        </Tooltip>
        <Divider type='vertical' />
        <Tooltip
          overlayStyle={{ maxWidth: 400 }}
          title={
            <div>
              <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                {`Lấy vĩ độ(latitude) và kinh độ(longitude) từ đường dẫn`}
              </Typography.Paragraph>
              <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                {`GoogleMap: (...google.com/maps/.../@`}
                <Typography.Text type='success'>latitude,longitude</Typography.Text>
                {`,...)`}
              </Typography.Paragraph>
              <Typography.Text style={{ color: '#fff' }}>--- hoặc ---</Typography.Text>
              <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                {`Kéo thả tới vị trí trên bản đồ`}
              </Typography.Paragraph>
            </div>
          }
        >
          <BsQuestionCircleFill className='icon-ques' />
        </Tooltip>
      </div>
    </LatLngInputStyled>
  );
};
const LatLngInputStyled = styled.div`
  position: relative;
  .help-text {
    position: absolute;
    top: 0;
    right: 0;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    transform: translateY(calc(-100% - 10px));
    & > * {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  &.help-text-in-input {
    .help-text {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
    }
  }
`;

export default LatLngInput;
