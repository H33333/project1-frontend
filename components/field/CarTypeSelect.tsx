import styled from '@emotion/styled';
import { Select, SelectProps, Typography } from 'antd';
import { useGetAllCarTypesQuery } from 'src/redux/query/car.query';
import { Xe16Icon, Xe29Icon, Xe35Icon, Xe45Icon, Xe5Icon, Xe7Icon, XeL9Icon } from '../icons';

type TCarTypeSelectProps = {
  imageSize?: number;
  styleItem?: React.CSSProperties;
} & SelectProps;

const CarTypeSelect = ({ imageSize, styleItem, ...props }: TCarTypeSelectProps) => {
  const { data: getCarTypeRes, isFetching } = useGetAllCarTypesQuery({});
  const carTypeList = getCarTypeRes?.data.car_types || [];
  return (
    <Select
      loading={isFetching}
      placeholder='Chọn loại xe'
      options={carTypeList.map(({ name_id, name }) => ({
        value: name_id,
        label: (
          <SelectItemStyled style={styleItem} className='cartype-item'>
            <div className='right-wrapper'>
              <Typography.Text ellipsis>{name}</Typography.Text>
            </div>
            <div className={`icon-wrapper icon${name_id}`}>
              {name_id === 1 && <Xe5Icon />}
              {name_id === 2 && <Xe7Icon />}
              {name_id === 3 && <Xe16Icon />}
              {name_id === 4 && <Xe29Icon />}
              {name_id === 5 && <Xe35Icon />}
              {name_id === 6 && <Xe45Icon />}
              {name_id === 7 && <XeL9Icon />}
            </div>
          </SelectItemStyled>
        ),
      }))}
      {...props}
    ></Select>
  );
};
export const SelectItemStyled = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  gap: 8px;
  flex: 1 1 auto;
  min-width: 0px;
  & > * {
    flex-shrink: 0;
  }
  .icon-wrapper {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    flex: 0 0 auto;
    min-width: 0px;
    opacity: 0.9;
    svg {
      width: auto;
      height: 16px;
    }
    &.icon1 svg {
      width: auto;
      height: 13px;
    }
    &.icon3 svg {
      width: auto;
      height: 17px;
    }
  }
  .right-wrapper {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex: 1 1 auto;
    min-width: 0px;
  }
`;

export default CarTypeSelect;
