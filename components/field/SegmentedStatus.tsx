import styled from '@emotion/styled';
import { Segmented, SegmentedProps } from 'antd';
import { FaLock, FaUnlockAlt } from 'react-icons/fa';

type TSegmentedStatusProps = { status?: number } & Omit<SegmentedProps, 'ref' | 'options'>;

const SegmentedStatus = ({ status, value, ...props }: TSegmentedStatusProps) => {
  return (
    <SegmentedStatusStyled
      value={status === 1 ? 'unlock' : 'lock'}
      options={[
        {
          value: 'unlock',
          icon: <FaUnlockAlt />,
        },
        {
          value: 'lock',
          icon: <FaLock />,
        },
      ]}
      className={status === 1 ? 'unlock' : 'lock'}
      {...props}
    />
  );
};
const SegmentedStatusStyled = styled(Segmented)`
  background-color: ${({ theme }) => (theme.mode === 'dark' ? '#141414' : '#f0f0f0')};
  .ant-segmented-group label {
    color: #8e8e8e;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &.lock .ant-segmented-item-selected {
    color: red;
  }
  &.unlock .ant-segmented-item-selected {
    color: ${({ theme }) => theme.generatedColors[6]};
  }
`;

export default SegmentedStatus;
