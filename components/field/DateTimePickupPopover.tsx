import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { DatePicker, Divider, Radio, theme, Typography } from 'antd';
import { useId } from 'react';
import { dayjs } from 'src/utils/utils-date';
import Button from '../button/Button';
import Container from '../shared/Container';

type TDateTimePickupPopoverProps = {
  defaultValue?: number;
  value?: number;
  onChange?: (value?: number) => void;
  style?: React.CSSProperties;
  placeholder?: string;
  actionsTop?: React.ReactNode;
  disabledBeforeNow?: boolean;
};

const DateTimePickupPopover = ({
  defaultValue,
  value,
  onChange,
  style,
  placeholder,
  actionsTop,
  disabledBeforeNow = true,
}: TDateTimePickupPopoverProps) => {
  const uid = useId();
  const {
    token: { colorTextPlaceholder },
  } = theme.useToken();
  const [open, setOpen] = useSafeState(false);
  const [internalValue, setInternalValue] = useSafeState<number | undefined>(undefined);

  return (
    <DatePicker
      open={open}
      onOpenChange={(o) => {
        setOpen(o);
        setInternalValue(value);
      }}
      style={style}
      placeholder={!open ? placeholder : dayjs().format('DD/MM/YYYY HH:mm')}
      showNow={false}
      allowClear={false}
      defaultValue={undefined}
      disabledDate={
        !!disabledBeforeNow ? (date) => !!date && date < dayjs().startOf('d') : undefined
      }
      showTime={{ format: 'HH:mm', showNow: false }}
      format={'DD/MM/YYYY HH:mm'}
      value={!!internalValue ? dayjs(internalValue) : !!value ? dayjs(value) : undefined}
      onSelect={(dateValue) => {
        let newDateValue = dateValue.valueOf();
        setInternalValue(newDateValue);
        setOpen(true);
      }}
      panelRender={(originPanel) => (
        <DateTimePickupPopoverStyled>
          <div className='title-wrapper'>
            <Typography.Paragraph
              style={{ margin: 0, lineHeight: 1, fontSize: 16, flexShrink: 0 }}
              type='secondary'
              className='title'
            >
              Chọn thời gian đón:
            </Typography.Paragraph>
            <div className='actions-top'>{actionsTop}</div>
          </div>
          <Divider style={{ margin: 0 }} />
          <div className='wrapper'>
            <div className='right-wrapper'>{originPanel}</div>
            {dayjs(internalValue || undefined).isBefore(dayjs().endOf('day')) && (
              <Container className='left-wrapper'>
                <Radio.Group
                  value={internalValue}
                  onChange={(e) => {
                    setInternalValue(e.target.value);
                    setOpen(true);
                  }}
                  rootClassName='quick-wrapper'
                  size='large'
                >
                  {[
                    {
                      value: dayjs().startOf('minute').add(15, 'minute').valueOf(),
                      text: '15 phút sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(30, 'minute').valueOf(),
                      text: '30 phút sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(60, 'minute').valueOf(),
                      text: '1 giờ sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(120, 'minute').valueOf(),
                      text: '2 giờ sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(240, 'minute').valueOf(),
                      text: '4 giờ sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(360, 'minute').valueOf(),
                      text: '6 giờ sau',
                    },
                    {
                      value: dayjs().startOf('minute').add(720, 'minute').valueOf(),
                      text: '12 giờ sau',
                    },
                  ].map((item, index) => (
                    <Radio.Button value={item.value} key={uid + 'quick' + index}>
                      <span style={{ marginRight: 8 }}>{item.text}</span>
                      <span style={{ color: colorTextPlaceholder }}>
                        {dayjs(item.value).format('HH:mm')}
                      </span>
                    </Radio.Button>
                  ))}
                </Radio.Group>
              </Container>
            )}
          </div>
          <Divider style={{ margin: 0 }} />
          <div className='footer'>
            <div className='left-wrapper'>
              <Typography.Text style={{ fontSize: 15, lineHeight: 1.25 }} className='date-text'>
                {!!internalValue
                  ? `Đã chọn • ${dayjs(internalValue).calendar()}`
                  : dayjs().format('DD/MM/YYYY HH:mm')}
              </Typography.Text>
            </div>
            <div className='right-wrapper'>
              <Button
                type='text'
                style={{ height: 32, marginRight: 8 }}
                htmlType='button'
                onClick={() => {
                  setInternalValue(undefined);
                  onChange?.(value);
                  setOpen(false);
                }}
              >
                Hủy
              </Button>
              <Button
                disabled={!internalValue || internalValue < Date.now()}
                type='primary'
                style={{ height: 32 }}
                htmlType='button'
                onClick={() => {
                  onChange?.(internalValue);
                  setInternalValue(undefined);
                  setOpen(false);
                }}
              >
                Xác nhận
              </Button>
            </div>
          </div>
        </DateTimePickupPopoverStyled>
      )}
    />
  );
};
const DateTimePickupPopoverStyled = styled.div`
  & > .title-wrapper {
    display: flex;
    align-items: center;
    padding: 0 4px 0 11px;
    height: 45px;
    & > .title {
      flex: 0 0 auto;
      min-width: 0px;
      padding: 1px 4px 0 0;
      font-size: 16px;
    }
    & > .actions-top {
      margin-left: 24px;
      flex: 1 1 auto;
      min-width: 0px;
      margin-left: auto;
    }
  }
  & > .wrapper {
    display: flex;
    & > .left-wrapper {
      height: 294px;
      max-width: 200px;
      border-left: 1px solid rgba(0, 0, 0, 0.05);
      padding: 10px 10px 0 10px;
    }
    & > .right-wrapper {
      height: 294px;
      padding: 0;
      flex: 1 1 auto;
      min-width: 200px;
    }
  }
  & > .footer {
    padding: 10px;
    display: flex;
    align-items: center;
    height: 48px;
    & > .left-wrapper {
      flex: 1 1 auto;
      min-width: 0px;
      & > .date-text {
        background-color: rgba(0, 0, 0, 0.05);
        border-radius: 8px;
        padding: 4px 8px;
      }
    }
    & > .right-wrapper {
      flex: 0 0 auto;
      min-width: 0px;
      margin-left: 12px;
    }
  }
  .quick-wrapper {
    width: 100%;
    display: flex;
    flex-direction: column;
    & > .ant-radio-button-wrapper {
      border-radius: 8px;
      border-width: 1px;
      padding-inline: 8px;
      margin-bottom: 10px;
      font-size: 14px;
      & > span:last-of-type {
        display: flex;
        justify-content: space-between;
        align-items: center;
      }
      &::before {
        display: none;
      }
    }
  }
  .ant-picker-panel-layout {
    .ant-picker-panel {
      width: 100%;
    }
    .ant-picker-body {
      padding: 0 10px;
    }
    .ant-picker-date-panel {
      flex: 1 1 auto;
    }
    .ant-picker-date-panel .ant-picker-content {
      width: 100%;
    }
    .ant-picker-time-panel-column {
      -ms-overflow-style: none;
      scrollbar-width: none;
      &::-webkit-scrollbar {
        display: none;
      }
    }
    .ant-picker-footer {
      display: none;
    }
  }
`;

export default DateTimePickupPopover;
