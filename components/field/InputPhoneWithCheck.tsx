import styled from '@emotion/styled';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import { useDebounce } from 'ahooks';
import { Form, FormItemProps, Input, InputProps } from 'antd';
import { useCheckAgencyPhoneQuery } from 'src/redux/query/agency.query';
import Link from '../next/Link';
type TInputPhoneWithCheckProps = {
  inputProps?: InputProps;
} & FormItemProps;

const InputPhoneWithCheck = ({ inputProps, name, rules, ...props }: TInputPhoneWithCheckProps) => {
  const form = Form.useFormInstance();
  const phoneWatch = Form.useWatch(name, form);
  const phoneDebounced = useDebounce(phoneWatch, { wait: 500 });

  const { isLoading, currentData } = useCheckAgencyPhoneQuery(
    !!phoneDebounced ? { phone: phoneDebounced } : skipToken,
  );

  // const [checkAgencyPhoneMutate, { isLoading, data }] = useCheckAgencyPhoneMutation();
  const checkAgencyData = currentData?.data?.agency_info;

  return (
    <Form.Item
      hasFeedback={!!checkAgencyData}
      validateStatus={isLoading ? 'validating' : !!checkAgencyData ? 'error' : undefined}
      name={name}
      rules={rules}
      help={
        !!checkAgencyData ? (
          <ErrorHelpWrapper>
            {`• ${currentData?.message}`}{' '}
            <Link href={`/agency/${checkAgencyData._id}`}>{checkAgencyData.name}</Link>
          </ErrorHelpWrapper>
        ) : undefined
      }
      {...props}
    >
      <Input type='tel' {...inputProps} />
    </Form.Item>
  );
};
const ErrorHelpWrapper = styled.div`
  margin-top: 2px;
  line-height: 1.2;
  & > a {
    line-height: 1.2;
  }
`;

export default InputPhoneWithCheck;
