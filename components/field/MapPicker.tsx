import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { Divider, Form, Input, Tooltip, Typography } from 'antd';
import { memo, useId, useRef } from 'react';
import { BsQuestionCircleFill } from 'react-icons/bs';
import { FcGoogle } from 'react-icons/fc';
import { FullscreenControl, MapRef, Marker, ScaleControl } from 'react-map-gl/maplibre';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { useReverseGeocodingMutation } from 'src/redux/query/nominatim.query';
import { DEFAULT_POSITION } from 'src/utils/constant';
import MapGL from '../map/MapGL';
import MapPopupName from '../map/MapPopupName';

type TMapPickerProps = {
  style?: React.CSSProperties;
  hidePopup?: boolean;
  draggable?: boolean;
  value?: {
    lat: number;
    lng: number;
  };
  onChange?: (value?: { lat: number; lng: number }) => void;
  onChangeAddressName?: (value?: string) => void;
};

const MapPicker = ({
  draggable,
  value,
  hidePopup,
  onChange,
  onChangeAddressName,
  style,
}: TMapPickerProps) => {
  const uid = useId();
  const mapRef = useRef<MapRef>(null);
  const { i18n } = useChangeLocale();
  const [internalValue, setInternalValue] = useSafeState<string | undefined>(
    !!value ? Object.values(value).join(',') : undefined,
  );
  const [mutateReverseGeocoding, { data, isLoading }] = useReverseGeocodingMutation();
  const { status } = Form.Item.useStatus();

  return (
    <MapPickerStyled>
      <div className='input-wrapper'>
        <Input
          value={internalValue}
          style={{ minWidth: 312, borderRadius: 4 }}
          onChange={(e) => {
            try {
              const newValue = e.target.value.replace(/\s/g, '');
              if (!!newValue) {
                const [newLat, newLng] = newValue.split(',');
                if (!!+newLat && !!+newLng) {
                  const newGeoLocation = {
                    lat: +newLat,
                    lng: +newLng,
                  };
                  mutateReverseGeocoding(newGeoLocation)
                    .unwrap()
                    .then((res) => {
                      onChangeAddressName?.(res?.display_name);
                    })
                    .catch(() => {})
                    .finally(() => {
                      setInternalValue(`${newGeoLocation.lat},${newGeoLocation.lng}`);
                      onChange?.(newGeoLocation);
                      mapRef.current?.setCenter(newGeoLocation);
                      mapRef.current?.easeTo({
                        padding: { left: 0, bottom: 0, top: 140, right: 0 },
                        duration: 100,
                      });
                      mapRef.current?.setZoom(18);
                    });
                } else {
                  setInternalValue(undefined);
                  onChange?.(undefined);
                }
              } else {
                setInternalValue(undefined);
                onChange?.(undefined);
              }
            } catch (error) {}
          }}
          onPressEnter={(e) => {
            e.preventDefault();
          }}
          status={status === 'error' ? 'error' : undefined}
          placeholder={'Nhập vĩ độ,kinh độ địa chỉ hiện tại'}
          suffix={
            <div className='previous-actions'>
              <Tooltip title='Xem vị trí trên bản đồ GoogleMap'>
                <a
                  className='google-map-link'
                  target='_blank'
                  href={`https://www.google.com/maps/search/?api=1&query=${
                    value?.lat || DEFAULT_POSITION.lat
                  },${value?.lng || DEFAULT_POSITION.lng}`}
                  rel='noreferrer'
                >
                  <FcGoogle size={16} />
                </a>
              </Tooltip>
              <Divider type='vertical' style={{ height: 24 }} />
              <Tooltip
                overlayStyle={{ maxWidth: 400 }}
                title={
                  <div>
                    <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                      {`Lấy vĩ độ(latitude) và kinh độ(longitude) từ đường dẫn`}
                    </Typography.Paragraph>
                    <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                      {`GoogleMap: (...google.com/maps/.../@`}
                      <Typography.Text type='success'>latitude,longitude</Typography.Text>
                      {`,...)`}
                    </Typography.Paragraph>
                    <Typography.Text style={{ color: '#fff' }}>--- hoặc ---</Typography.Text>
                    <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                      {`Kéo thả tới vị trí trên bản đồ`}
                    </Typography.Paragraph>
                  </div>
                }
              >
                <BsQuestionCircleFill className='icon-ques' />
              </Tooltip>
            </div>
          }
        />
      </div>
      <MapGL
        ref={mapRef}
        initialViewState={{
          latitude: value?.lat || DEFAULT_POSITION.lat,
          longitude: value?.lng || DEFAULT_POSITION.lng,
          zoom: 16,
          padding: { left: 0, bottom: 0, top: 100, right: 0 },
        }}
        maxZoom={20}
        minZoom={2}
        style={{ width: '100%', ...style }}
        key={uid + 'MapGL'}
      >
        <FullscreenControl position='bottom-right' />
        <ScaleControl position='bottom-left' />
        <Marker
          color='red'
          latitude={value?.lat || DEFAULT_POSITION.lat}
          longitude={value?.lng || DEFAULT_POSITION.lng}
          draggable={draggable}
          scale={0.8}
          onDragEnd={(e) => {
            const newGeoLocation = e.lngLat;
            mutateReverseGeocoding(newGeoLocation)
              .unwrap()
              .then((res) => {
                onChangeAddressName?.(res?.display_name);
              })
              .catch(() => {})
              .finally(() => {
                setInternalValue(`${newGeoLocation.lat},${newGeoLocation.lng}`);
                onChange?.(newGeoLocation);
              });
          }}
        />
        {!hidePopup && value?.lat && value?.lng ? (
          <MapPopupName
            key={uid + String(value)}
            latitude={value.lat}
            longitude={value.lng}
            closeButton={false}
            closeOnClick={false}
            anchor='bottom'
            offset={[0, -22] as any}
            onClose={() => {}}
            style={{
              padding: 0,
              zIndex: 0,
            }}
            className='hide-arrow'
            nameProps={{ style: { right: 2, bottom: 8 } }}
          />
        ) : (
          <></>
        )}
      </MapGL>
    </MapPickerStyled>
  );
};
const MapPickerStyled = styled.div`
  position: relative;
  .input-wrapper {
    position: relative;
    top: 0;
    left: 0;
    z-index: 10;
    margin-bottom: 2px;
  }
  .previous-actions {
    display: flex;
    align-items: center;
    z-index: 10;
    .icon-ques {
      cursor: pointer;
    }
    .google-map-link {
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  .maplibregl-ctrl-bottom-right.maplibregl-ctrl-bottom-right .maplibregl-ctrl-attrib {
    display: none;
  }
`;

export default memo(MapPicker);
