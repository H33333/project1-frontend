import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { Input, Space, Typography } from 'antd';
import { BsSearch } from 'react-icons/bs';
import useChangeLocale from 'src/hooks/useChangeLocale';
import Button from '../button/Button';

type TFilterSearchCompactProps = {
  extra?: React.ReactNode;
  value?: string;
  onChange?: (v?: string) => void;
  size?: 'small' | 'middle' | 'large' | undefined;
  placeholder?: string;
  block?: boolean;
};

const FilterSearchCompact = ({
  extra,
  value,
  onChange,
  placeholder,
  size,
  block,
}: TFilterSearchCompactProps) => {
  const { i18n } = useChangeLocale();
  const [isShowSearch, setIsShowSearch] = useSafeState(false);
  return (
    <Space.Compact block={block} size={size} className='filter-search-compact'>
      {isShowSearch ? (
        <Input
          allowClear
          autoFocus
          placeholder={placeholder}
          value={value}
          onChange={(e) => onChange?.(e.target.value)}
          suffix={
            <Typography.Text
              className='cancel-search-action'
              type='secondary'
              underline
              onClick={() => {
                setIsShowSearch(false);
                onChange?.('');
              }}
              title='Hide search'
            >
              {i18n['Hủy']}
            </Typography.Text>
          }
        />
      ) : (
        extra
      )}
      <Button
        type={isShowSearch ? 'primary' : 'dashed'}
        icon={<BsSearch size={18} />}
        onClick={() => setIsShowSearch(true)}
      ></Button>
    </Space.Compact>
  );
};
const FilterSearchCompactStyled = styled(Space.Compact)``;

export default FilterSearchCompact;
