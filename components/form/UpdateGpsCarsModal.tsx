import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { ButtonProps, Form, Typography } from 'antd';
import { useId } from 'react';
import useApp from 'src/hooks/useApp';
import { useUpdateGpsCarMutation } from 'src/redux/query/car.query';
import { ErrorCode } from 'src/types/response.types';
import Button from '../button/Button';
import MapPicker from '../field/MapPicker';
import Modal from '../modal/Modal';

type TInitialValue = {
  _id: string;
  name?: string;
  plates?: string;
  type?: string;
  geoLocation?: { lat: number; lng: number };
};

type TUpdateGpsCarsModalProps = {
  initialValues?: TInitialValue[];
  buttonProps?: ButtonProps;
};

const UpdateGpsCarsModal = ({ initialValues = [], buttonProps }: TUpdateGpsCarsModalProps) => {
  const uid = useId();
  const { notification } = useApp();
  const [isModalOpen, setIsModalOpen] = useSafeState(false);
  const [form] = Form.useForm();
  const [updateGpsCarMutate, { isLoading }] = useUpdateGpsCarMutation();

  const handleOpen = () => {
    setIsModalOpen(true);
    setTimeout(() => {
      form.resetFields();
    }, 100);
  };
  const handleOk = () => {
    form
      .validateFields()
      .then((formData) => {
        const mappedFormData = formData.car_list
          .map((item: TInitialValue) =>
            !!item.geoLocation
              ? {
                  _id: item._id,
                  lat: String(item.geoLocation.lat),
                  long: String(item.geoLocation.lng),
                }
              : undefined,
          )
          .filter((item: any) => !!item);
        updateGpsCarMutate({
          car_list: mappedFormData,
        })
          .unwrap()
          .then(({ data, message }) => {
            notification.success({ message, placement: 'bottomRight' });
            setIsModalOpen(false);
          })
          .catch((err) => {
            if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
              notification.error({ message: err.error[0].message, placement: 'bottomRight' });
          });
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Button loading={isModalOpen} onClick={handleOpen} {...buttonProps}>
        Cập nhật vị trí
      </Button>
      <Modal
        open={isModalOpen}
        confirmLoading={isLoading}
        onOk={handleOk}
        onCancel={handleCancel}
        title={
          <>
            Cập nhật vị trí cho <u>{initialValues.length}</u> xe
          </>
        }
        okText='Cập nhật vị trí'
        cancelText='Hủy'
        okButtonProps={{ size: 'large', disabled: isLoading }}
        cancelButtonProps={{ size: 'large', disabled: isLoading }}
        width={600}
        style={{ top: 24 }}
      >
        <Form
          form={form}
          layout='vertical'
          size='large'
          disabled={isLoading}
          initialValues={{ car_list: initialValues }}
        >
          <Form.List name='car_list'>
            {(fields, { add, remove }) => (
              <>
                {fields.map((field) => {
                  const foundItem = initialValues[field.name];
                  if (!foundItem) return <></>;
                  return (
                    <Form.Item
                      key={uid + 'geoLocation' + field.key}
                      name={[field.name, 'geoLocation']}
                      label={
                        <Typography.Text>
                          Vị trí xe • <u>{foundItem?.name}</u>, <u>{foundItem?.type}</u>,{' '}
                          <u>{foundItem?.plates}</u>
                        </Typography.Text>
                      }
                      rules={[{ type: 'object', required: true }]}
                      help=''
                      style={{ marginBottom: 12 }}
                    >
                      <MapPicker
                        style={{ height: 140, zIndex: 0, width: '100%' }}
                        draggable={true}
                      />
                    </Form.Item>
                  );
                })}
              </>
            )}
          </Form.List>
        </Form>
      </Modal>
    </>
  );
};
const UpdateGpsCarsModalStyled = styled.div``;

export default UpdateGpsCarsModal;
