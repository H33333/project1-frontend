import styled from '@emotion/styled';
import { Form, Input, InputNumber, theme, Typography } from 'antd';
import { useId } from 'react';
import { BsXLg } from 'react-icons/bs';
import { MdOutlineArrowUpward } from 'react-icons/md';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { useGetAddressDetailQuery } from 'src/redux/query/address.query';
import Button from '../button/Button';

type TUpdateProvinceFormProps = {
  provinceId: string;
  onClose?: () => void;
  style?: React.CSSProperties;
  onFinish?: (formData?: any) => void;
};

const UpdateProvinceForm = ({ onClose, provinceId, style, onFinish }: TUpdateProvinceFormProps) => {
  const uid = useId();
  const {
    token: { colorBorder },
  } = theme.useToken();
  const [formUpdateDistrict] = Form.useForm();
  const { i18n } = useChangeLocale();
  const { data } = useGetAddressDetailQuery(provinceId, { refetchOnMountOrArgChange: true });
  const districtDetail = data?.data;
  if (!districtDetail) return <></>;
  return (
    <UpdateProvinceFormStyled style={style}>
      <Typography.Paragraph strong>{i18n['Cập nhật']}</Typography.Paragraph>
      <Button
        type='text'
        className='close-btn'
        icon={<BsXLg size={18} />}
        onClick={() => onClose?.()}
      />
      <Form
        key={uid + districtDetail.address.id + 'form'}
        form={formUpdateDistrict}
        layout='vertical'
        size='large'
        onFinish={onFinish}
        initialValues={{
          id: districtDetail.address.id,
          name: districtDetail.address.name,
          centerText: [districtDetail.address.center_lat, districtDetail.address.center_long].join(
            ',',
          ),
          priority: districtDetail.address.priority,
        }}
      >
        <Form.Item name={'id'} hidden>
          <Input />
        </Form.Item>
        <Form.Item name={'name'} label={i18n['Tên']} rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item
          name={'priority'}
          label={i18n['Độ ưu tiên trong danh sách']}
          rules={[{ required: true }]}
        >
          <InputNumber step={1} min={1} style={{ width: '100%' }} />
        </Form.Item>
        <div className='actions-wrapper'>
          <Button
            type='text'
            htmlType='button'
            bgColor={colorBorder}
            onClick={() => {
              formUpdateDistrict.resetFields();
            }}
          >
            {i18n['Hủy']}
          </Button>
          <Button type='primary' htmlType='submit' icon={<MdOutlineArrowUpward />}>
            {i18n['Cập nhật']}
          </Button>
        </div>
      </Form>
    </UpdateProvinceFormStyled>
  );
};
const UpdateProvinceFormStyled = styled.div`
  margin-top: 12px;
  padding: 12px 12px;
  background-color: var(--colorBorder);
  border-radius: 6px;
  position: relative;
  & > .close-btn {
    position: absolute;
    top: 6px;
    right: 6px;
  }
  & > form {
    .centerText-wrapper {
      position: relative;
      .centerText-help {
        position: absolute;
        top: 2px;
        right: 0;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        & > * {
          display: flex;
          justify-content: center;
          align-items: center;
        }
      }
    }
    & > .actions-wrapper {
      display: flex;
      gap: 12px;
      justify-content: flex-end;
    }
  }
`;

export default UpdateProvinceForm;
