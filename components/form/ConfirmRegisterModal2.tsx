import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { Form, Input } from 'antd';
import { BiDotsHorizontalRounded } from 'react-icons/bi';
import { BsCheck2Circle, BsCheckCircleFill } from 'react-icons/bs';
import { PiPasswordBold } from 'react-icons/pi';
import useApp from 'src/hooks/useApp';
import useChangeLocale from 'src/hooks/useChangeLocale';
import {
  TConfirmRegisterDetailData,
  useConfirmRegisterMutation,
} from 'src/redux/query/register.query';
import { TRegister } from 'src/types/register.types';
import { ErrorCode } from 'src/types/response.types';
import { mappedErrorToFormError } from 'src/utils/utils-error';
import Button, { TButtonProps } from '../button/Button';
import ProgressTimer from '../loader/ProgressTimer';
import Modal from '../modal/Modal';

type TProps = {
  buttonProps?: Pick<TButtonProps, 'size' | 'block' | 'style' | 'disabled'>;
} & TRegister;

const ConfirmRegisterModal2 = ({ buttonProps, ...item }: TProps) => {
  const { i18n } = useChangeLocale();
  const { message } = useApp();
  const [form] = Form.useForm();
  const [open, setOpen] = useSafeState(false);

  const [confirmMutate, { isLoading: confirmLoading }] = useConfirmRegisterMutation();

  const handleConfirm = ({ lat, long, ...formData }: TConfirmRegisterDetailData) => {
    confirmMutate({ lat: formData.lat_address, long: formData.long_address, ...formData })
      .unwrap()
      .then((res) => {
        message.success(res.message);
      })
      .catch((err) => {
        if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
          message.error(err.error[0].message);
        if (err.response_code === ErrorCode.RequestValidationError) {
          form.setFields(mappedErrorToFormError(err.error));
        }
      })
      .finally(() => {
        setOpen(false);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setOpen(false);
  };

  return (
    <>
      <Button
        loading={open}
        key={JSON.stringify(item) + 'ModalButton'}
        disabled={item.status === 1}
        icon={
          item.status === 1 ? (
            <BsCheckCircleFill size={18} />
          ) : (
            <BiDotsHorizontalRounded size={18} />
          )
        }
        type={'primary'}
        ghost={item.status === 1}
        {...buttonProps}
        onClick={() => {
          setOpen(true);
          form.resetFields();
        }}
      >
        {item.status === 1 ? i18n['Đã xác nhận'] : i18n['Chờ xác nhận']}
      </Button>
      <Modal
        key={JSON.stringify(item) + 'Modal'}
        open={open}
        title={`${i18n['Xác nhận đăng ký']} - ${item.name}`}
        okText={i18n['Xác nhận']}
        cancelText={i18n['Hủy']}
        okButtonProps={{ size: 'large', icon: <BsCheck2Circle />, style: { marginLeft: 12 } }}
        cancelButtonProps={{ size: 'large' }}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose
        onOk={() => {
          form
            .validateFields()
            .then(({ confirm, ...values }) => {
              handleConfirm({ id: item._id, ...values });
            })
            .catch((info) => {
              console.log('Validate Failed:', info);
            });
        }}
      >
        <ModalBodyWrapper>
          <Form
            form={form}
            layout='vertical'
            name='form_in_modal'
            size='large'
            initialValues={{
              code: item.code,
            }}
          >
            <Form.Item name='code' help='' style={{ marginBottom: 2 }}>
              <Input
                prefix={<PiPasswordBold />}
                type='tel'
                placeholder='Code...'
                disabled
                size='large'
              />
            </Form.Item>
          </Form>
          <div className='timer-wrapper'>
            <ProgressTimer initialSeconds={5} onEnd={handleCancel} />
          </div>
        </ModalBodyWrapper>
      </Modal>
    </>
  );
};

const ModalBodyWrapper = styled.div`
  position: relative;
  & > .timer-wrapper {
    position: absolute;
    bottom: -14px;
    left: 0;
    transform: translateY(100%);
    z-index: 1;
  }
`;

export default ConfirmRegisterModal2;
