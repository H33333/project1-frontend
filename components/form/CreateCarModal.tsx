import { ButtonProps, Form, Input, Typography } from 'antd';
import { useRouter } from 'next/router';
import { Fragment, useState } from 'react';
import { BsPlusLg } from 'react-icons/bs';
import useApp from 'src/hooks/useApp';
import { agencyApi } from 'src/redux/query/agency.query';
import { useCreateCarMutation } from 'src/redux/query/car.query';
import { useAppDispatch } from 'src/redux/store';
import { TAgency } from 'src/types/agency.types';
import { ErrorCode } from 'src/types/response.types';
import { mappedErrorToFormError } from 'src/utils/utils-error';
import Button from '../button/Button';
import CarTypeSelect from '../field/CarTypeSelect';
import MapPicker from '../field/MapPicker';
import Modal from '../modal/Modal';

function CreateCarModal({
  buttonProps,
  agencyData,
}: {
  buttonProps?: Omit<ButtonProps, 'onClick'>;
  agencyData?: TAgency;
}) {
  const { query } = useRouter();
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { notification } = useApp();
  const [createMutate, { isLoading }] = useCreateCarMutation();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then(({ geoLocation, agency_id, ...formData }) => {
        createMutate({
          ...formData,
          agency_id,
          lat: String(geoLocation?.lat || ''),
          long: String(geoLocation?.lng || ''),
        })
          .unwrap()
          .then(({ data, message }) => {
            notification.success({ message, placement: 'bottomRight' });
            form.resetFields();
            dispatch(agencyApi.util.invalidateTags([{ type: 'Agencies', id: 'ID' }]));
            setIsModalOpen(false);
          })
          .catch((err) => {
            if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
              notification.error({ message: err.error[0].message, placement: 'bottomRight' });
            if ([ErrorCode.RequestValidationError].includes(err.response_code))
              form.setFields(mappedErrorToFormError(err.error));
          });
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalOpen(false);
  };

  return (
    <Fragment>
      <Button
        loading={isModalOpen}
        block
        icon={<BsPlusLg />}
        type='dashed'
        size='large'
        onClick={showModal}
        {...buttonProps}
      >
        Thêm xe mới
      </Button>
      {isModalOpen && (
        <Modal
          open={isModalOpen}
          confirmLoading={isLoading}
          onOk={handleOk}
          onCancel={handleCancel}
          title='Thêm 01 xe mới'
          okText='Thêm xe mới'
          cancelText='Hủy'
          okButtonProps={{ size: 'large', disabled: isLoading }}
          cancelButtonProps={{ size: 'large', disabled: isLoading }}
          style={{ top: 24 }}
        >
          <Form
            form={form}
            layout='vertical'
            size='large'
            initialValues={{
              geoLocation:
                !!agencyData?.lat_address && !!agencyData?.long_address
                  ? { lat: agencyData.lat_address, lng: agencyData.long_address }
                  : undefined,
              agency_id: agencyData?._id,
            }}
            disabled={isLoading}
          >
            <Form.Item name='agency_id' hidden>
              <Input />
            </Form.Item>
            <Form.Item
              name='car_type_id'
              label={
                <div>
                  Loại xe, <Typography.Text type='secondary'>vd: 5 chỗ</Typography.Text>
                </div>
              }
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Type is required' }]}
            >
              <CarTypeSelect placeholder='Chọn loại xe' allowClear></CarTypeSelect>
            </Form.Item>
            <Form.Item
              name='name'
              label='Tên xe'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Name is required' }]}
            >
              <Input placeholder='Nhập tên xe..., vd: Toyota Vios' />
            </Form.Item>
            <Form.Item
              name='plates'
              label='Biển số xe'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Plates is required' }]}
            >
              <Input placeholder='Nhập biển số xe..., vd: 29A-12345' />
            </Form.Item>
            <Form.Item name='geoLocation' label='Vị trí xe' help='' style={{ marginBottom: 10 }}>
              <MapPicker
                style={{ height: 140, zIndex: 0, width: '100%' }}
                draggable={true}
                onChangeAddressName={(name) => form.setFieldValue('address', name)}
              />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Fragment>
  );
}

export default CreateCarModal;
