import { ButtonProps, Form, Input } from 'antd';
import { useRouter } from 'next/router';
import { Fragment, useState } from 'react';
import { BsFillPersonPlusFill } from 'react-icons/bs';
import useApp from 'src/hooks/useApp';
import { useCreateDriverMutation } from 'src/redux/query/driver.query';
import { ErrorCode } from 'src/types/response.types';
import { mappedErrorToFormError } from 'src/utils/utils-error';
import Button from '../button/Button';
import MapPicker from '../field/MapPicker';
import Modal from '../modal/Modal';

function CreateDriverModal({ buttonProps }: { buttonProps?: Omit<ButtonProps, 'onClick'> }) {
  const { query } = useRouter();
  const agencyId = !!query?.agencyId ? String(query.agencyId) : undefined;
  const { notification } = useApp();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [form] = Form.useForm();

  const [createMutate, { isLoading }] = useCreateDriverMutation();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then(({ geoLocation, ...formData }) => {
        createMutate({
          agency: agencyId,
          ...formData,
          lat_address: String(geoLocation?.lat || ''),
          long_address: String(geoLocation?.lng || ''),
        })
          .unwrap()
          .then(({ data, message }) => {
            notification.success({ message, placement: 'bottomRight' });
            form.resetFields();
            setIsModalOpen(false);
          })
          .catch((err) => {
            if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
              notification.error({ message: err.error[0].message, placement: 'bottomRight' });
            if ([ErrorCode.RequestValidationError].includes(err.response_code))
              form.setFields(mappedErrorToFormError(err.error));
          });
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalOpen(false);
  };

  return (
    <Fragment>
      <Button
        loading={isModalOpen}
        block
        icon={<BsFillPersonPlusFill />}
        type='dashed'
        size='large'
        onClick={showModal}
        {...buttonProps}
      >
        Thêm lái xe
      </Button>
      {isModalOpen && (
        <Modal
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          title='Thêm 01 lái xe'
          okText='Thêm lái xe'
          cancelText='Hủy'
          okButtonProps={{ size: 'large', disabled: isLoading }}
          cancelButtonProps={{ size: 'large', disabled: isLoading }}
          style={{ top: 24 }}
        >
          <Form
            form={form}
            layout='vertical'
            size='large'
            initialValues={{
              lat_long: '',
            }}
            disabled={isLoading}
          >
            <Form.Item
              name='name'
              label='Tên lái xe'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Name is required' }]}
            >
              <Input placeholder='Tên lái xe..., vd: Nguyen Van A' />
            </Form.Item>
            <Form.Item
              name='phone'
              label='Số điện thoại'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Phone is required' }]}
            >
              <Input type='tel' placeholder='Số điện thoại..., vd: 0989123456' />
            </Form.Item>
            <Form.Item name='license_id' label='Số bằng lái xe'>
              <Input type='tel' placeholder='Số bằng lái xe..., vd: 123400006789' />
            </Form.Item>
            <Form.Item name='geoLocation' label='Địa chỉ' help='' style={{ marginBottom: 10 }}>
              <MapPicker
                style={{ height: 140, zIndex: 0, width: '100%' }}
                draggable={true}
                hidePopup
                onChangeAddressName={(name) => form.setFieldValue('address', name)}
              />
            </Form.Item>
            <Form.Item name='address' label='Địa chỉ chi tiết'>
              <Input.TextArea placeholder='Nhập địa chỉ...' autoSize={{ minRows: 2 }} showCount />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </Fragment>
  );
}

export default CreateDriverModal;
