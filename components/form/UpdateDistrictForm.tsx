import styled from '@emotion/styled';
import { Divider, Form, Input, InputNumber, theme, Tooltip, Typography } from 'antd';
import { useId } from 'react';
import { BsQuestionCircleFill, BsXLg } from 'react-icons/bs';
import { FcGoogle } from 'react-icons/fc';
import { MdOutlineArrowUpward } from 'react-icons/md';
import useChangeLocale from 'src/hooks/useChangeLocale';
import { useGetAddressDetailQuery } from 'src/redux/query/address.query';
import { DEFAULT_POSITION } from 'src/utils/constant';
import Button from '../button/Button';

type TUpdateDistrictFormProps = {
  districtId: string;
  onClose?: () => void;
};

const UpdateDistrictForm = ({ onClose, districtId }: TUpdateDistrictFormProps) => {
  const uid = useId();
  const {
    token: { colorBorder },
  } = theme.useToken();
  const [formUpdateDistrict] = Form.useForm();
  const { i18n } = useChangeLocale();
  const { data } = useGetAddressDetailQuery(districtId, { refetchOnMountOrArgChange: true });
  const districtDetail = data?.data;
  if (!districtDetail) return <></>;
  return (
    <UpdateDistrictFormStyled>
      <Typography.Paragraph strong>{i18n['Cập nhật']}</Typography.Paragraph>
      <Button
        type='text'
        className='close-btn'
        icon={<BsXLg size={18} />}
        onClick={() => onClose?.()}
      />
      <Form
        key={uid + districtDetail.address.id + 'form'}
        form={formUpdateDistrict}
        layout='vertical'
        size='large'
        initialValues={{
          name: districtDetail.address.name,
          centerText: [districtDetail.address.center_lat, districtDetail.address.center_long].join(
            ',',
          ),
          priority: districtDetail.address.priority,
        }}
      >
        <Form.Item name={'name'} label={i18n['Tên']} rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <div className='centerText-wrapper'>
          <Form.Item name={'centerText'} label={i18n['Điểm đón']} rules={[{ required: true }]}>
            <Input.TextArea
              autoSize={{ maxRows: 2 }}
              style={{ resize: 'none' }}
              placeholder={i18n['Nhập vĩ độ,kinh độ địa chỉ hiện tại']}
            />
          </Form.Item>
          <div className='centerText-help'>
            <Tooltip title='Xem vị trí trên bản đồ GoogleMap'>
              <a
                className='google-map-link'
                target='_blank'
                rel='noopener noreferrer'
                href={`https://www.google.com/maps/search/?api=1&query=${
                  districtDetail.address.center_lat || DEFAULT_POSITION.lat
                },${districtDetail.address.center_long || DEFAULT_POSITION.lng}`}
              >
                <FcGoogle size={16} />
              </a>
            </Tooltip>
            <Divider type='vertical' />
            <Tooltip
              overlayStyle={{ maxWidth: 400 }}
              title={
                <div>
                  <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                    {`Lấy vĩ độ(latitude) và kinh độ(longitude) từ đường dẫn`}
                  </Typography.Paragraph>
                  <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                    {`GoogleMap: (...google.com/maps/.../@`}
                    <Typography.Text type='success'>latitude,longitude</Typography.Text>
                    {`,...)`}
                  </Typography.Paragraph>
                  <Typography.Text style={{ color: '#fff' }}>--- hoặc ---</Typography.Text>
                  <Typography.Paragraph style={{ color: '#fff', margin: 0 }}>
                    {`Kéo thả tới vị trí trên bản đồ`}
                  </Typography.Paragraph>
                </div>
              }
            >
              <BsQuestionCircleFill className='icon-ques' />
            </Tooltip>
          </div>
        </div>
        <Form.Item
          name={'priority'}
          label={i18n['Độ ưu tiên trong danh sách']}
          rules={[{ required: true }]}
        >
          <InputNumber step={1} min={1} style={{ width: '100%' }} />
        </Form.Item>
        <div className='actions-wrapper'>
          <Button
            type='text'
            htmlType='button'
            bgColor={colorBorder}
            onClick={() => {
              formUpdateDistrict.resetFields();
            }}
          >
            {i18n['Hủy']}
          </Button>
          <Button type='primary' htmlType='submit' icon={<MdOutlineArrowUpward />}>
            {i18n['Cập nhật']}
          </Button>
        </div>
      </Form>
    </UpdateDistrictFormStyled>
  );
};
const UpdateDistrictFormStyled = styled.div`
  margin-top: 6px;
  padding: 12px 12px;
  background-color: var(--colorBorder);
  border-radius: 6px;
  position: relative;
  & > .close-btn {
    position: absolute;
    top: 6px;
    right: 6px;
  }
  & > form {
    .centerText-wrapper {
      position: relative;
      .centerText-help {
        position: absolute;
        top: 2px;
        right: 0;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        & > * {
          display: flex;
          justify-content: center;
          align-items: center;
        }
      }
    }
    & > .actions-wrapper {
      display: flex;
      gap: 12px;
      justify-content: flex-end;
    }
  }
`;

export default UpdateDistrictForm;
