import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { Typography } from 'antd';
import { IoMdCar } from 'react-icons/io';
import { MdVisibility } from 'react-icons/md';
import { TCar } from 'src/types/car.types';
import CheckboxAvatar from '../avatar/CheckboxAvatar';
import { NameWithViewIcon } from '../shared/ItemStyled';
import Tag from '../tag/Tag';

type TCarListItemProps = {
  actions?: React.ReactNode;
  actionsBottom?: React.ReactNode;
  carIcon?: React.ReactNode;
  checked?: boolean;
  disabled?: boolean;
  style?: React.CSSProperties;
  onClick?: () => void;
} & TCar;

const CarListItem = ({
  actions,
  actionsBottom,
  carIcon,
  checked,
  disabled,
  style,
  onClick,
  ...item
}: TCarListItemProps) => {
  const { generatedColors } = useTheme();
  return (
    <CarListItemStyled
      style={style}
      className={`${checked ? 'checked' : ''} ${disabled ? 'disabled' : ''}`}
    >
      <CheckboxAvatar
        value={item._id}
        icon={<IoMdCar />}
        shape='square'
        bg={generatedColors[1]}
        size={68}
        checked={checked}
        disabled={disabled}
      ></CheckboxAvatar>
      <div className='detail-wrapper'>
        <NameWithViewIcon onClick={onClick}>
          <Typography.Text className='code' strong ellipsis style={{ fontSize: 16 }}>
            {item.name}
          </Typography.Text>
          <MdVisibility className='view-icon' size={16} />
        </NameWithViewIcon>
        <Typography.Text className='phone' type='secondary'>
          {item.plates}
        </Typography.Text>
        <div className='type-wrapper'>
          <Tag className='type-tag' icon={carIcon} bordered={false}>
            {item.type}
          </Tag>
        </div>
      </div>
      <div className='actions-wrapper'>{actions}</div>
      <div className='actions-wrapper-bottom'>{actionsBottom}</div>
    </CarListItemStyled>
  );
};

const CarListItemStyled = styled.div`
  display: flex;
  padding: 0 0 12px;
  position: relative;
  .ant-avatar {
    flex-shrink: 0;
  }
  .detail-wrapper {
    margin-left: 12px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    .state-wrapper {
      margin-top: auto;
    }
  }
  & > .state-wrapper {
    position: absolute;
    bottom: 12px;
    right: 0;
  }
  & > .actions-wrapper {
    position: absolute;
    top: 0;
    right: 12px;
    display: flex;
    align-items: center;
    gap: 6px;
  }
  & > .actions-wrapper-bottom {
    position: absolute;
    bottom: 12px;
    right: 12px;
    display: flex;
    align-items: center;
    gap: 6px;
    .underline {
      text-decoration: underline;
    }
  }

  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  &:not(:last-of-type) {
    margin-bottom: 12px;
  }
  .detail-wrapper .type-wrapper {
    margin-top: auto;
    .type-tag {
      justify-content: flex-start;
      height: 26px;
      min-height: 26px;
      font-weight: 500;
      .icon-type {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        opacity: 0.8;
        margin-right: 8px;
        svg {
          width: auto;
          height: 15px;
        }
        &.icon1 svg {
          height: 12px;
        }
        &.icon3 svg {
          height: 16px;
        }
      }
    }
  }
  &.disabled {
    .CheckboxAvatar {
      opacity: 0.5;
    }
  }
`;

export default CarListItem;
