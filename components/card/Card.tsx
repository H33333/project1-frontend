import styled from '@emotion/styled';
import { Card as AntdCard, CardProps, Skeleton } from 'antd';

type TCardProps = CardProps & {};

const Card = ({ children, loading, title, ...props }: TCardProps) => {
  return (
    <CardStyled
      title={loading ? <Skeleton.Button active size='small' /> : title}
      loading={loading}
      {...props}
    >
      {children}
    </CardStyled>
  );
};

const CardStyled = styled(AntdCard)``;
export default Card;
