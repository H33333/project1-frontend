import styled from '@emotion/styled';
import { Typography } from 'antd';
import { BsFillPersonFill } from 'react-icons/bs';
import { TUser } from 'src/types/user.types';
import RoleAvatar from '../avatar/RoleAvatar';
import Link from '../next/Link';

type TPMListItemProps = { actions?: React.ReactNode; style?: React.CSSProperties } & TUser;

const PMListItem = ({ actions, style, ...item }: TPMListItemProps) => {
  return (
    <PMListItemStyled style={style}>
      <Link href={`/admin/partner/${item._id}`} className='image-link'>
        <RoleAvatar
          size={40}
          offset={[0, 34]}
          role={item.role}
          isActive={item.status === 1}
          icon={<BsFillPersonFill />}
        ></RoleAvatar>
      </Link>
      <div className='detail-wrapper'>
        <Link href={`/admin/partner/${item._id}`} className='name'>
          {item.name}
        </Link>
        <Typography.Text type='secondary' className='phone'>
          {item.phone}
        </Typography.Text>
      </div>
      <div className='actions-wrapper'>{actions}</div>
    </PMListItemStyled>
  );
};
const PMListItemStyled = styled.div`
  display: flex;
  padding: 12px 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  & > .image-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .detail-wrapper {
    display: flex;
    flex-direction: column;
    margin-left: 16px;
    & > .name {
      font-weight: 500;
      font-size: 16px;
    }
  }
  & > .actions-wrapper {
    margin-left: auto;
    align-self: center;
  }
`;

export default PMListItem;
