import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { Badge, Typography } from 'antd';
import { BsCardText, BsPersonFill } from 'react-icons/bs';
import { MdPhone, MdStarRate, MdVisibility } from 'react-icons/md';
import { TCarPopulate, TDriver } from 'src/types/driver.types';
import CheckboxAvatar from '../avatar/CheckboxAvatar';
import { NameWithViewIcon } from '../shared/ItemStyled';
import Tag from '../tag/Tag';
import TextDescription from '../text/TextDescription';

type TDriverListItemProps = {
  actions?: React.ReactNode;
  actionsBottom?: React.ReactNode;
  checked?: boolean;
  onClick?: () => void;
} & TDriver;

const DriverListItem = ({
  actions,
  actionsBottom,
  checked,
  onClick,
  ...item
}: TDriverListItemProps) => {
  const { generatedColors } = useTheme();
  return (
    <DriverListItemStyled className={`${checked ? 'checked' : ''}`}>
      <div className='container'>
        <CheckboxAvatar
          value={item._id}
          icon={<BsPersonFill />}
          shape='square'
          bg={generatedColors[1]}
          size={70}
          checked={checked}
        ></CheckboxAvatar>
        <div className='detail-wrapper'>
          <NameWithViewIcon onClick={onClick}>
            <Typography.Text
              className='code'
              strong
              ellipsis
              style={{ fontSize: 18, maxWidth: 200 }}
            >
              {item.name}
            </Typography.Text>
            <MdVisibility className='view-icon' size={16} />
          </NameWithViewIcon>
          <TextDescription
            className='phone'
            style={{ margin: 'auto 0 0 0' }}
            icon={<MdPhone style={{ marginRight: 4 }} />}
            textProps={{ type: 'secondary' }}
          >
            {item.phone}
          </TextDescription>
          <TextDescription
            className='license_id'
            style={{ margin: '0 0 3px 0' }}
            icon={<BsCardText style={{ marginRight: 4 }} />}
            textProps={{ type: 'secondary' }}
          >
            {item.license_id}
          </TextDescription>
          <div className='extra-info-icons'>
            <Tag
              style={{ height: 16 }}
              color={!!item?.rank ? 'gold' : 'default'}
              bordered={false}
              icon={<MdStarRate />}
            >
              {item.rank || '0'}
            </Tag>
          </div>
        </div>
        <div className='actions-wrapper'>{actions}</div>

        <div className='actions-wrapper-bottom'>{actionsBottom}</div>
      </div>
      {item.car_id && (
        <div className='state-wrapper'>
          <div className='line'></div>
          <Tag color='geekblue' bordered={false} style={{ minHeight: 26 }}>
            Đang lái:
            <div style={{ marginLeft: 6 }}>
              <Badge status='processing' />
              <Typography.Text underline style={{ marginLeft: 4, color: 'inherit' }}>
                {(item.car_id as TCarPopulate).name}
              </Typography.Text>
            </div>
          </Tag>
        </div>
      )}
    </DriverListItemStyled>
  );
};
const DriverListItemStyled = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 0 0 12px;
  .container {
    display: flex;
    position: relative;
    min-height: 70px;
    .ant-avatar {
      flex-shrink: 0;
    }
    .detail-wrapper {
      margin-left: 12px;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      .state-wrapper {
        margin-top: auto;
      }
      & > .extra-info-icons {
        height: 16px;
        margin-top: -1px;
        display: flex;
        align-items: center;
      }
    }
    & > .actions-wrapper {
      position: absolute;
      top: 0;
      right: 0;
      display: flex;
      align-items: center;
      gap: 6px;
    }
    & > .actions-wrapper-bottom {
      position: absolute;
      bottom: 0;
      right: 0;
      display: flex;
      align-items: center;
      gap: 6px;
      .underline {
        text-decoration: underline;
      }
    }
  }
  & > .state-wrapper {
    position: relative;
    margin-top: 6px;
    display: flex;
    & > .line {
      --w: 70px;
      flex: 0 0 var(--w);
      margin-right: 12px;
      position: relative;
      &:before {
        content: '';
        position: absolute;
        top: 0;
        height: 60%;
        border-inline-end: 2px solid #d9d9d9;
        left: 50%;
        transform: translateX(-50%);
      }
      &:after {
        content: '';
        position: absolute;
        width: calc(var(--w) / 2);
        height: 60%;
        border-bottom: 2px solid #d9d9d9;
        left: 0;
        transform: translateX(calc(var(--w) / 2));
      }
    }
  }

  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  &:not(:last-of-type) {
    margin-bottom: 12px;
  }
`;

export default DriverListItem;
