import styled from '@emotion/styled';
import { Typography } from 'antd';
import { TRegister } from 'src/types/register.types';
import TypeAgencyAvatar from '../avatar/TypeAgencyAvatar';
import Link from '../next/Link';
import Tag from '../tag/Tag';

type TRegisterListItemProps = {
  actions?: React.ReactNode;
  style?: React.CSSProperties;
} & TRegister;

const RegisterListItem = ({ actions, style, ...item }: TRegisterListItemProps) => {
  return (
    <RegisterListItemStyled style={style}>
      <Link href={`/admin/partner/${item._id}`} className='image-link'>
        <TypeAgencyAvatar
          size={48}
          offset={[0, 34]}
          isActive={item.status === 1}
          isDriver={item.isDriver}
          isTransportation={item.isTransportation}
        ></TypeAgencyAvatar>
      </Link>
      <div className='detail-wrapper'>
        <Link href={`/register/${item._id}`} className='name'>
          {item.name}
        </Link>
        <Typography.Text type='secondary' className='phone'>
          {item.phone}
        </Typography.Text>
      </div>
      <div className='type-wrapper'>
        {item.isTransportation && <Tag color='geekblue'>Transportation</Tag>}
        {item.isDriver && <Tag color='blue'>Driver</Tag>}
      </div>
      <div className='code-wrapper'>
        <Tag color={item.status === 1 ? 'success' : 'error'}>
          <Typography.Text style={{ color: 'inherit' }} copyable={{ tooltips: false }}>
            {item.code}
          </Typography.Text>
        </Tag>
      </div>
      <div className='actions-wrapper'>{actions}</div>
    </RegisterListItemStyled>
  );
};
const RegisterListItemStyled = styled.div`
  display: flex;
  padding: 12px 0;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  & > .image-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .detail-wrapper {
    display: flex;
    flex-direction: column;
    margin-left: 16px;
    & > .name {
      font-weight: 500;
      font-size: 16px;
    }
  }
  & > .type-wrapper {
    display: flex;
    align-items: center;
    gap: 12px;
    margin-left: 24px;
  }
  & > .code-wrapper {
    margin-left: auto;
    display: flex;
    align-items: center;
    .ant-typography-copy {
      color: inherit;
    }
  }
  & > .actions-wrapper {
    margin-left: 24px;
    align-self: center;
  }
`;

export default RegisterListItem;
