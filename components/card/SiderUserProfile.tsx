import styled from '@emotion/styled';
import { Skeleton, Typography } from 'antd';
import { useId } from 'react';
import { useAppSelector } from 'src/redux/store';

type TSiderUserProfileProps = { style?: React.CSSProperties };

const SiderUserProfile = ({ style }: TSiderUserProfileProps) => {
  const uid = useId();
  const userState = useAppSelector((s) => s.user.data);
  const isSiderCollapsed = useAppSelector((s) => s.visible.isSiderCollapsed);

  return !!userState ? (
    <SiderUserProfileStyled className='profile-wrapper' style={style}>
      <div className='top-wrapper'>
        <div className='name-wrapper' style={{ width: 164 }}>
          <Typography.Title level={3} ellipsis style={{ margin: 0 }} className='name'>
            {userState.name}
          </Typography.Title>
        </div>
        <div className='phone-wrapper'>
          <Typography.Text type='secondary' className='phone'>
            {userState.phone}
          </Typography.Text>
        </div>
      </div>
    </SiderUserProfileStyled>
  ) : (
    <div className='profile-wrapper'>
      <Skeleton active />
    </div>
  );
};
const SiderUserProfileStyled = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  .top-wrapper {
    display: flex;
    flex-direction: column;
    & > .name-wrapper {
    }
    & > .phone-wrapper {
      position: relative;
    }
  }
`;

export default SiderUserProfile;
