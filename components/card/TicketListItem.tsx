import styled from '@emotion/styled';
import { Badge, Typography } from 'antd';
import { rgba } from 'emotion-rgba';
import React from 'react';
import { TAllTicketItem, TTicketWait } from 'src/types/ticket.types';
import { dayjs } from 'src/utils/utils-date';
import Avatar from '../avatar/Avatar';
import { CoinIcon } from '../icons';
import Link from '../next/Link';
import { TextDescriptionItem } from '../shared/ItemStyled';
import Tag from '../tag/Tag';

type TTicketListItemProps = {
  actions?: React.ReactNode;
  carIcon?: React.ReactNode;
  mappedAddress?: React.ReactNode;
  avatarIcon?: React.ReactNode;
  className?: string;
  isActive?: boolean;
  wait_list?: TTicketWait[];
} & TAllTicketItem;

const TicketListItem = ({
  actions,
  avatarIcon,
  carIcon,
  mappedAddress,
  className,
  isActive,
  ...item
}: TTicketListItemProps) => {
  return (
    <TicketListItemStyled className={`status${item.status || 0} ${className}`}>
      <div className='title-wrapper'>
        <div className='left-wrapper'>
          <Avatar size={26} style={{ fontSize: 14 }}>
            {avatarIcon}
          </Avatar>
        </div>
        <div className='actions-wrapper'>{actions}</div>
      </div>
      <Typography.Paragraph className='description' style={{ marginBottom: 0, fontSize: 14 }}>
        {item.description}
      </Typography.Paragraph>

      {!!mappedAddress && (
        <TextDescriptionItem>
          <Typography.Text type='secondary' className='label'>
            Khu vực đón:
          </Typography.Text>
          <Typography.Text className='content'>{mappedAddress}</Typography.Text>
        </TextDescriptionItem>
      )}
      {!!item.pickup_time && (
        <TextDescriptionItem>
          <Typography.Text type='secondary' className='label'>
            Thời gian đón:
          </Typography.Text>
          <Typography.Text className='content'>
            {dayjs(item.pickup_time).format('DD/MM/YYYY HH:mm')}
          </Typography.Text>
        </TextDescriptionItem>
      )}

      {!!item.supplier_id && typeof item.supplier_id === 'object' && (
        <TextDescriptionItem>
          <Typography.Text type='secondary' className='label'>
            Đã giao cho:
          </Typography.Text>
          <Link href={`/agency/${item.supplier_id._id}`}>
            <Typography.Text
              ellipsis={{
                tooltip: {
                  title: item.supplier_id.name,
                  arrow: false,
                  placement: 'topLeft',
                },
              }}
              style={{ marginLeft: 0, maxWidth: 100 }}
              underline
            >
              {item.supplier_id.name}
            </Typography.Text>
          </Link>
          <Typography.Text type='secondary' style={{ marginLeft: 1 }}>
            •{item.supplier_id.phone}
          </Typography.Text>
        </TextDescriptionItem>
      )}

      {typeof item.agency_id === 'object' && (
        <TextDescriptionItem>
          <Typography.Text type='secondary' className='label'>
            Người tạo:
          </Typography.Text>
          <Link href={`/agency/${item.agency_id._id}`}>
            <Typography.Text
              ellipsis={{
                tooltip: {
                  title: item.agency_id.name,
                  arrow: false,
                  placement: 'topLeft',
                },
              }}
              style={{ marginLeft: 0, maxWidth: 140 }}
              underline
            >
              {item.agency_id.name}
            </Typography.Text>
          </Link>
          <Typography.Text type='secondary' style={{ marginLeft: 1 }}>
            •{item.agency_id.phone}
          </Typography.Text>
          {!item.agency_id.status && (
            <Typography.Text type='danger' style={{ opacity: 0.7 }}>
              •Đã khóa
            </Typography.Text>
          )}
        </TextDescriptionItem>
      )}

      {!!item.updatedAt && (
        <TextDescriptionItem>
          <Typography.Text type='secondary' className='label'>
            Cập nhật:
          </Typography.Text>
          <Typography.Text className='content'>{dayjs(item.updatedAt).fromNow()}</Typography.Text>
        </TextDescriptionItem>
      )}

      {/* {typeof item.agency_id === 'object' && (
        <Tag className='tag-agency' bordered={false} style={{ height: 30, marginTop: 6 }}>
          <TypeAgencyAvatar
            size={24}
            isDriver={item.agency_id.isDriver}
            isTransportation={item.agency_id.isTransportation}
          />
          <Typography.Text
            ellipsis={{
              tooltip: {
                title: item.agency_id.name,
                arrow: false,
                placement: 'topLeft',
              },
            }}
            style={{ marginLeft: 6, maxWidth: 100 }}
          >
            {item.agency_id.name}
          </Typography.Text>
          <Typography.Text type='secondary' style={{ marginLeft: 4 }}>
            • {item.agency_id.phone}
          </Typography.Text>
        </Tag>
      )} */}
      <div className='others-wrapper'>
        {carIcon}
        <Tag icon={<CoinIcon width={16} height={16} />} bordered={false}>
          {item.coin || 'Free'}
        </Tag>
        {item?.status === 0 && !!item?.wait_list?.length && (
          <Tag icon={<Badge status='processing' />} bordered={false}>
            Có <b>{item.wait_list.length}</b> xe bổ
          </Tag>
        )}
        {item.status === 1 && (
          <Tag icon={<Badge status='processing' />} color='success'>
            Đã giao lịch
          </Tag>
        )}
        {item.status === 2 && (
          <Tag icon={<Badge status='success' />} color='success'>
            Đã hoàn thành
          </Tag>
        )}
        {item.status === 3 && (
          <Tag icon={<Badge status='error' />} color='error'>
            Đã hủy
          </Tag>
        )}
      </div>
    </TicketListItemStyled>
  );
};
const TicketListItemStyled = styled.div`
  padding: 8px 8px 8px;
  border-radius: 8px;
  background-color: rgba(0, 0, 0, 0.01);
  box-shadow: var(--boxShadow);
  margin-bottom: 12px;
  border: 1px solid transparent;
  & > .title-wrapper {
    margin-bottom: 0;
    position: relative;
    height: 30px;
    & > .left-wrapper {
      display: flex;
      align-items: center;
      .ant-avatar {
        opacity: 0.6;
        background-color: ${({ theme }) =>
          theme.mode === 'dark' ? rgba(theme.generatedColors[6], 0.5) : theme.generatedColors[1]};
        display: flex;
        justify-content: center;
        align-items: center;
      }
      & > .ant-tag {
        gap: 6px;
        margin-left: 6px;
      }
    }
    & > .actions-wrapper {
      position: absolute;
      top: 0;
      right: 0;
    }
  }
  & > .description {
    white-space: pre-line;
  }

  & > .others-wrapper {
    border-top: 1px solid var(--colorBorder);
    display: flex;
    align-items: center;
    gap: 8px;
    margin-top: 6px;
    padding-top: 8px;
    & > .ant-tag {
      gap: 6px;
      font-size: 14px;
      height: 28px;
    }
  }
  &.active {
    border: 1px solid
      ${({ theme }) =>
        theme.mode === 'dark' ? theme.generatedColors[6] : theme.generatedColors[1]};
    background-color: ${({ theme }) =>
      theme.mode === 'dark' ? theme.generatedColors[1] : theme.generatedColors[0]};
  }
  &.status1 {
  }
`;

export default TicketListItem;
