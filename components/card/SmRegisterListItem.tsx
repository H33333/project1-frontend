import styled from '@emotion/styled';
import { Typography } from 'antd';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import { BiDotsHorizontalRounded } from 'react-icons/bi';
import { TRegister } from 'src/types/register.types';
import TypeAgencyAvatar from '../avatar/TypeAgencyAvatar';
import Link from '../next/Link';

type TSmRegisterListItemProps = {
  actions?: React.ReactNode;
  style?: React.CSSProperties;
} & TRegister;

const SmRegisterListItem: ForwardRefRenderFunction<HTMLDivElement, TSmRegisterListItemProps> = (
  { actions, style, ...item },
  forwardedRef,
) => {
  return (
    <SmRegisterListItemStyled style={style} ref={forwardedRef}>
      <Link href={`/admin/partner/${item._id}`} className='image-link'>
        <TypeAgencyAvatar
          size={40}
          offset={[0, 33]}
          isActive={item.status === 1}
          isDriver={item.isDriver}
          isTransportation={item.isTransportation}
          customBadge={item.status === 0 && <BiDotsHorizontalRounded size={10.2} />}
        ></TypeAgencyAvatar>
      </Link>
      <div className='detail-wrapper'>
        <Link href={`/register/${item._id}`} className='name'>
          {item.name}
        </Link>
        <Typography.Text type='secondary' className='phone'>
          {item.phone}
        </Typography.Text>
      </div>
      <div className='actions-wrapper'>{actions}</div>
    </SmRegisterListItemStyled>
  );
};
const SmRegisterListItemStyled = styled.div`
  display: flex;
  padding: 12px 0 12px 4px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  & > .image-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .detail-wrapper {
    display: flex;
    flex-direction: column;
    margin-left: 12px;
    & > .name {
      font-weight: 500;
      font-size: 16px;
    }
  }
  & > .actions-wrapper {
    margin-left: auto;
    align-self: center;
  }
`;

export default forwardRef(SmRegisterListItem);
