import React from 'react';
import { BsFillPersonFill } from 'react-icons/bs';
import styled from '@emotion/styled';
import { Typography } from 'antd';
import RoleAvatar from '../avatar/RoleAvatar';

type TPMListItemProps = {
  actions?: React.ReactNode;
  style?: React.CSSProperties;
  index: number;
} & any;

const ListAgencyCheck = ({ actions, style, index, ...item }: TPMListItemProps) => {
  return (
    <PMListItemStyled style={style}>
      <div style={{ marginTop: '10px', marginRight: '20px' }} className=''>
        <div className='actions-wrapper'>{actions}</div>
      </div>

      <div className='left-part'>
        <div className='image-link'>
          <RoleAvatar isActive={true} size={40} offset={[0, 34]} icon={<BsFillPersonFill />} />
        </div>
        <div className='detail-wrapper'>
          <div className='name'>{item?.name}</div>
          <Typography.Text type='secondary' className='phone'>
            {item?.phone}
          </Typography.Text>
        </div>
      </div>
    </PMListItemStyled>
  );
};

const PMListItemStyled = styled.div`
  display: flex;
  padding: 12px 0;
  cursor: pointer;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);

  & > .left-part {
    display: flex;
    flex: 3;
    align-items: center;
    justify-content: flex-start;

    & > .image-link {
      display: flex;
      align-items: center;
    }

    & > .detail-wrapper {
      display: flex;
      flex-direction: column;
      margin-left: 16px;

      & > .name {
        font-weight: 500;
        font-size: 16px;
      }
    }
  }

  & > .middle-part {
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;

    & > .rank-tags-wrapper {
      display: flex;
      flex-direction: column;
      gap: 4px;
    }
  }

  & > .right-part {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: flex-end;

    & > .actions-wrapper {
      align-self: center;
    }
  }
`;

export default ListAgencyCheck;
