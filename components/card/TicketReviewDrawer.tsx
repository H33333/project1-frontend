import styled from '@emotion/styled';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import {
  Badge,
  Descriptions,
  Drawer,
  DrawerProps,
  Form,
  Input,
  InputNumber,
  Rate,
  Skeleton,
  Switch,
  theme,
  Typography,
} from 'antd';
import { useId } from 'react';
import { BsXLg } from 'react-icons/bs';
import { FaTelegramPlane } from 'react-icons/fa';
import useApp from 'src/hooks/useApp';
import useCarType from 'src/hooks/useCarType';
import useGetAddress from 'src/hooks/useGetAddress';
import {
  useGetTicketReviewDetailQuery,
  useUpdateTicketReviewMutation,
} from 'src/redux/query/ticket-review.query';
import { ErrorCode } from 'src/types/response.types';
import { dayjs } from 'src/utils/utils-date';
import Button from '../button/Button';
import AddressCombinedSelect from '../field/AddressCombinedSelect';
import CarTypeSelect from '../field/CarTypeSelect';
import DateTimePickupPopover from '../field/DateTimePickupPopover';
import { CoinIcon } from '../icons';
import Container from '../shared/Container';
import Tag from '../tag/Tag';

type TTicketReviewDrawerProps = {
  ticket_review_id: string;
  setOpen?: (open?: boolean) => void;
} & DrawerProps;

const TicketReviewDrawer = ({ ticket_review_id, setOpen, ...props }: TTicketReviewDrawerProps) => {
  const ruid = useId();
  const uid = `${ruid}:${ticket_review_id}`;
  const { notification } = useApp();
  const {
    token: { colorSuccess, colorTextPlaceholder, colorBorder, colorBgTextHover },
  } = theme.useToken();
  const [form] = Form.useForm();

  const { data: getTicketReviewDetailRes } = useGetTicketReviewDetailQuery(
    !!ticket_review_id ? { ticket_review_id } : skipToken,
    {
      refetchOnFocus: true,
      refetchOnMountOrArgChange: true,
    },
  );
  const detailTicketInfo = getTicketReviewDetailRes?.data.ticket_info;
  const ticketReviewInfo = getTicketReviewDetailRes?.data.ticket_review_info;
  const ticketRankInfo = getTicketReviewDetailRes?.data.ticket_rank_info;
  // const ticketWaitList = getTicketReviewDetailRes?.data.ticket_wait_list || [];
  const { carIconById } = useCarType();
  const { getProvinceDistrictTextById } = useGetAddress();
  const mappedAddress = getProvinceDistrictTextById(
    detailTicketInfo?.pickup_province,
    detailTicketInfo?.pickup_district,
  )
    .map((item) => item.name)
    .join(', ');

  const [updateTicketReviewMutate, { isLoading: updateTicketReviewLoading }] =
    useUpdateTicketReviewMutation();
  const handleUpdateTicketReview = (formData: any) => {
    if (!!formData) {
      updateTicketReviewMutate({
        car_type_id: formData.car_type_id,
        ticket_review_id: formData.ticket_review_id,
        pickup_province: formData.pickup_area?.[0],
        pickup_district: formData.pickup_area?.[1],
        dropoff_province: formData.dropoff_area?.[0],
        dropoff_district: formData.dropoff_area?.[1],
        pickup_time: formData.pickup_time,
        review_price: formData.review_price,
        ticket_price: formData.ticket_price,
        ...(!!formData.is_ranked_again ? { is_ranked: false } : {}),
      })
        .unwrap()
        .then(({ message }) => {
          notification.success({ message, placement: 'bottomRight' });
          setOpen?.(false);
        })
        .catch((err: any) => {
          if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
            notification.error({ message: err.error[0].message, placement: 'bottomRight' });
        });
    }
  };

  return (
    <Drawer
      width={740}
      onClose={() => setOpen?.(false)}
      title={null}
      headerStyle={{ display: 'none' }}
      bodyStyle={{ padding: 0 }}
      {...props}
    >
      <TicketReviewDrawerStyled>
        <div className='left-wrapper' style={{ borderRight: `1px solid ${colorBorder}` }}>
          <Container className='detail-wrapper'>
            {!!detailTicketInfo ? (
              <div className='detail'>
                <div className='title-wrapper'>
                  <Typography.Paragraph
                    type='secondary'
                    strong
                    style={{ fontSize: 20, margin: '2px 0 0' }}
                  >
                    Thông tin lịch
                  </Typography.Paragraph>
                  <div className='actions-wrapper'></div>
                </div>
                <div className='detail'>
                  <Typography.Paragraph
                    strong
                    type='secondary'
                    style={{ marginBottom: 0, fontSize: 14 }}
                  >
                    Chi tiết lịch:
                  </Typography.Paragraph>
                  <Typography.Paragraph
                    className='description'
                    style={{ marginBottom: 0, fontSize: 14 }}
                  >
                    {detailTicketInfo.description}
                  </Typography.Paragraph>

                  <Descriptions column={1} rootClassName='others-desc-wrapper' size='small'>
                    {!!mappedAddress && (
                      <Descriptions.Item label='Khu vực đón' className='mappedAddress'>
                        {mappedAddress}
                      </Descriptions.Item>
                    )}
                    {!!detailTicketInfo.pickup_time && (
                      <Descriptions.Item label='Thời gian đón'>
                        {dayjs(detailTicketInfo.pickup_time).format('DD/MM/YYYY HH:mm')}
                      </Descriptions.Item>
                    )}
                    <Descriptions.Item label='Đã tạo lúc'>
                      {dayjs(detailTicketInfo.createdAt).format('DD/MM/YYYY HH:mm')}
                    </Descriptions.Item>
                  </Descriptions>
                  <div className='others-wrapper'>
                    {carIconById(detailTicketInfo.car_type_id)}
                    <Tag icon={<CoinIcon width={18} height={18} />} bordered={false}>
                      {detailTicketInfo.coin || 'Free'}
                    </Tag>
                    {/* {detailTicketInfo?.status === 0 && !!ticketWaitList?.length && (
                      <Tag icon={<Badge status='processing' />} bordered={false}>
                        Có <b>{ticketWaitList.length}</b> xe bổ
                      </Tag>
                    )} */}
                    {detailTicketInfo.status === 1 && (
                      <Tag icon={<Badge status='processing' />} color='success'>
                        Đã giao lịch
                      </Tag>
                    )}
                    {detailTicketInfo.status === 2 && (
                      <Tag icon={<Badge status='success' />} color='success'>
                        Đã hoàn thành
                      </Tag>
                    )}
                    {detailTicketInfo.status === 3 && (
                      <Tag icon={<Badge status='error' />} color='error'>
                        Đã hủy
                      </Tag>
                    )}
                  </div>
                </div>
              </div>
            ) : (
              <div className='empty-wrapper'>
                <Skeleton paragraph={{ rows: 10 }} active avatar={false} title />
              </div>
            )}
          </Container>
          <div className='footer-wrapper'>
            {!!ticketReviewInfo && (
              <>
                <Tag
                  color={ticketReviewInfo.is_ranked ? 'success' : 'default'}
                  bordered={!!ticketReviewInfo.is_ranked}
                  style={{ fontSize: 14, height: 26 }}
                >
                  {ticketReviewInfo.is_ranked ? 'Đã xếp hạng' : 'Chưa xếp hạng'}
                </Tag>
                <span
                  style={{ margin: '0 10px 0 12px', fontSize: 24, color: colorTextPlaceholder }}
                >
                  •
                </span>
                <Rate
                  value={ticketRankInfo?.rank}
                  disabled
                  character={({ index }) => (index || 0) + 1}
                />
              </>
            )}
          </div>
        </div>
        <div className='right-wrapper'>
          <Container className='form-wrapper'>
            <div className='title-wrapper'>
              <Typography.Paragraph
                type='secondary'
                strong
                style={{ fontSize: 20, margin: '2px 0 0' }}
              >
                Đánh giá lịch
              </Typography.Paragraph>
              <div className='actions-wrapper'>
                <Button
                  icon={<BsXLg size={18} />}
                  type='text'
                  onClick={() => setOpen?.(false)}
                  size='middle'
                  style={{
                    backgroundColor: colorBgTextHover,
                    padding: '0 8px 0 6px',
                    fontSize: 16,
                  }}
                >
                  Đóng
                </Button>
              </div>
            </div>
            {!!ticketReviewInfo ? (
              <Form
                key={uid}
                id={uid + 'form'}
                form={form}
                layout='vertical'
                size='large'
                onFinish={handleUpdateTicketReview}
                initialValues={{
                  ticket_review_id: ticket_review_id,
                  car_type_id: ticketReviewInfo?.car_type_id,
                  ticket_price: ticketReviewInfo?.ticket_price,
                  review_price: ticketReviewInfo?.review_price,
                  pickup_area:
                    !!ticketReviewInfo?.pickup_province && !!ticketReviewInfo?.pickup_district
                      ? [ticketReviewInfo.pickup_province, ticketReviewInfo.pickup_district]
                      : !!detailTicketInfo?.pickup_province && !!detailTicketInfo?.pickup_district
                      ? [detailTicketInfo?.pickup_province, detailTicketInfo?.pickup_district]
                      : undefined,
                  dropoff_area:
                    !!ticketReviewInfo?.dropoff_province && !!ticketReviewInfo?.dropoff_district
                      ? [ticketReviewInfo.dropoff_province, ticketReviewInfo.dropoff_district]
                      : undefined,
                  pickup_time:
                    ticketReviewInfo?.pickup_time || detailTicketInfo?.pickup_time || undefined,
                  is_ranked_again: undefined,
                }}
              >
                <Form.Item name='ticket_review_id' hidden>
                  <Input />
                </Form.Item>
                <Form.Item name='ticket_price' label={'Giá từ lịch'}>
                  <InputNumber
                    prefix={'₫'}
                    step={1000}
                    placeholder={'Nhập giá'}
                    style={{ color: colorTextPlaceholder, width: '100%' }}
                    formatter={(value) => String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                  />
                </Form.Item>
                <Form.Item name='review_price' label={'Giá đánh giá'}>
                  <InputNumber
                    prefix={'₫'}
                    step={1000}
                    placeholder={'Nhập giá'}
                    style={{ color: colorSuccess, width: '100%' }}
                    formatter={(value) => String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={(value) => value!.replace(/\$\s?|(,*)/g, '')}
                  />
                </Form.Item>
                <Form.Item name='car_type_id' label='Loại xe (đánh giá)'>
                  <CarTypeSelect allowClear={false} placement='bottomRight' />
                </Form.Item>
                <Form.Item name='pickup_area' label='Chọn khu vực đón' style={{ marginBottom: 14 }}>
                  <AddressCombinedSelect placeholder='Chọn khu vực' allowClear={false} />
                </Form.Item>
                <Form.Item
                  name='dropoff_area'
                  label='Chọn khu vực trả khách'
                  style={{ marginBottom: 14 }}
                >
                  <AddressCombinedSelect placeholder='Chọn khu vực' allowClear={false} />
                </Form.Item>
                <Form.Item name='pickup_time' label='Thời gian đón'>
                  <DateTimePickupPopover
                    style={{ width: '100%' }}
                    placeholder={'Chọn thời gian đón'}
                    disabledBeforeNow={false}
                  />
                </Form.Item>
                {!!ticketReviewInfo?.is_ranked && (
                  <Form.Item
                    name='is_ranked_again'
                    label='Xếp hạng lại lịch'
                    valuePropName='checked'
                  >
                    <Switch />
                  </Form.Item>
                )}
              </Form>
            ) : (
              <div className='empty-wrapper'>
                <Skeleton paragraph={{ rows: 10 }} active avatar={false} title />
              </div>
            )}
          </Container>
          <div
            className='footer-wrapper'
            style={{ borderTop: `1px solid ${colorBorder}`, marginTop: 'auto' }}
          >
            <Button
              type='text'
              size='large'
              style={{ backgroundColor: colorBgTextHover, marginRight: 12 }}
              htmlType='button'
              onClick={() => {
                form.resetFields();
                setOpen?.(false);
              }}
            >
              Hủy
            </Button>
            <Button
              type='primary'
              size='large'
              block
              htmlType='submit'
              icon={<FaTelegramPlane size={22} />}
              form={uid + 'form'}
            >
              Gửi đánh giá
            </Button>
          </div>
        </div>
      </TicketReviewDrawerStyled>
    </Drawer>
  );
};
const TicketReviewDrawerStyled = styled.div`
  display: flex;
  height: 100%;
  overflow: hidden;
  & > .left-wrapper {
    width: 100%;
    max-width: 380px;
    display: flex;
    flex-direction: column;
    & > .detail-wrapper {
      & > .detail {
        padding: 0 12px;
        position: relative;
        display: flex;
        flex-direction: column;
        height: 100%;
        & > .title-wrapper {
          position: relative;
          height: 60px;
          display: flex;
          align-items: center;
          flex-shrink: 0;
          & > .actions-wrapper {
            position: absolute;
            right: 0;
            top: 50%;
            transform: translateY(-50%);
          }
        }
        & > .detail {
          display: flex;
          flex-direction: column;
          padding-left: 2px;
          & > .title-wrapper {
            display: flex;
            align-items: center;
            height: 24px;
            margin: 0 0 10px -2px;
          }
          & > .description {
            white-space: pre-line;
            margin-bottom: 4px;
          }
          & > .others-desc-wrapper {
            .ant-tag {
              gap: 6px;
              font-size: 14px;
              height: 26px;
            }
            .ant-descriptions-item {
              padding-bottom: 4px;
            }
            .ant-descriptions-item.mappedAddress {
              .ant-descriptions-item-container > span {
                white-space: nowrap;
              }
            }
          }
          & > .others-wrapper {
            display: flex;
            align-items: center;
            gap: 9.6px;
            margin-top: 4px;
            & > .ant-tag {
              gap: 6px;
              font-size: 14px;
              height: 26px;
            }
          }
        }
      }
    }
    & > .footer-wrapper {
      flex-shrink: 0;
      height: 64px;
      margin-top: auto;
      display: flex;
      align-items: center;
      padding: 0 12px;
    }
  }
  & > .right-wrapper {
    flex: 1 1 auto;
    min-width: 0px;
    position: relative;
    display: flex;
    flex-direction: column;
    & > .form-wrapper {
      & > .title-wrapper {
        padding: 0 12px;
        position: relative;
        height: 60px;
        display: flex;
        align-items: center;
        flex-shrink: 0;
        & > .actions-wrapper {
          position: absolute;
          right: 12px;
          top: 50%;
          transform: translateY(-50%);
        }
      }
      & > .ant-form {
        padding: 0 12px;
      }
    }
    & > .footer-wrapper {
      flex-shrink: 0;
      height: 64px;
      margin-top: auto;
      display: flex;
      align-items: center;
      padding: 0 12px;
    }
  }
`;

export default TicketReviewDrawer;
