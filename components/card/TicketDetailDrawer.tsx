import styled from '@emotion/styled';

type TTicketDetailDrawerProps = {};

const TicketDetailDrawer = ({}: TTicketDetailDrawerProps) => {
  return <TicketDetailDrawerStyled>TicketDetailDrawer</TicketDetailDrawerStyled>;
};
const TicketDetailDrawerStyled = styled.div``;

export default TicketDetailDrawer;
