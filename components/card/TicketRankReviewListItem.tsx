import styled from '@emotion/styled';
import { Badge, Descriptions, Rate, Space, Typography } from 'antd';
import { rgba } from 'emotion-rgba';
import { TTicketReviewItem } from 'src/types/ticket.types';
import { formatNumber } from 'src/utils/utils';
import { dayjs } from 'src/utils/utils-date';
import Avatar from '../avatar/Avatar';
import { CoinIcon } from '../icons';
import { TextDescriptionItem } from '../shared/ItemStyled';
import Tag from '../tag/Tag';

type TTicketRankReviewListItemProps = {
  actions?: React.ReactNode;
  carIcon?: React.ReactNode;
  mappedAddress?: React.ReactNode;
  mappedAddressTicket?: React.ReactNode;
  avatarIcon?: React.ReactNode;
  className?: string;
  isActive?: boolean;
} & TTicketReviewItem;

const TicketRankReviewListItem = ({
  actions,
  avatarIcon,
  carIcon,
  mappedAddress,
  mappedAddressTicket,
  className,
  isActive,
  ...item
}: TTicketRankReviewListItemProps) => {
  return (
    <TicketRankReviewListItemStyled className={`status${item.status || 0} ${className}`}>
      <div className='left-wrapper'>
        <div className='title-wrapper'>
          <div className='left-wrapper'>
            <Avatar size={26} style={{ fontSize: 14 }}>
              {avatarIcon}
            </Avatar>
          </div>
          <div className='actions-wrapper'>{actions}</div>
        </div>
        <Typography.Paragraph className='description' style={{ marginBottom: 0, fontSize: 14 }}>
          {item.ticket_info.description}
        </Typography.Paragraph>

        {!!mappedAddressTicket && (
          <TextDescriptionItem>
            <Typography.Text type='secondary' className='label'>
              Khu vực đón:
            </Typography.Text>
            <Typography.Text className='content'>{mappedAddressTicket}</Typography.Text>
          </TextDescriptionItem>
        )}
        {!!item.ticket_info.pickup_time && (
          <TextDescriptionItem>
            <Typography.Text type='secondary' className='label'>
              Thời gian đón:
            </Typography.Text>
            <Typography.Text className='content'>
              {dayjs(item.ticket_info.pickup_time).format('DD/MM/YYYY hh:mm A')}
            </Typography.Text>
          </TextDescriptionItem>
        )}
        <div className='others-wrapper'>
          <div className='wrapper'>
            {carIcon}
            <Tag icon={<CoinIcon width={16} height={16} />} bordered={false}>
              {item.ticket_info.coin || 'Free'}
            </Tag>
            {item.ticket_info.status === 1 && (
              <Tag icon={<Badge status='processing' />} color='success'>
                Đã giao lịch
              </Tag>
            )}
            {item.ticket_info.status === 2 && (
              <Tag icon={<Badge status='success' />} color='success'>
                Đã hoàn thành
              </Tag>
            )}
            {item.ticket_info.status === 3 && (
              <Tag icon={<Badge status='error' />} color='error'>
                Đã hủy
              </Tag>
            )}
          </div>
        </div>
      </div>
      <div className='right-wrapper'>
        <div className='wrapper'>
          <Descriptions
            column={2}
            items={[
              {
                label: 'Giá từ lịch',
                children: !!item?.ticket_price
                  ? `${formatNumber(item.ticket_price)}₫`
                  : 'Chưa đánh giá',
              },
              {
                label: 'Giá đánh giá',
                children: !!item?.review_price ? (
                  <Space size={4} align='start' style={{ marginTop: 0 }}>
                    <Typography.Text>{`${formatNumber(item.review_price)}₫`}</Typography.Text>
                    {item.review_price > item.ticket_price && (
                      <Tag
                        bordered={false}
                        color={
                          1 - item.ticket_price / item.review_price > 0.5
                            ? '#ff4d4f'
                            : 1 - item.ticket_price / item.review_price > 0.3
                            ? '#ffa940'
                            : 'default'
                        }
                        style={{ height: 18, padding: '0 3px', display: 'flex' }}
                      >
                        {'↗ '}
                        {((1 - item.ticket_price / item.review_price) * 100).toFixed(1)}%
                      </Tag>
                    )}
                  </Space>
                ) : (
                  'Chưa đánh giá'
                ),
              },
              {
                label: 'Khu vực đón',
                span: 2,
                children: mappedAddress
                  ? `${mappedAddress} (đã đánh giá)`
                  : !!mappedAddressTicket
                  ? `${mappedAddressTicket} (từ lịch)`
                  : 'Chưa đánh giá',
              },
              {
                label: 'Thời gian đón',
                span: 2,
                style: !!item.status ? {} : { padding: 0 },
                children: !!item.pickup_time
                  ? `${dayjs(item.pickup_time).format('DD/MM/YYYY HH:mm')} (đã đánh giá)`
                  : !!item.ticket_info.pickup_time
                  ? `${dayjs(item.ticket_info.pickup_time).format('DD/MM/YYYY HH:mm')} (từ lịch)`
                  : 'Chưa đánh giá',
              },
              {
                label: 'Cập nhật',
                span: 2,
                style: !!item.status ? {} : { display: 'none' },
                children: `${dayjs(item.updatedAt).fromNow()}`,
              },
            ]}
          />
          <div className='bottom-right-wrapper' style={{ marginLeft: 'auto' }}>
            <Typography.Text type={item.rank ? 'success' : 'secondary'} className='text'>
              {item.rank ? 'Đã xếp hạng' : 'Chưa xếp hạng'}
            </Typography.Text>
            <Rate value={item.rank} disabled character={({ index }) => (index || 0) + 1} />
          </div>
        </div>
      </div>
    </TicketRankReviewListItemStyled>
  );
};
const TicketRankReviewListItemStyled = styled.div`
  padding: 6px;
  margin-bottom: 12px;
  display: flex;
  border-radius: 6px;
  border: 1px solid var(--colorBorder);
  background-color: #fff;
  & > .left-wrapper {
    display: flex;
    flex-direction: column;
    flex: 0 0 340px;
    max-width: 340px;
    padding: 6px 0 6px 6px;
    & > .title-wrapper {
      margin-bottom: 0;
      position: relative;
      height: 34px;
      & > .left-wrapper {
        display: flex;
        align-items: center;
        .ant-avatar {
          opacity: 0.6;
          background-color: ${({ theme }) =>
            theme.mode === 'dark' ? rgba(theme.generatedColors[6], 0.5) : theme.generatedColors[1]};
          display: flex;
          justify-content: center;
          align-items: center;
        }
        & > .ant-tag {
          gap: 6px;
          margin-left: 6px;
        }
      }
      & > .actions-wrapper {
        position: absolute;
        top: 0;
        right: 0;
      }
    }
    & > .description {
      white-space: pre-line;
    }
    & > .others-wrapper {
      margin-top: auto;
      & > .wrapper {
        border-top: 1px solid var(--colorBorder);
        display: flex;
        align-items: center;
        gap: 10px;
        margin-top: 8px;
        padding-top: 10px;
        & > .ant-tag {
          gap: 6px;
          font-size: 14px;
          height: 28px;
        }
      }
    }
  }
  & > .right-wrapper {
    flex: 1 1 auto;
    min-width: 0;
    margin-left: 12px;
    & > .wrapper {
      width: 100%;
      height: 100%;
      padding: 12px 12px;
      background-color: rgba(0, 0, 0, 0.01);
      border: 1px solid #fff;
      border-radius: 3px;
      display: flex;
      flex-direction: column;
      position: relative;
      & > .bottom-right-wrapper {
        position: absolute;
        bottom: 0;
        right: 0;
        border: 1px solid rgba(0, 0, 0, 0.02);
        background-color: rgba(0, 0, 0, 0.02);
        padding: 4px 12px;
        & > .text {
          position: absolute;
          top: -2px;
          right: 0;
          transform: translate(-6px, -100%);
        }
      }
      & > .footer-wrapper {
        margin-top: auto;
        & > .wrapper {
          margin-top: 10px;
          padding-top: 10px;
          border-top: 1px solid var(--colorBorder);
          display: flex;
          align-items: center;
        }
        & .ant-tag {
          height: 26px;
          font-size: 14px;
        }
      }
    }
  }
`;

export default TicketRankReviewListItem;
