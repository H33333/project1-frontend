import { useTheme } from '@emotion/react';
import styled from '@emotion/styled';
import { AvatarProps, Typography } from 'antd';
import { useId } from 'react';
import { MdVisibility } from 'react-icons/md';
import { TAddress } from 'src/redux/query/address.query';
import Avatar from '../avatar/Avatar';
import { NameWithViewIcon } from '../shared/ItemStyled';

type TAddressListItemProps = {
  actions?: React.ReactNode;
  onClickName?: () => void;
  selected?: boolean;
  avatarProps?: AvatarProps;
  style?: React.CSSProperties;
  nameIcon?: React.ReactNode;
} & TAddress;

const AddressListItem = ({
  actions,
  selected = false,
  onClickName,
  avatarProps,
  style,
  nameIcon,
  ...item
}: TAddressListItemProps) => {
  const uid = useId();
  const { generatedColors } = useTheme();
  return (
    <AddressListItemStyled className={`${selected ? 'selected ' : ''}`} style={style}>
      <Avatar className='image-wrapper' size={32} {...avatarProps} />
      <div className='right-wrapper'>
        <NameWithViewIcon onClick={() => onClickName?.()}>
          <Typography.Text className='name' style={{ fontSize: 14 }}>
            {item.name}
          </Typography.Text>
          {nameIcon || <MdVisibility className='view-icon' size={16} />}
        </NameWithViewIcon>
      </div>
      <div className='actions-wrapper'>{actions}</div>
    </AddressListItemStyled>
  );
};
const AddressListItemStyled = styled.div`
  display: flex;
  padding: 12px 6px;
  border-bottom: 1px solid var(--colorBorder);
  position: relative;
  & > .image-wrapper {
    margin-right: 12px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .right-wrapper {
    display: flex;
    flex-direction: column;
    align-self: center;
  }
  & > .actions-wrapper {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 6px;
    display: flex;
    align-items: center;
  }
  &.selected {
    & > .image-wrapper {
      background-color: ${({ theme }) => theme.generatedColors[1]};
      color: ${({ theme }) => theme.colorPrimary};
    }
    & > .right-wrapper {
      .name {
        color: ${({ theme }) => theme.colorPrimary};
        text-decoration: underline;
      }
    }
  }
`;

export const AddressItemWrapper = styled.div`
  position: relative;
  padding: 6px;
  & > div:first-of-type {
    border-bottom: none;
    padding: 6px 0 0 6px;
  }
  & > .district-wrapper {
    position: relative;
    padding: 0 0 0 32px;
    &::before {
      content: '';
      position: absolute;
      top: 0;
      left: 20px;
      height: 100%;
      width: 2px;
      background-color: var(--colorBorder);
    }
    & > * {
      position: relative;
      border-bottom: none;
      &::before {
        content: '';
        position: absolute;
        top: 50%;
        transform: translate(-100%, -50%);
        left: 0;
        height: 2px;
        width: 12px;
        background-color: var(--colorBorder);
      }
    }
  }
  & > .form-wrapper {
    margin-top: 6px;
    padding: 12px 12px;
    background-color: var(--colorBorder);
    border-radius: 6px;
    position: relative;
    & > .close-btn {
      position: absolute;
      top: 6px;
      right: 6px;
    }
    & > form {
      .centerText-wrapper {
        position: relative;
        .centerText-help {
          position: absolute;
          top: 2px;
          right: 0;
          display: flex;
          justify-content: flex-end;
          align-items: center;
          & > * {
            display: flex;
            justify-content: center;
            align-items: center;
          }
        }
      }
      & > .actions-wrapper {
        display: flex;
        gap: 12px;
        justify-content: flex-end;
      }
    }
  }
`;

export default AddressListItem;
