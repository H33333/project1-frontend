import styled from '@emotion/styled';
import { Typography } from 'antd';
import { BsFillPersonFill } from 'react-icons/bs';
import RoleAvatar from '../avatar/RoleAvatar';

type TPMListItemProps = { actions?: React.ReactNode; style?: React.CSSProperties } & any;

const MoniterListItem = ({ actions, style, ...item }: TPMListItemProps) => {
  return (
    <PMListItemStyled style={style}>
      <div className='image-link'>
        <RoleAvatar
          size={40}
          offset={[0, 34]}
          //   role={item.role}
          //   isActive={item.status === 1}
          icon={<BsFillPersonFill />}
        ></RoleAvatar>
      </div>
      <div className='detail-wrapper'>
        <div className='title'>
        <strong>{item?.title}</strong>
        </div>
        <Typography.Text type='secondary' className='description'>
          {item?.description}
        </Typography.Text>
      </div>
      <div className='actions-wrapper'>{actions}</div>
    </PMListItemStyled>
  );
};
const PMListItemStyled = styled.div`
  display: flex;
  padding: 12px 0;
  cursor: pointer;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);
  & > .image-link {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  & > .detail-wrapper {
    display: flex;
    flex-direction: column;
    margin-left: 16px;
    & > .name {
      font-weight: 500;
      font-size: 16px;
    }
  }
  & > .actions-wrapper {
    margin-left: auto;
    align-self: center;
  }
`;

export default MoniterListItem;
