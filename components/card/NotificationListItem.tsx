import styled from '@emotion/styled';
import { Divider, Typography } from 'antd';
import RoleAvatar from '../avatar/RoleAvatar';
import Tag from '../tag/Tag';

type TPMListItemProps = {
  actions?: React.ReactNode;
  style?: React.CSSProperties;
  index: number;
} & any;

const NotificationListItem = ({ actions, style, index, ...item }: TPMListItemProps) => {
  return (
    <PMListItemStyled style={style}>
      <div className='left-part'>
        <div className='image-link'>
          <RoleAvatar isActive={true} size={40} offset={[0, 34]} icon={index + 1}></RoleAvatar>
        </div>
        <div className='detail-wrapper'>
          <div className='name'>{item?.title}</div>
          <Typography.Text type='secondary' className='phone'>
            {item?.body}
          </Typography.Text>
        </div>
      </div>
      <Divider type='vertical' style={{ margin: '0 12px' }} />
      <div className='middle-part'>
        <div className='rank-tags-wrapper'>
          <Tag bordered={true}>Thành công: {item?.success}</Tag>
          <Tag bordered={true}>Thất bại: {item?.fail}</Tag>
        </div>
      </div>
      <Divider type='vertical' style={{ margin: '0 12px' }} />
      <div className='right-part'>
        <div className='actions-wrapper'>{actions}</div>
      </div>
    </PMListItemStyled>
  );
};

const PMListItemStyled = styled.div`
  display: flex;
  padding: 12px 0;
  cursor: pointer;
  border-bottom: 1px solid rgba(0, 0, 0, 0.05);

  & > .left-part {
    display: flex;
    flex: 3;
    align-items: center;
    justify-content: flex-start;

    & > .image-link {
      display: flex;
      align-items: center;
    }

    & > .detail-wrapper {
      display: flex;
      flex-direction: column;
      margin-left: 16px;

      & > .name {
        font-weight: 500;
        font-size: 16px;
      }
    }
  }

  & > .middle-part {
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;

    & > .rank-tags-wrapper {
      display: flex;
      flex-direction: column;
      gap: 4px;
    }
  }

  & > .right-part {
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: flex-end;

    & > .actions-wrapper {
      align-self: center;
    }
  }
`;

export default NotificationListItem;
