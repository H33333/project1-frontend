import styled from '@emotion/styled';
import { useSafeState } from 'ahooks';
import { Divider, Empty, Tabs, Typography } from 'antd';
import { useId } from 'react';
import { BsFillPersonFill, BsXLg } from 'react-icons/bs';
import { TbChevronDown } from 'react-icons/tb';
import { TRankTicketReviewAgencyItem } from 'src/types/ticket.types';
import RoleAvatar from '../avatar/RoleAvatar';
import Button from '../button/Button';
import Tag from '../tag/Tag';
import TicketRankReviewListItem from './TicketRankReviewListItem';

type TTicketRankAgencyListItemProps = {
  actions?: React.ReactNode;
  avatarIcon?: React.ReactNode;
  getCarIcon: any;
  getAddressText: any;
} & TRankTicketReviewAgencyItem;

const TicketRankAgencyListItem = ({
  actions,
  getAddressText,
  getCarIcon,
  avatarIcon,
  ...item
}: TTicketRankAgencyListItemProps) => {
  const uid = useId();
  const [open, setOpen] = useSafeState(false);
  const [selectedTab, setSelectedTab] = useSafeState(`rank-1${uid}`);
  return (
    <TicketRankAgencyListItemStyled>
      <div className='info-wrapper'>
        <div className='left-wrapper'>
          <RoleAvatar
            isActive={item.agency_info.status === 1}
            icon={avatarIcon || <BsFillPersonFill />}
            size={44}
          ></RoleAvatar>
        </div>
        <div className='detail-wrapper'>
          <Typography.Text ellipsis className='user-name'>
            {item.agency_info.name}
          </Typography.Text>
          <Typography.Text ellipsis className='phone'>
            {item.agency_info.phone}
          </Typography.Text>
        </div>

        <Divider type='vertical' style={{ margin: '0 12px 0 16px' }} />

        <div className='rank-tags-wrapper'>
          <div className='' style={{ marginBottom: 4 }}>
            <Tag bordered={false}>Lịch tốt • {item.rank_5.length}</Tag>
            <Tag bordered={false}>Lịch chất lượng • {item.rank_4.length}</Tag>
          </div>
          <div className=''>
            <Tag bordered={false} color={item.rank_1.length ? 'red' : 'default'}>
              Lịch ảo • {item.rank_1.length}
            </Tag>
            <Tag bordered={false}>Lịch chất lượng kém • {item.rank_2.length}</Tag>
            <Tag bordered={false}>Lịch chất lượng trung • {item.rank_3.length}</Tag>
          </div>
        </div>

        <div className='actions-wrapper'>
          {actions}
          <Divider type='vertical' />
          <Button
            type={'dashed'}
            danger={open}
            size='large'
            shape='circle'
            icon={open ? <BsXLg size={20} /> : <TbChevronDown size={24} />}
            onClick={() => setOpen(!open)}
          ></Button>
        </div>
      </div>
      {!!open && (
        <div className='ticket-review-wrapper'>
          <div className='header-wrapper'>
            <Tabs
              activeKey={selectedTab}
              onTabClick={(activeKey) => setSelectedTab(activeKey)}
              tabBarStyle={{ margin: 0 }}
              style={{ marginBottom: 0 }}
              tabBarGutter={14}
              rootClassName='tabs-wrapper'
              items={[
                {
                  key: `rank-1${uid}`,
                  label: 'Lịch ảo',
                },
                {
                  key: `rank-2${uid}`,
                  label: 'Lịch chất lượng kém',
                },
                {
                  key: `rank-3${uid}`,
                  label: 'Lịch chất lượng trung',
                },
                {
                  key: `rank-4${uid}`,
                  label: 'Lịch chất lượng',
                },
                {
                  key: `rank-5${uid}`,
                  label: 'Lịch tốt',
                },
              ]}
            />
          </div>
          {selectedTab === `rank-1${uid}` &&
            (!!item.rank_1.length ? (
              <div className='rank-1-wrapper list'>
                {item.rank_1.map((ritem, index) => (
                  <TicketRankReviewListItem
                    key={uid + 'rank-1' + ritem._id}
                    carIcon={getCarIcon(ritem.ticket_info.car_type_id)}
                    mappedAddressTicket={getAddressText(
                      ritem.ticket_info.pickup_province,
                      ritem.ticket_info.pickup_district,
                    )
                      .map((item: any) => item.name)
                      .join(',')}
                    mappedAddress={getAddressText(ritem.pickup_province, ritem.pickup_district)
                      .map((item: any) => item.name)
                      .join(',')}
                    avatarIcon={index + 1}
                    {...ritem}
                  />
                ))}
              </div>
            ) : (
              <Empty description={false} style={{ margin: '48px 0' }} />
            ))}
          {selectedTab === `rank-2${uid}` &&
            (!!item.rank_2.length ? (
              <div className='rank-2-wrapper list'>
                {item.rank_2.map((ritem, index) => (
                  <TicketRankReviewListItem
                    key={uid + 'rank-2' + ritem._id}
                    carIcon={getCarIcon(ritem.ticket_info.car_type_id)}
                    mappedAddressTicket={getAddressText(
                      ritem.ticket_info.pickup_province,
                      ritem.ticket_info.pickup_district,
                    )
                      .map((item: any) => item.name)
                      .join(',')}
                    mappedAddress={getAddressText(ritem.pickup_province, ritem.pickup_district)
                      .map((item: any) => item.name)
                      .join(',')}
                    avatarIcon={index + 1}
                    {...ritem}
                  />
                ))}
              </div>
            ) : (
              <Empty description={false} style={{ margin: '48px 0' }} />
            ))}
          {selectedTab === `rank-3${uid}` &&
            (!!item.rank_3.length ? (
              <div className='rank-3-wrapper list'>
                {item.rank_3.map((ritem, index) => (
                  <TicketRankReviewListItem
                    key={uid + 'rank-3' + ritem._id}
                    carIcon={getCarIcon(ritem.ticket_info.car_type_id)}
                    mappedAddressTicket={getAddressText(
                      ritem.ticket_info.pickup_province,
                      ritem.ticket_info.pickup_district,
                    )
                      .map((item: any) => item.name)
                      .join(',')}
                    mappedAddress={getAddressText(ritem.pickup_province, ritem.pickup_district)
                      .map((item: any) => item.name)
                      .join(',')}
                    avatarIcon={index + 1}
                    {...ritem}
                  />
                ))}
              </div>
            ) : (
              <Empty description={false} style={{ margin: '48px 0' }} />
            ))}
          {selectedTab === `rank-4${uid}` &&
            (!!item.rank_4.length ? (
              <div className='rank-4-wrapper list'>
                {item.rank_4.map((ritem, index) => (
                  <TicketRankReviewListItem
                    key={uid + 'rank-4' + ritem._id}
                    carIcon={getCarIcon(ritem.ticket_info.car_type_id)}
                    mappedAddressTicket={getAddressText(
                      ritem.ticket_info.pickup_province,
                      ritem.ticket_info.pickup_district,
                    )
                      .map((item: any) => item.name)
                      .join(',')}
                    mappedAddress={getAddressText(ritem.pickup_province, ritem.pickup_district)
                      .map((item: any) => item.name)
                      .join(',')}
                    avatarIcon={index + 1}
                    {...ritem}
                  />
                ))}
              </div>
            ) : (
              <Empty description={false} style={{ margin: '48px 0' }} />
            ))}
          {selectedTab === `rank-5${uid}` &&
            (!!item.rank_5.length ? (
              <div className='rank-5-wrapper list'>
                {item.rank_5.map((ritem, index) => (
                  <TicketRankReviewListItem
                    key={uid + 'rank-5' + ritem._id}
                    carIcon={getCarIcon(ritem.ticket_info.car_type_id)}
                    mappedAddressTicket={getAddressText(
                      ritem.ticket_info.pickup_province,
                      ritem.ticket_info.pickup_district,
                    )
                      .map((item: any) => item.name)
                      .join(',')}
                    mappedAddress={getAddressText(ritem.pickup_province, ritem.pickup_district)
                      .map((item: any) => item.name)
                      .join(',')}
                    avatarIcon={index + 1}
                    {...ritem}
                  />
                ))}
              </div>
            ) : (
              <Empty description={false} style={{ margin: '48px 0' }} />
            ))}
        </div>
      )}
    </TicketRankAgencyListItemStyled>
  );
};
const TicketRankAgencyListItemStyled = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.05);
  border-radius: 8px;
  margin-bottom: 12px;
  display: flex;
  flex-direction: column;
  & > .info-wrapper {
    padding: 12px;
    display: flex;
    align-items: center;
    position: relative;
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
    & > .left-wrapper {
      display: flex;
      justify-content: center;
      align-items: center;
      .ant-avatar-circle {
        cursor: pointer;
        user-select: none;
        flex-shrink: 0;
        box-shadow: ${({ theme }) =>
          `0 0 0 2px ${theme.generatedColors[0]}, 0 0 0 3.6px ${theme.generatedColors[4]}`};
        background-color: ${({ theme }) => theme.generatedColors[1]};
      }
    }
    & > .detail-wrapper {
      display: flex;
      flex-direction: column;
      margin-left: 12px;
      overflow: hidden;
      width: 120px;
      max-width: 120px;
      .user-name {
        font-size: 18px;
        font-weight: 500;
        color: ${({ theme }) =>
          theme.mode === 'dark' ? theme.generatedColors[6] : theme.generatedColors[6]};
      }
      .phone {
        font-size: 14px;
        color: ${({ theme }) =>
          theme.mode === 'dark' ? theme.generatedColors[8] : theme.generatedColors[4]};
      }
    }
    & > .rank-tags-wrapper {
      display: flex;
      justify-content: center;
      flex-direction: column;
      .ant-tag {
        font-size: 13px;
        margin-right: 4px;
      }
    }
    & > .actions-wrapper {
      position: absolute;
      right: 12px;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      display: flex;
      align-items: center;
    }
  }
  & > .ticket-review-wrapper {
    background-color: rgba(0, 0, 0, 0.01);
    & > .header-wrapper {
      padding: 0 12px;
      position: relative;
      margin-bottom: 12px;
    }
    & > .list {
      padding: 0 12px 0;
      display: flex;
      flex-direction: column;
    }
  }
`;

export default TicketRankAgencyListItem;
