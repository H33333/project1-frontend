import styled from '@emotion/styled';
import { MacScrollbar } from 'mac-scrollbar';
import { forwardRef, ForwardRefRenderFunction } from 'react';

type TContainerProps = {
  children?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
};

const InternalContainer: ForwardRefRenderFunction<HTMLDivElement, TContainerProps> = (
  { children, className, style },
  forwardedRef,
) => {
  return (
    <ContainerStyled ref={forwardedRef} className={className} style={style}>
      {children}
    </ContainerStyled>
  );
};
const ContainerStyled = styled(MacScrollbar)``;

const Container = forwardRef(InternalContainer);
export default Container;
