import { css } from '@emotion/react';
import styled from '@emotion/styled';

export const cssSegmentedStatus = css`
  background-color: var(--bgColor);
  .ant-segmented-group label {
    color: #8e8e8e;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &.lock .ant-segmented-item-selected {
    color: red;
  }
  &.unlock .ant-segmented-item-selected {
    color: var(--bgColor);
  }
`;

export const NameWithViewIcon = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  width: fit-content;
  cursor: pointer;
  .name,
  .code {
    flex: 1 1 auto;
    min-width: 0px;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    word-break: break-word;
    font-size: 16px;
    font-weight: 600;
    line-height: 1.1;
  }
  .code {
    color: ${({ theme }) => theme.colorPrimary};
    margin: 0;
  }
  .view-icon {
    display: none;
    color: ${({ theme }) => theme.colorPrimary};
    position: absolute;
    top: 50%;
    right: 0;
    transform: translate(calc(100% + 8px), -50%);
  }
  &:hover {
    .ant-typography {
      text-decoration: underline;
    }
    .view-icon {
      display: block;
    }
  }
`;

export const cssCheckboxTypeGroup = css`
  display: flex;
  flex-wrap: wrap;
  gap: 12px;
  .checkbox-item {
    padding: 8px 24px 8px 8px;
    border: 1px solid var(--colorBorder);
    position: relative;
    align-items: center;
    border-radius: 8px;
    margin-left: 0;
    & span:last-of-type {
      display: flex;
      align-items: center;
      gap: 4px;
    }
    &.ant-checkbox-wrapper-checked {
      border: 1px solid var(--colorPrimary);
      color: var(--colorPrimary);
    }
    .ant-checkbox {
      position: absolute;
      top: 0;
      right: 0;
      transform: translate(-50%, 50%);
    }
  }
`;

export const cssTabDriverCar = css`
  display: flex;
  align-items: center;
  & > .text {
  }
  & > .tag-car {
    margin: 0 6px;
  }
`;

export const BreadcrumbNodeWrapper = styled.div`
  display: flex;
  align-items: center;
  margin: -8px 0 0 4px;
  & > .left-wrapper {
    flex: 0 0 auto;
    min-width: 0px;
    .basic-wrapper {
      display: flex;
      align-items: center;
      margin-right: 20px;
      .detail-wrapper {
        display: flex;
        flex-direction: column;
        margin: 0 0 0 12px;
        .name {
          margin: 0;
          font-size: 18px;
        }
        .phone {
          font-size: 12px;
          display: none;
        }
      }
    }
  }
  & > .right-wrapper {
    flex: 1 1 auto;
    min-width: 0px;
  }
`;

export const DriverFormListItem = styled.div`
  display: flex;
  border: 1px dashed var(--colorTextPlaceholder);
  border-radius: 8px;
  & > .left-wrapper {
    margin-right: 6px;
    display: flex;
    flex-direction: column;
    padding: 5px 0 5px 5px;
    z-index: 0;
    & > .ant-avatar {
      flex: 0 0 auto;
      min-height: 0px;
      z-index: 1;
      background-color: #bfbfbf;
      display: flex;
      justify-content: center;
      align-items: center;
      border: 2px solid var(--bgColor);
    }
  }
  & > .right-wrapper {
    display: flex;
    flex-direction: column;
    flex: 1 1 auto;
    min-width: 0px;
    position: relative;
    padding: 3px 32px 0 0;
    & .ant-form-item-control-input {
      min-height: 24px;
      & .ant-input-affix-wrapper {
        padding: 0;
      }
      & input {
        padding: 0;
      }
    }
    & > .actions-top {
      position: absolute;
      top: 6px;
      right: 6px;
      display: flex;
      align-items: center;
    }
    & > .actions-bottom {
      position: absolute;
      bottom: 6px;
      right: 6px;
      display: flex;
      align-items: center;
    }
  }
  &:focus-within {
    border-color: ${({ theme }) => theme.generatedColors[2]};
    & > .left-wrapper {
      & > .ant-avatar {
        background-color: ${({ theme }) => theme.generatedColors[2]};
      }
    }
    & > .right-wrapper {
      & .ant-form-item-control-input input:focus::placeholder {
        color: ${({ theme }) => theme.generatedColors[2]};
      }
    }
  }

  &:not(:last-of-type) {
    margin-bottom: 12px;
  }
`;

export const DotTimeline = styled.div`
  position: relative;
  width: 100%;
  flex: 1 1 auto;
  min-height: 0px;
  z-index: 0;
  &::before {
    content: '';
    width: 2px;
    height: calc(100%);
    background: #bfbfbf;
    position: absolute;
    left: 50%;
    bottom: 2px;
    transform: translate(-50%, -14px);
    z-index: 0;
  }
  &::after {
    content: '';
    position: absolute;
    background: #bfbfbf;
    position: absolute;
    bottom: 2px;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 8px;
    height: 8px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
  }
`;

export const cssListItemCreate = css`
  display: flex;
  position: relative;
  padding: 6px;
  border: 1px dashed var(--colorBorder);
  border-radius: 8px;
  margin-top: 12px;
  & > .ant-avatar {
    margin-right: 7px;
  }
  & > .right-wrapper {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    flex: 1 1 auto;
    min-width: 0px;
    padding-right: 28px;
    & > .ant-typography {
      line-height: 1.25;
    }
  }
  & > .actions-top {
    position: absolute;
    top: 6px;
    right: 6px;
  }
`;

export const TextDescriptionItem = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  & > .label {
    flex: 0 0 auto;
    min-width: 0px;
    margin-right: 4px;
  }
  & > .content {
    flex: 1 1 auto;
    min-width: 0px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
