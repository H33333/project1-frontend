import React, { useEffect } from 'react';
import type { FormProps } from 'antd';
import { Button, Form, Input, Modal } from 'antd';
import useChangeLocale from 'src/hooks/useChangeLocale';

type FieldType = {
    _id?: string;
    title?: string;
    time?: string;
    module?: number;
    device?: number;
    description?: string;
    agency_info?: any;
    agency_name:string;
    agency_phone:string;
};

interface IProps {
    isModalOpen: boolean;
    view: FieldType;
    handleCancel: any;
}

const ModalView = (props: IProps) => {
    const { i18n } = useChangeLocale();
    const { isModalOpen, view, handleCancel } = props;
    const [form] = Form.useForm();

    const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    useEffect(() => {
        form.setFieldValue('title',view.title);
        form.setFieldValue('time',view.time);
        form.setFieldValue('module',view.module);
        form.setFieldValue('device',view.device);
        form.setFieldValue('description',view.description);
        form.setFieldValue('agency_name',view.agency_info?.agency_name);
        form.setFieldValue('agency_phone',view.agency_info?.agency_phone);
    }, [view]);

    

    return (
        <Modal onCancel={handleCancel} title={i18n['Chi tiết']} open={isModalOpen} footer={false}>
            <Form
                form={form}
                name="basic"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}
                style={{ maxWidth: 600 }}
                onFinish={onFinish}
                initialValues={{ remember: true }}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item<FieldType> label={i18n['Tên']} name="agency_name">
                    <Input disabled />
                </Form.Item>
                <Form.Item<FieldType> label={i18n['Số điện thoại']} name="agency_phone">
                    <Input disabled />
                </Form.Item>
                <Form.Item<FieldType> label={i18n['Tiêu đề']} name="title">
                    <Input disabled />
                </Form.Item>

                <Form.Item<FieldType> label={i18n['Thời gian']} name="time">
                    <Input disabled />
                </Form.Item>

                <Form.Item<FieldType> label="Module" name="module">
                    <Input disabled />
                </Form.Item>

                <Form.Item<FieldType> label={i18n['Thiết bị']} name="device">
                    <Input disabled />
                </Form.Item>

                <Form.Item<FieldType> label={i18n['Mô tả']} name="description">
                    <Input.TextArea disabled />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 20, span: 4 }}>
                    <Button type="primary" onClick={handleCancel}>
                    {i18n['Đóng']}
                    </Button>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalView;
