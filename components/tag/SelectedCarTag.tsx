import styled from '@emotion/styled';
import { Typography } from 'antd';
import { TCar } from 'src/types/car.types';
import Tag from './Tag';

type TSelectedCarTagProps = {} & Partial<TCar>;

const SelectedCarTag = (item: TSelectedCarTagProps) => {
  if (!item?._id) return <Tag bordered={false}>Chưa chọn xe</Tag>;
  return (
    <SelectedCarTagStyled color='geekblue' bordered={false} style={{ minHeight: 26 }}>
      Đã chọn:
      <div style={{ marginLeft: 6 }}>
        <Typography.Text underline style={{ color: 'inherit' }}>
          {item.name}
        </Typography.Text>
      </div>
    </SelectedCarTagStyled>
  );
};
const SelectedCarTagStyled = styled(Tag)``;

export default SelectedCarTag;
