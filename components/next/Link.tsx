import { Typography } from 'antd';
import { LinkProps } from 'antd/es/typography/Link';
import { default as NextLink } from 'next/link';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import useChangeLocale from 'src/hooks/useChangeLocale';

export type TLinkProps = LinkProps & {};

const Link: ForwardRefRenderFunction<HTMLAnchorElement, TLinkProps> = (
  { children, onClick, href = '/', ...props },
  forwardedRef,
) => {
  const { locale } = useChangeLocale();
  return (
    <NextLink legacyBehavior href={href} passHref locale={locale}>
      <Typography.Link onClick={onClick} ref={forwardedRef} {...props}>
        {children}
      </Typography.Link>
    </NextLink>
  );
};
export default forwardRef(Link);
