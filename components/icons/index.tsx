import CoinAltIcon from './coin-alt.svg';
import CoinIcon from './coin.svg';
import LogoSvg from './logo-pipcar.svg';
import UserAdminIcon from './user-admin.svg';
import UserExpertIcon from './user-expert.svg';
import Xe16Icon from './xe16.svg';
import Xe29Icon from './xe29.svg';
import Xe35Icon from './xe35.svg';
import Xe45Icon from './xe45.svg';
import Xe5Icon from './xe5.svg';
import Xe7Icon from './xe7.svg';
import XeL9Icon from './xeL9.svg';
export {
  LogoSvg,
  UserAdminIcon,
  UserExpertIcon,
  Xe5Icon,
  Xe7Icon,
  Xe16Icon,
  Xe29Icon,
  Xe35Icon,
  Xe45Icon,
  XeL9Icon,
  CoinIcon,
  CoinAltIcon,
};
