import styled from '@emotion/styled';
import { Avatar as AntdAvatar, AvatarProps } from 'antd';
import { useState } from 'react';
type TAvatarProps = AvatarProps & { bg?: string };

const Avatar = ({ children, src, bg, style, ...props }: TAvatarProps) => {
  const [internalSrc, setInternalSrc] = useState(src);
  return (
    <AvatarStyled
      src={internalSrc}
      onError={() => {
        setInternalSrc('/image-placeholder.jpeg');
        return false;
      }}
      {...(bg ? { style: { backgroundColor: bg, ...style } } : { style })}
      {...props}
    >
      {children}
    </AvatarStyled>
  );
};
const AvatarStyled = styled(AntdAvatar)`
  svg {
    flex-shrink: 0;
  }
`;

export const AvatarGroup = AntdAvatar.Group;

export default Avatar;
