import styled from '@emotion/styled';
import { AvatarProps, Checkbox, CheckboxProps } from 'antd';
import { useState } from 'react';
import Avatar from './Avatar';
type TAvatarProps = AvatarProps &
  Pick<CheckboxProps, 'checked' | 'disabled' | 'onChange' | 'value'> & {
    bg?: string;
  };

const CheckboxAvatar = ({
  children,
  src,
  bg,
  style,
  icon,
  checked,
  disabled,
  onChange,
  value,
  ...props
}: TAvatarProps) => {
  const [internalSrc, setInternalSrc] = useState(src);
  return (
    <CheckboxAvatarStyled
      checked={checked}
      disabled={disabled}
      onChange={onChange}
      value={value}
      className='CheckboxAvatar'
    >
      <Avatar
        src={internalSrc}
        onError={() => {
          setInternalSrc('/image-placeholder.jpeg');
          return false;
        }}
        icon={icon}
        {...(bg ? { style: { backgroundColor: bg, ...style } } : { style })}
        {...props}
      >
        {children}
      </Avatar>
    </CheckboxAvatarStyled>
  );
};
const CheckboxAvatarStyled = styled(Checkbox)`
  position: relative;
  z-index: 0;
  .ant-checkbox {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 1;
    width: 100%;
    height: 100%;
    /* opacity: 0;
    visibility: hidden;
    transition: all 0.1s ease; */
    & > .ant-checkbox-inner {
      width: 100%;
      height: 100%;
      background-color: transparent !important;
      border-color: transparent;
      position: absolute;
      top: 0;
      left: 0;
      &:after {
        position: absolute;
        top: 50%;
        left: 35%;
        width: calc(5.7142857142857135px + 16px);
        height: calc(9.142857142857142px + 18px);
        border-width: 8px;
        border-color: ${({ theme }) => theme.colorPrimary};
      }
    }
    &:after {
      animation: none;
    }
    &.ant-checkbox-checked:not(.ant-checkbox-disabled):after {
      visibility: hidden;
    }
  }
  & > span:last-of-type {
    padding: 0;
  }
  svg {
    flex-shrink: 0;
  }
  &:hover {
    .ant-checkbox {
      opacity: 1;
      visibility: visible;
    }
  }
`;

export default CheckboxAvatar;
