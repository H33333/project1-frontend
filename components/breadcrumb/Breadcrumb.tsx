import { Breadcrumb as AntdBreadcrumb, BreadcrumbProps } from 'antd';
import { useRouter } from 'next/router';
import Link from 'src/components/next/Link';
import useChangeLocale from 'src/hooks/useChangeLocale';

type TBreadcrumbProps = Omit<BreadcrumbProps, 'items'> & {
  items?: Record<string, React.ReactNode | { node: React.ReactNode }>;
  current?: React.ReactNode | { node: React.ReactNode };
  hideHomePage?: boolean;
  notUseLink?: boolean;
};

const Breadcrumb = ({
  items = {},
  current,
  hideHomePage = false,
  notUseLink = false,
  ...props
}: TBreadcrumbProps) => {
  const { asPath } = useRouter();
  const { i18n } = useChangeLocale();

  return (
    <AntdBreadcrumb
      items={Object.entries<React.ReactNode | { node: React.ReactNode }>({
        ...(hideHomePage ? {} : { ['/']: i18n['Tổng quan'] }),
        ...items,
        ...(!!current ? { [asPath]: current } : {}),
      }).map((item) => {
        const [path, label] = item;
        if (!!label && typeof label === 'object' && 'node' in label)
          return {
            title: label?.node,
            key: path,
          };
        return {
          title: (
            <Link href={path} type='secondary'>
              {label}
            </Link>
          ),
          key: path,
        };
      })}
      {...props}
    />
  );
};

export default Breadcrumb;
