import styled from '@emotion/styled';
import { Button as AntdButton, ButtonProps, Tooltip, TooltipProps } from 'antd';
import Link from 'next/link';
import { forwardRef, ForwardRefRenderFunction } from 'react';

export type TButtonProps = ButtonProps & {
  tooltip?: React.ReactNode;
  bgColor?: string;
  color?: string;
  tooltipProps?: TooltipProps;
};

const ButtonStyled = styled(AntdButton)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  flex-wrap: nowrap;
  &:not(.ant-btn-icon-only) > .ant-btn-icon:not(:last-child) {
    margin-inline-end: 4px;
  }
`;
const Button: ForwardRefRenderFunction<HTMLElement, TButtonProps> = (
  { children, tooltip, bgColor, color, tooltipProps, ...props },
  forwardedRef,
) => {
  const button = !tooltip ? (
    <ButtonStyled
      htmlType='button'
      {...props}
      style={{ backgroundColor: bgColor, color, ...props.style }}
      ref={forwardedRef}
    >
      {children}
    </ButtonStyled>
  ) : (
    <Tooltip destroyTooltipOnHide title={tooltip} {...tooltipProps}>
      <ButtonStyled
        htmlType='button'
        {...props}
        style={{ backgroundColor: bgColor, color, ...props.style }}
        ref={forwardedRef}
      >
        {children}
      </ButtonStyled>
    </Tooltip>
  );

  if (!!props?.href) {
    return (
      <Link href={props.href} passHref legacyBehavior>
        {button}
      </Link>
    );
  }

  return button;
};

export const ButtonGroup = AntdButton.Group;

export default forwardRef(Button);
