import styled from '@emotion/styled';
import { Segmented } from 'antd';
import { MdOutlineLightMode, MdOutlineWbSunny } from 'react-icons/md';
import { RiMoonClearFill, RiMoonClearLine } from 'react-icons/ri';
import { setThemeMode, toggleThemeMode } from 'src/redux/reducer/theme.reducer';
import { useAppDispatch, useAppSelector } from 'src/redux/store';
import Button from './Button';

function SideTheme({ collapsed = false }) {
  const dispatch = useAppDispatch();
  const mode = useAppSelector((s) => s.theme.mode);
  return (
    <SideThemeStyled>
      {collapsed ? (
        <Button
          block
          size='large'
          type='text'
          className='btn-toggle'
          onClick={() => dispatch(toggleThemeMode(null))}
        >
          {mode === 'light' ? <MdOutlineLightMode size={22} /> : <RiMoonClearLine size={22} />}
        </Button>
      ) : (
        <Segmented
          options={[
            {
              value: 'light',
              icon: <MdOutlineWbSunny />,
              label: 'Light',
            },
            {
              value: 'dark',
              icon: <RiMoonClearFill />,
              label: 'Dark',
            },
          ]}
          value={mode}
          onChange={(v) => dispatch(setThemeMode(v as any))}
        />
      )}
    </SideThemeStyled>
  );
}

const SideThemeStyled = styled.div`
  .ant-segmented {
    width: 100%;
  }
  .ant-segmented-item {
    flex: 1;
  }
  .ant-segmented-item-label {
    display: flex;
    justify-content: center;
    align-items: center;
    .ant-segmented-item-icon {
      display: inline-flex;
      justify-content: center;
      align-items: center;
      flex-shrink: 0;
    }
  }
`;

export default SideTheme;
