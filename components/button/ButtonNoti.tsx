import styled from '@emotion/styled';
import { Badge } from 'antd';
import { HiBell } from 'react-icons/hi2';
import { setVisibleItem } from 'src/redux/reducer/visible.reducer';
import { useAppDispatch } from 'src/redux/store';
import Button from './Button';

type TButtonNotiProps = {};

const ButtonNoti = ({}: TButtonNotiProps) => {
  const dispatch = useAppDispatch();
  return (
    <Badge count={1} offset={[-6, 6]}>
      <ButtonNotiStyled
        type='text'
        onClick={() => dispatch(setVisibleItem('sideadminnotivisible'))}
        icon={<HiBell size={24} color='#595959' />}
      ></ButtonNotiStyled>
    </Badge>
  );
};
const ButtonNotiStyled = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
  & svg {
    z-index: 1;
  }
`;

export default ButtonNoti;
