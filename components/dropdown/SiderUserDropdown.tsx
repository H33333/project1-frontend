import styled from '@emotion/styled';
import { Dropdown, theme, Typography } from 'antd';
import { cloneElement, Fragment } from 'react';
import { BsFillPersonFill, BsGear } from 'react-icons/bs';
import { FiLogOut } from 'react-icons/fi';
import { HiOutlineSelector } from 'react-icons/hi';
import useChangeLocale from 'src/hooks/useChangeLocale';
import useLogout from 'src/hooks/useLogout';
import { useAppDispatch, useAppSelector } from 'src/redux/store';
import RoleAvatar from '../avatar/RoleAvatar';
import SiderUserProfile from '../card/SiderUserProfile';
import Link from '../next/Link';

function SiderUserDropdown({
  collapsed,
  style,
}: {
  collapsed?: boolean;
  style?: React.CSSProperties;
}) {
  const { i18n } = useChangeLocale();
  const { mode } = useAppSelector((s) => s.theme);
  const userState = useAppSelector((s) => s.user.data);
  const {
    token: { colorBgElevated, borderRadiusLG, boxShadowSecondary },
  } = theme.useToken();
  const dispatch = useAppDispatch();

  const { handleLogout, isLoadingLogout } = useLogout();

  if (!userState) return <></>;

  return (
    <Dropdown
      menu={{
        items: [
          collapsed
            ? {
                type: 'divider',
              }
            : undefined,
          {
            key: 'Setting',
            label: (
              <DropdownItemLink href='/admin/setting'>
                <span>{i18n['Cài đặt tài khoản']}</span>
                <BsGear size={16} />
              </DropdownItemLink>
            ),
          },
          {
            type: 'divider',
          },
          {
            key: 'Logout',
            label: (
              <DropdownItemBtn onClick={() => handleLogout()}>
                <span style={{ fontSize: 14 }}>{i18n['Đăng xuất']}</span>
                <FiLogOut size={14} />
              </DropdownItemBtn>
            ),
            disabled: isLoadingLogout,
          },
        ].filter((i) => !!i) as any,
      }}
      placement='topRight'
      dropdownRender={(originNode) => (
        <DropdownStyled
          style={{
            backgroundColor: colorBgElevated,
            borderRadius: borderRadiusLG,
            boxShadow: boxShadowSecondary,
          }}
        >
          {collapsed && <SiderUserProfile style={{ padding: '6px 8px 4px 10px' }} />}
          {cloneElement(originNode as any, { style: { boxShadow: 'none', padding: 0 } })}
        </DropdownStyled>
      )}
    >
      <UserWrapperStyled
        style={{ padding: !!collapsed ? '12px 27px 12px 17px' : '12px 27px 12px 16px', ...style }}
      >
        <RoleAvatar
          role={userState.role}
          isActive={userState.status === 1}
          icon={<BsFillPersonFill />}
        ></RoleAvatar>
        {!collapsed && (
          <Fragment>
            <div className='user-container'>
              <Typography.Text style={{ width: 140 }} ellipsis className='user-name'>
                {userState.name}
              </Typography.Text>
              <Typography.Text ellipsis className='phone'>
                {userState.phone}
              </Typography.Text>
            </div>
            <div className='bar-icon'>
              <HiOutlineSelector size={22} />
            </div>
          </Fragment>
        )}
      </UserWrapperStyled>
    </Dropdown>
  );
}

const DropdownStyled = styled.div`
  padding: 4px;
  .ant-dropdown-menu .ant-dropdown-menu-item {
    padding: 0;
  }
`;
const UserWrapperStyled = styled.div`
  position: relative;
  z-index: 0;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: nowrap;
  border-radius: 8px;
  background-color: ${({ theme }) => theme.generatedColors[0]};
  cursor: pointer;
  flex-shrink: 0;
  .ant-avatar-circle {
    cursor: pointer;
    user-select: none;
    flex-shrink: 0;
    box-shadow: ${({ theme }) =>
      `0 0 0 2px ${theme.generatedColors[0]}, 0 0 0 3.6px ${theme.generatedColors[4]}`};
    background-color: ${({ theme }) => theme.generatedColors[1]};
  }
  .user-container {
    flex: 1 1 auto;
    min-width: 0px;
    display: flex;
    flex-direction: column;
    margin-left: 12px;
    overflow: hidden;
    .user-name {
      font-size: 15px;
      font-weight: 500;
      color: ${({ theme }) =>
        theme.mode === 'dark' ? theme.generatedColors[6] : theme.generatedColors[6]};
    }
    .phone {
      font-size: 14px;
      color: ${({ theme }) =>
        theme.mode === 'dark' ? theme.generatedColors[8] : theme.generatedColors[4]};
    }
  }
  .bar-icon {
    position: absolute;
    right: 4px;
    top: 50%;
    transform: translateY(-50%);
    color: ${({ theme }) => theme.generatedColors[4]};
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &.close {
    padding: 8px 0;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: transparent;
    border-color: transparent;
  }
`;

export const DropdownItemBtn = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 16px;
  flex-wrap: nowrap;
  padding: 8px 10px;
`;

export const DropdownItemLink = styled(Link)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 16px;
  flex-wrap: nowrap;
  padding: 8px 10px;
`;

export default SiderUserDropdown;
