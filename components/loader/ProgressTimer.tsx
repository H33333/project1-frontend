import styled from '@emotion/styled';
import { useCountDown } from 'ahooks';
import { Progress } from 'antd';
import { useId } from 'react';

type TProgressTimerProps = { initialSeconds?: number; onEnd?: () => void };

const ProgressTimer = ({ initialSeconds = 10, onEnd }: TProgressTimerProps) => {
  const uid = useId();
  const [countdown] = useCountDown({
    leftTime: initialSeconds * 1000,
    interval: 1000,
    onEnd,
  });
  return (
    <Progress
      status='active'
      strokeColor={{
        from: '#f5222d',
        to: '#ff7a45',
      }}
      percent={(Math.round(countdown / 1000) / initialSeconds) * 100}
      size={36}
      type='circle'
      showInfo
      format={(percent) => `${Math.round(countdown / 1000)}s`}
    />
  );
};
const ProgressTimerStyled = styled.div``;

export default ProgressTimer;
