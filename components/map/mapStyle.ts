export const lineStyle = (color = '#69b1ff', width = 1): any => {
  return {
    type: 'line',
    paint: {
      'line-width': width,
      'line-color': color,
    },
  };
};

export const textStyle: any = {
  type: 'symbol',
  paint: {
    'text-color': '#69b1ff',
  },
  layout: {
    'text-field': ['get', 'text'],
    'text-allow-overlap': true,
    'text-anchor': 'top',
    'text-size': 12,
  },
};
