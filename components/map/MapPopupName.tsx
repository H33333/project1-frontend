import styled from '@emotion/styled';
import { skipToken } from '@reduxjs/toolkit/dist/query';
import { Popup, PopupProps } from 'react-map-gl/maplibre';
import { useGetReverseGeocodingQuery } from 'src/redux/query/nominatim.query';

type TMapPopupNameProps = {
  number?: number;
  nameProps?: {
    hidden?: boolean;
    style?: React.CSSProperties;
    text?: string;
  };
} & PopupProps;

const MapPopupName = ({
  longitude,
  latitude,
  number,
  nameProps,
  children,
  ...props
}: TMapPopupNameProps) => {
  const { data } = useGetReverseGeocodingQuery(
    !!nameProps?.hidden ? skipToken : { lat: latitude, lng: longitude },
    { refetchOnMountOrArgChange: true },
  );
  return (
    <MapPopupNameStyled
      longitude={longitude}
      latitude={latitude}
      closeButton={false}
      onClose={() => {}}
      closeOnClick={false}
      {...props}
    >
      {!!number && <div className='number'>{number}</div>}
      {!nameProps?.hidden && (
        <div className='address-name' style={nameProps?.style}>
          {!!nameProps?.text ? nameProps.text : data?.display_name}
        </div>
      )}
    </MapPopupNameStyled>
  );
};
const MapPopupNameStyled = styled(Popup)`
  --border-color: ${({ theme }) =>
    theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.25)' : 'rgba(0, 0, 0, 0.25)'};
  --text-color: ${({ theme }) =>
    theme.mode === 'dark' ? 'rgba(255, 255, 255, 0.45)' : 'rgba(0, 0, 0, 0.45)'};
  .maplibregl-popup-content {
    position: relative;
    width: 36px;
    height: 36px;
    padding: 0;
    background-color: transparent;
    box-shadow: none;
    .number {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      border-radius: 1000px;
      min-width: 48px;
      height: 48px;
      background-color: var(--bg-color);
      border: 2px solid var(--border-color);
      color: var(--text-color);
      font-size: 32px;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .address-name {
      position: absolute;
      bottom: 0;
      right: 0;
      transform: translate(100%, 100%);
      width: 220px;
      line-height: 1.3;
      font-size: 12px;
      margin: 0;
      text-shadow: 1px 1px 2px var(--colorBorder);
    }
  }
  &.hide-arrow {
    .maplibregl-popup-tip {
      display: none;
    }
  }
  &.selected {
    & .maplibregl-popup-content .number {
      background-color: ${({ theme }) =>
        theme.mode === 'dark' ? theme.generatedColors[1] : theme.generatedColors[0]};
      border-color: ${({ theme }) => theme.colorPrimary};
      color: ${({ theme }) => theme.colorPrimary};
    }
  }
`;

export default MapPopupName;
