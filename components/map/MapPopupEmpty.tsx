import styled from '@emotion/styled';
import { theme } from 'antd';
import { TbMapPinOff } from 'react-icons/tb';
import { Popup, PopupProps } from 'react-map-gl/maplibre';
import { DEFAULT_POSITION } from 'src/utils/constant';

type TMapPopupEmptyProps = { desc?: React.ReactNode } & Partial<PopupProps>;

const MapPopupEmpty = ({
  longitude = DEFAULT_POSITION.lng,
  latitude = DEFAULT_POSITION.lat,
  desc,
}: TMapPopupEmptyProps) => {
  const {
    token: { colorTextSecondary },
  } = theme.useToken();
  return (
    <MapPopupEmptyStyled
      longitude={longitude}
      latitude={latitude}
      anchor='bottom'
      closeButton={false}
      onClose={() => {}}
      closeOnClick={false}
    >
      <div className='icon'>
        <TbMapPinOff size={28} color={colorTextSecondary} />
      </div>
      {desc}
    </MapPopupEmptyStyled>
  );
};
const MapPopupEmptyStyled = styled(Popup)`
  & > .maplibregl-popup-content {
    padding: 8px;
    width: 200px;
    display: flex;
    align-items: flex-start;
    & > div {
      display: flex;
    }
    .icon {
      align-self: center;
      margin-right: 4px;
    }
  }
`;

export default MapPopupEmpty;
