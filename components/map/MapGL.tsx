import styled from '@emotion/styled';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import type { MapProps, MapRef } from 'react-map-gl/maplibre';
import Map, { Layer, Source } from 'react-map-gl/maplibre';
import { VMN } from 'src/utils/constant';
import { textStyle } from './mapStyle';
import MAP_STYLE from './mapstylebasic.json';

type TMapGLProps = {
  children: React.ReactNode;
  maxZoom?: number;
  minZoom?: number;
  maxPitch?: number;
  minPitch?: number;
  touchPitch?: boolean;
  touchZoomRotate?: boolean;
  scrollZoom?: boolean;
} & Omit<
  MapProps,
  | 'children'
  | 'maxZoom'
  | 'minZoom'
  | 'minPitch'
  | 'maxPitch'
  | 'touchPitch'
  | 'touchZoomRotate'
  | 'scrollZoom'
>;

export type TMapRef = MapRef;
const InternalMapGL: ForwardRefRenderFunction<MapRef, TMapGLProps> = (
  { children, ...props },
  forwardedRef,
) => {
  return (
    <MapGLStyled
      ref={forwardedRef}
      mapStyle={MAP_STYLE as any}
      terrain={undefined}
      dragRotate={false}
      dragPan={true}
      touchPitch={true}
      touchZoomRotate={false}
      {...props}
    >
      {children}
      <Source type='geojson' data={VMN.HoangSa as any}>
        <Layer id='HoangSa' {...textStyle} />
      </Source>
      <Source type='geojson' data={VMN.TruongSa as any}>
        <Layer id='TruongSa' {...textStyle} />
      </Source>
    </MapGLStyled>
  );
};
const MapGLStyled = styled(Map)`
  z-index: 0;
`;
const MapGL = forwardRef<MapRef, TMapGLProps>(InternalMapGL);
export default MapGL;
