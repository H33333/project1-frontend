import { css } from '@emotion/react';
import styled from '@emotion/styled';
import { Layout } from 'antd';

export const cssSider = css`
  position: relative;
  box-shadow: var(--boxShadow);
  max-height: 100vh;
  .logo-wrapper {
    height: 64px;
    padding: 0 27px;
    width: 100%;
    display: flex;
    align-items: center;
    flex-shrink: 0;
  }
  .ant-layout-sider-children {
    display: flex;
    flex-direction: column;
  }
  .collapse-button {
    position: absolute;
    top: 32px;
    right: 0;
    transform: translate(50%, -50%);
    z-index: 10;
  }
  &.ant-layout-sider-collapsed {
    .logo-wrapper {
      padding: 0 25px;
    }
  }
`;

const StyledLayout = styled(Layout)`
  --bgColor: ${({ theme }) => (theme.mode === 'dark' ? '#141414' : '#fff')};
  --colorTextSecondary: ${({ theme }) =>
    theme.mode === 'light' ? 'rgba(0,0,0,0.45)' : 'rgba(255,255,255,0.45)'};
  --colorTextPlaceholder: ${({ theme }) =>
    theme.mode === 'light' ? 'rgba(0,0,0,0.25)' : 'rgba(255,255,255,0.25)'};
  --colorBorder: ${({ theme }) =>
    theme.mode === 'light' ? 'rgba(0,0,0,0.05)' : 'rgba(255,255,255,0.05)'};
  --bgSegmented: ${({ theme }) => (theme.mode === 'dark' ? '#141414' : '#f0f0f0')};
  --boxShadow: ${({ theme }) =>
    theme.mode === 'light'
      ? `0 1px 2px 0 rgba(0, 0, 0, 0.03), 0 1px 6px -1px rgba(0, 0, 0, 0.02),
    0 2px 4px 0 rgba(0, 0, 0, 0.02)`
      : `0 1px 2px 0 rgba(255, 255, 255, 0.03), 0 1px 6px -1px rgba(255, 255, 255, 0.02),
      0 2px 4px 0 rgba(255, 255, 255, 0.02)`};
  --colorPrimary: ${({ theme }) => theme.colorPrimary};
  height: 100vh;
  overflow: hidden;

  .ant-statistic-content {
    display: flex;
    align-items: center;
    .ant-statistic-content-prefix {
      height: fit-content;
      display: flex;
    }
  }

  .ant-segmented-status {
    background-color: ${({ theme }) => (theme.mode === 'dark' ? '#141414' : '#f0f0f0')};
    .ant-segmented-group label {
      color: #8e8e8e;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    &.lock .ant-segmented-item-selected {
      color: red;
    }
    &.unlock .ant-segmented-item-selected {
      color: ${({ theme }) => theme.generatedColors[6]};
    }
  }
`;

export default StyledLayout;
