import styled from '@emotion/styled';
import { Divider } from 'antd';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import { HiOutlineBars3BottomLeft } from 'react-icons/hi2';
import { useMediaQuery } from 'react-responsive';
import { setVisibleItem } from 'src/redux/reducer/visible.reducer';
import { useAppDispatch } from 'src/redux/store';
import Button from '../button/Button';
import ButtonNoti from '../button/ButtonNoti';
import PMSiderDrawer from '../drawer/PMSiderDrawer';
import LocaleSelect from '../dropdown/LocaleSelect';
import StyledHeader from '../header/StyledHeader';
import Container from '../shared/Container';
import LogoWithText from '../shared/LogoWithText';
type TPageLayoutProps = {
  children?: React.ReactNode;
  breadcrumb?: React.ReactNode;
  className?: string;
};

const PageLayout: ForwardRefRenderFunction<HTMLDivElement, TPageLayoutProps> = (
  { children, breadcrumb, className },
  forwardedRef,
) => {
  const mediaAbove1023 = useMediaQuery({ minWidth: 1023 });
  const dispatch = useAppDispatch();
  return (
    <PageLayoutStyled ref={forwardedRef}>
      <StyledHeader>
        <div className='header-left'>
          {mediaAbove1023 ? (
            breadcrumb
          ) : (
            <>
              <LogoWithText fontSize={28} logoSize={32} />
              <Button
                className='sider-drawer-button'
                type='text'
                shape='circle'
                onClick={() => dispatch(setVisibleItem('side-PM-drawer-visible'))}
              >
                <HiOutlineBars3BottomLeft size={28} />
              </Button>
            </>
          )}
        </div>
        <div className='header-right'>
          <LocaleSelect />
          <Divider type='vertical' />
          <ButtonNoti />
        </div>
      </StyledHeader>
      <main className={className}>{children}</main>
      <PMSiderDrawer />
    </PageLayoutStyled>
  );
};
const PageLayoutStyled = styled(Container)`
  flex: 1 1 auto;
  min-width: 0px;
  main {
    overflow: hidden;
    width: 100%;
    max-width: 1000px;
    margin: 0 auto;
    position: relative;
  }
`;

export default forwardRef(PageLayout);
