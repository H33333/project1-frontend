import styled from '@emotion/styled';
import { Divider, Layout as AntdLayout } from 'antd';
import { ReactNode } from 'react';
import { AiOutlineMenuFold, AiOutlineMenuUnfold } from 'react-icons/ai';
import { useMediaQuery } from 'react-responsive';
import { setSiderCollapsed } from 'src/redux/reducer/visible.reducer';
import { useAppDispatch, useAppSelector } from 'src/redux/store';
import Button from '../button/Button';
import SideTheme from '../button/SideTheme';
import AdminSiderDrawer from '../drawer/AdminSiderDrawer';
import SiderUserDropdown from '../dropdown/SiderUserDropdown';
import AdminMenuNav from '../menu/AdminMenuNav';
import LogoWithText from '../shared/LogoWithText';
import StyledLayout, { cssSider } from './StyledLayout';

const AdminLayout = ({ children }: { children: ReactNode }) => {
  const mediaAbove767 = useMediaQuery({ minWidth: 767 });
  const isSiderCollapsed = useAppSelector((s) => s.visible.isSiderCollapsed);
  const dispatch = useAppDispatch();

  return (
    <LayoutWrapper hasSider>
      {mediaAbove767 && (
        <AntdLayout.Sider
          css={cssSider}
          width={220}
          theme={'light'}
          breakpoint='lg'
          style={{ height: '100vh' }}
          trigger={null}
          collapsible
          collapsed={isSiderCollapsed}
          collapsedWidth={84}
        >
          <LogoWithText fontSize={!isSiderCollapsed ? 28 : 0} logoSize={32} />
          <AdminMenuNav />
          <footer style={{ marginTop: 'auto', padding: 8 }}>
            <SiderUserDropdown collapsed={isSiderCollapsed} />
            <Divider style={{ margin: '8px 0' }} />
            <SideTheme collapsed={isSiderCollapsed} />
          </footer>
          <Button
            className='collapse-button'
            shape='circle'
            icon={
              isSiderCollapsed ? <AiOutlineMenuUnfold size={18} /> : <AiOutlineMenuFold size={18} />
            }
            onClick={() => dispatch(setSiderCollapsed(!isSiderCollapsed))}
          ></Button>
        </AntdLayout.Sider>
      )}
      {children}
      <AdminSiderDrawer />
    </LayoutWrapper>
  );
};

const LayoutWrapper = styled(StyledLayout)``;

export default AdminLayout;
