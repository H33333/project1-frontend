export const DEFAULT_POSITION = {
  lat: 21.0046118,
  lng: 105.8463549,
};
export const VMN = {
  HoangSa: 'https://api.pipcar.vn/s/HoangSa.json',
  TruongSa: 'https://api.pipcar.vn/s/TruongSa.json',
  VMN0: 'https://api.pipcar.vn/s/VNM0.json',
  VMN1: 'https://api.pipcar.vn/s/VNM1.json',
  VMN2: 'https://api.pipcar.vn/s/VNM2.json',
};
