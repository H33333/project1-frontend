import { default as _dayjs } from 'dayjs';
import 'dayjs/locale/vi';
import calendar from 'dayjs/plugin/calendar';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import duration from 'dayjs/plugin/duration';
import isBetween from 'dayjs/plugin/isBetween';
import relativeTime from 'dayjs/plugin/relativeTime';
import updateLocale from 'dayjs/plugin/updateLocale';
import { DateISO } from 'src/types/global.types';
_dayjs.locale('vi');
_dayjs.extend(updateLocale);
_dayjs.extend(calendar);
_dayjs.extend(duration);
_dayjs.extend(relativeTime);
_dayjs.extend(isBetween);
_dayjs.extend(customParseFormat);

_dayjs.updateLocale('vi', {
  calendar: {
    sameDay: '[Hôm nay lúc] hh:mm A', // The same day ( Today at 2:30 AM )
    nextDay: '[Ngày mai lúc] hh:mm A', // The next day ( Tomorrow at 2:30 AM )
    nextWeek: 'DD/MM/YYYY h:mm A', // The next week ( Sunday at 2:30 AM )
    lastDay: '[Hôm qua lúc] h:mm A', // The day before ( Yesterday at 2:30 AM )
    lastWeek: 'DD/MM/YYYY h:mm A', // Last week ( Last Monday at 2:30 AM )
    sameElse: 'DD/MM/YYYY h:mm A', // Everything else ( 17/10/2011 )
  },
});

export const dayjs = _dayjs;

export const dateFormat = 'DD-MM-YYYY';
export const timeFormat = 'HH:mm:ss';

export const formatFromNow = (date: DateISO | number) => dayjs(date).fromNow();

export const formatDate = (date: DateISO | number, format = dateFormat) =>
  dayjs(date).format(format);

export const isSameTime = (date1?: DateISO, date2?: DateISO, unit?: any) => {
  if (!date1 || !date2) return false;
  return dayjs(date1).isSame(dayjs(date2), unit);
};

export const isBetweenDate = (date: DateISO, fromDate: DateISO, toDate: DateISO) =>
  dayjs(date).isBetween(fromDate, toDate);

export const sorterByDate =
  <T extends Record<string, any>>(sorterKey: keyof T) =>
  (a: T, b: T) =>
    dayjs(b[sorterKey]).valueOf() - dayjs(a[sorterKey]).valueOf();
