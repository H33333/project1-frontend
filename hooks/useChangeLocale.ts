import { useRouter } from 'next/router';
import { useMemo } from 'react';
import { en } from 'src/types/i18n/en';
import { vi } from 'src/types/i18n/vi';

export default function useChangeLocale() {
  const { locale, replace } = useRouter();
  const i18nObj = useMemo<Record<string, string>>(() => {
    if (locale === 'en') return en;
    return vi;
  }, [locale]);
  return { locale, i18n: i18nObj };
}
