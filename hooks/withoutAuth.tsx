import { EmotionJSX } from '@emotion/react/types/jsx-namespace';
import { ComponentType } from 'react';
import Navigate from 'src/components/shared/Navigate';
import { useAppSelector } from 'src/redux/store';

type IntrinsicAttributes = EmotionJSX.IntrinsicAttributes;

function withoutAuth<T extends IntrinsicAttributes>(WrappedComponent: ComponentType<T>) {
  return function ComponentWithoutAuth(props: T) {
    const userState = useAppSelector((s) => s.user.data);
    const refreshToken = useAppSelector((s) => s.auth.refreshToken);

    if (!!refreshToken && userState?.role === 'ADMIN') return <Navigate to='/admin' />;
    if (!!refreshToken && userState?.role === 'PM') return <Navigate to='/' />;
    if (!!refreshToken && userState?.role === 'EXPERT') return <Navigate to='/' />;

    return <WrappedComponent {...props} />;
  };
}

export default withoutAuth;
