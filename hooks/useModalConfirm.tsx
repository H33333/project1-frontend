import styled from '@emotion/styled';
import { ModalFuncProps, Progress, Tooltip, Typography } from 'antd';
import useApp from 'src/hooks/useApp';
import useChangeLocale from './useChangeLocale';

type TProps = ModalFuncProps;

export default function useModalConfirm({ onOk, ...props }: TProps) {
  const { i18n } = useChangeLocale();
  const { modal: Modal } = useApp();
  const handleConfirm = (id?: string, title?: string, actionString = 'xóa') => {
    let secondsToGo = 5;
    const modal = Modal.confirm({
      title: (
        <Typography.Paragraph ellipsis={{ rows: 2 }} style={{ maxWidth: 416, margin: 0 }}>
          Bạn chắc chắn muốn {actionString}{' '}
          <Typography.Text type='danger'>{!!title ? title : id}</Typography.Text> ?
        </Typography.Paragraph>
      ),
      icon: null,
      content: '____',
      okText: i18n['Xác nhận'],
      cancelText: i18n['Hủy'],
      cancelButtonProps: { size: 'middle' },
      okButtonProps: { size: 'middle', danger: true },
      bodyStyle: { position: 'relative' },
      onOk: () => onOk?.(id),
      ...props,
    });
    const timer = setInterval(() => {
      secondsToGo -= 1;
      modal.update({
        content: (
          <>
            {i18n['Tự động đóng trong']} {secondsToGo}s.
            <ProgressWrapper>
              <Tooltip
                title={`${i18n['Tự động đóng trong']} ${secondsToGo}s.`}
                placement='bottomLeft'
              >
                <Progress
                  status='active'
                  strokeColor={{
                    from: '#f5222d',
                    to: '#ff7a45',
                  }}
                  percent={(secondsToGo / 5) * 100}
                  size={36}
                  type='circle'
                  showInfo
                  format={(percent) => `${secondsToGo}s`}
                />
              </Tooltip>
            </ProgressWrapper>
          </>
        ),
      });
    }, 1000);
    setTimeout(() => {
      clearInterval(timer);
      modal.destroy();
    }, secondsToGo * 1000 - 1);
  };
  return { handleConfirm };
}

const ProgressWrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
`;
