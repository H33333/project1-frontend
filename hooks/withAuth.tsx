import { EmotionJSX } from '@emotion/react/types/jsx-namespace';
import { ComponentType } from 'react';
import AdminLayout from 'src/components/layout/AdminLayout';
import PMLayout from 'src/components/layout/PMLayout';
import Navigate from 'src/components/shared/Navigate';
import { useAppSelector } from 'src/redux/store';

type IntrinsicAttributes = EmotionJSX.IntrinsicAttributes;

function withAuth<T extends IntrinsicAttributes>(
  WrappedComponent: ComponentType<T>,
  allowAdmin = false,
) {
  return function ComponentWithAuth(props: T) {
    const userState = useAppSelector((s) => s.user.data);
    const refreshToken = useAppSelector((s) => s.auth.refreshToken);

    if (!refreshToken) return <Navigate to='/login' />;
    if (!allowAdmin && userState?.role === 'ADMIN') return <Navigate to='/admin' />;
    if (!!allowAdmin && userState?.role === 'ADMIN')
      return (
        <AdminLayout>
          <WrappedComponent {...props} />
        </AdminLayout>
      );

    return (
      <PMLayout>
        <WrappedComponent {...props} />
      </PMLayout>
    );
  };
}

export default withAuth;
