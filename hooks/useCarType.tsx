import styled from '@emotion/styled';
import { TagProps } from 'antd';
import { useCallback } from 'react';
import { IoMdCar } from 'react-icons/io';
import {
  Xe16Icon,
  Xe29Icon,
  Xe35Icon,
  Xe45Icon,
  Xe5Icon,
  Xe7Icon,
  XeL9Icon,
} from 'src/components/icons';
import Tag from 'src/components/tag/Tag';
import { useGetAllCarTypesQuery } from 'src/redux/query/car.query';

function useCarType() {
  const { data: getCarTypeRes } = useGetAllCarTypesQuery({});
  const carIcon = useCallback(
    (type?: string) => {
      const carTypeList = getCarTypeRes?.data.car_types || [];
      const foundType = carTypeList.find((t) => t.name === type);
      if (!foundType)
        return (
          <IconStyled className='icon-type'>
            <IoMdCar />
          </IconStyled>
        );
      if (foundType.name_id === 1)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe5Icon />
          </IconStyled>
        );
      if (foundType.name_id === 2)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe7Icon />
          </IconStyled>
        );
      if (foundType.name_id === 3)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe16Icon />
          </IconStyled>
        );
      if (foundType.name_id === 4)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe29Icon />
          </IconStyled>
        );
      if (foundType.name_id === 5)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe35Icon />
          </IconStyled>
        );
      if (foundType.name_id === 6)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <Xe45Icon />
          </IconStyled>
        );
      if (foundType.name_id === 7)
        return (
          <IconStyled className={`icon-type icon${foundType.name_id}`}>
            <XeL9Icon />
          </IconStyled>
        );
    },
    [getCarTypeRes],
  );

  const carIconById = useCallback(
    (car_type_id?: number, tagProps?: TagProps) => {
      const carTypeList = getCarTypeRes?.data.car_types || [];
      const foundType = carTypeList.find((t) => t.name_id === car_type_id);
      return (
        <Tag
          bordered={false}
          icon={
            <IconStyled
              className={`icon-type icon${car_type_id || '0'}`}
              style={{ marginRight: 4, display: 'inline-flex', alignItems: 'center' }}
            >
              {!car_type_id && <IoMdCar />}
              {car_type_id === 1 && <Xe5Icon />}
              {car_type_id === 2 && <Xe7Icon />}
              {car_type_id === 3 && <Xe16Icon />}
              {car_type_id === 4 && <Xe29Icon />}
              {car_type_id === 5 && <Xe35Icon />}
              {car_type_id === 6 && <Xe45Icon />}
              {car_type_id === 7 && <XeL9Icon />}
            </IconStyled>
          }
          {...tagProps}
        >
          {foundType?.name}
        </Tag>
      );
    },
    [getCarTypeRes],
  );

  return { carIcon, carIconById };
}

const IconStyled = styled.div`
  flex: 0 0 auto;
  min-width: 0px;
  opacity: 0.9;
  svg {
    width: auto;
    height: 16px;
  }
  &.icon1 svg {
    width: auto;
    height: 13px;
  }
  &.icon3 svg {
    width: auto;
    height: 17px;
  }
`;

export default useCarType;
