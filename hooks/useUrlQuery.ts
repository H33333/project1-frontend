import { useMemoizedFn } from 'ahooks';
import { useRouter } from 'next/router';

function useUrlQuery<T extends Record<string, any>>(initialQuery: T): [T, (v?: T) => void] {
  const { pathname, query, replace, isReady } = useRouter();
  const handleChangeQuery = useMemoizedFn((v?: T) => {
    replace({ pathname, query }, { pathname, query: v || initialQuery }, { shallow: true });
  });

  // useUpdateEffect(() => {
  //   if (!isReady) handleChangeQuery(initialQuery);
  // }, [isReady, initialQuery]);

  return [(query as T) || initialQuery, handleChangeQuery];
}

export default useUrlQuery;
