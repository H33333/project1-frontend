import { useMemoizedFn } from 'ahooks';
import { useGetProvinceDistrictListQuery } from 'src/redux/query/address.query';

function useGetAddress() {
  const { data } = useGetProvinceDistrictListQuery({});
  const getProvinceDistrictTextById = useMemoizedFn((pId = '', dId = '') => {
    const provinceDistrictList = data?.data?.address_list || [];
    return provinceDistrictList.filter((item) => item.id === pId || item.id === dId);
  });
  return { getProvinceDistrictTextById };
}

export default useGetAddress;
