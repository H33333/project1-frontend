import { DateISO, ID } from './global.types';
export type TAgency = {
  _id: ID;
  phone: string;
  name: string;
  code: string;
  agency_avatar?: string;

  isTransportation?: boolean;
  isDriver?: boolean;
  status?: number;
  hasCar?: number;

  rank?: number;
  point?: number;

  address?: string;
  lat_address?: string;
  long_address?: string;
  lat?: string;
  long?: string;
  updated_gps_time?: number;
  updated_login_time_string?: string;
  updated_gps_time_string?: string;

  createdAt: DateISO;
  updatedAt: DateISO;
  is_refresh_token: number;
  refresh_token_expire?: string;
};
