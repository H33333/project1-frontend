import { TAgency } from './agency.types';
import { DateISO, ID } from './global.types';

export type TAllTicketItem = {
  _id: ID;
  agency_id: any;
  description: string;
  car_type_id: number;
  coin: number;
  supplier_id: any;
  status: number;
  pickup_province?: string;
  pickup_district?: string;
  pickup_time?: number;
  wait_list: any[];
  createdAt: DateISO;
  updatedAt: DateISO;
};

export type TTicketRank = {
  _id: ID;
  ticket_id: ID;
  agency_id: ID;
  rank: number;
};

export type TTicketWait = {
  _id: ID;
  ticket_id: ID;
  supplier_id: ID;
  status: number;
};

export type TTicket = {
  _id: ID;
  agency_id: string;
  description: string;
  car_type_id: number;
  coin: number;
  supplier_id: string;
  status: number;
  pickup_province?: string;
  pickup_district?: string;
  pickup_time?: number;
  createdAt: DateISO;
  updatedAt: DateISO;
};

export type TAllTicketReviewItem = {
  ticket_info: TTicket;
  agency_info: Pick<TAgency, '_id' | 'name' | 'phone' | 'status'>;
  ticket_rank_info: TTicketRank;
  _id: ID;
  ticket_id: ID;
  ticket_price: number;
  review_price: number;
  pickup_province: string;
  pickup_district: string;
  dropoff_province: string;
  dropoff_district: string;
  car_type_id: number;
  pickup_time: number;
  status: number;
  is_ranked: number;
  createdAt: DateISO;
  updatedAt: DateISO;
};

export type TRankTicketReviewAgencyItem = {
  rank_1: TTicketReviewItem[];
  rank_2: TTicketReviewItem[];
  rank_3: TTicketReviewItem[];
  rank_4: TTicketReviewItem[];
  rank_5: TTicketReviewItem[];
  total: number;
  agency_info: TAgency;
};

export type TTicketReviewItem = {
  ticket_info: TTicket;
  _id: ID;
  ticket_id: ID;
  ticket_price: number;
  review_price: number;
  pickup_province: string;
  pickup_district: string;
  dropoff_province: string;
  dropoff_district: string;
  car_type_id: number;
  pickup_time: number;
  status: number;
  is_ranked: number;
  rank: number;
  createdAt: DateISO;
  updatedAt: DateISO;
};
